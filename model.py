from datetime import datetime
from sqlalchemy.orm import backref
from init import *

class Test(db.Model):
    __bind_key__ = 'mssql_bind'
    __tablename__ = 'Test'

    UserID = db.Column(
        db.String(255),
        primary_key=True
    )
    FirstName = db.Column(
        db.String(255),
        index=False,
        nullable=False
    )
    LastName = db.Column(
        db.String(255),
        index=False,
        nullable=False
    )

    def __repr__(self):
        return '<Users {}>'.format(self)

class User(db.Model, UserMixin):
    """Data model for user accounts."""

    __tablename__ = 'Users'
    UserID = db.Column(
        db.String(255),
        primary_key=True
    )
    FirstName = db.Column(
        db.String(255),
        index=False,
        nullable=False
    )
    LastName = db.Column(
        db.String(255),
        index=False,
        nullable=False
    )
    Username = db.Column(
        db.String(255),
        index=False,
        unique=True,
        nullable=False
    )
    email = db.Column(
        db.String(255),
        index=True,
        unique=True,
        nullable=False
    )
    AccountType = db.Column(
        db.String(255),
        index=False,
        nullable=False
    )
    ActivationCode = db.Column(
        db.String(255),
        index=False,
        unique=True,
        nullable=False
    )

    RecoveryCode = db.Column(
        db.String(255),
        index=False,
        unique=True,
        nullable=False
    )
    EmailConfirmed = db.Column(
        db.Boolean,
        index=False,
        nullable=False
    )
    AccountStatus = db.Column(
        db.Boolean,
        index=False,
        nullable=False
    )
    PasswordHash = db.Column(
        db.String(255),
        nullable=False
    )
    LinkCreatedDate = db.Column(
        db.DateTime,
        default=datetime.utcnow
    )
    LinkExpiryDate = db.Column(
        db.DateTime,
        default=datetime.utcnow

    )

    def get_id(self):
        return (self.UserID)
    def __repr__(self):
        return '<Users {}>'.format(self)

    def has_role(self, role):
        return role in self.AccountType


class Role(db.Model):
    """Data model for user accounts."""

    __tablename__ = 'Roles'
    RoleID = db.Column(
        db.Integer,
        primary_key=True
    )
    RoleName = db.Column(
        db.String(255)
    )

    def __repr__(self):
        return '<Roles {}>'.format(self)


class UserRoles(db.Model):
    """Data model for user accounts."""

    __tablename__ = 'UserRoles'
    UserID = db.Column(
        db.String(255),
        db.ForeignKey('Users.UserID'),
        primary_key=True,
    )
    relation = db.relationship(
        'User',
        backref=backref("Users", uselist=False)
    )
    RoleID = db.Column(
        db.Integer,
        db.ForeignKey('Roles.RoleID'),
        primary_key=True,
    )
    relation = db.relationship(
        'Role',
        backref=backref("Roles", uselist=False)
    )

    def __repr__(self):
        return '<UserRoles {}>'.format(self)


class EventLog(db.Model):
    """Data model for user accounts."""

    __tablename__ = 'EventLog'
    RoleID = db.Column(
        db.String(255),
        primary_key=True
    )

    UserID = db.Column(
        db.String(255)
    )
    Event = db.Column(
        db.String(255)
    )
    EventSummary = db.Column(
        db.String(255)
    )
    EventDate = db.Column(
        db.DateTime,
        default=datetime.utcnow
    )

    def __repr__(self):
        return '<Roles {}>'.format(self)
