import os
import shutil
import uuid
from flask import *
from flask_mail import Mail, Message
from urllib import request
from flask import session, redirect, url_for, render_template, flash, request
from werkzeug.security import generate_password_hash, check_password_hash
import psycopg2
import psycopg2.extras
import pymssql
from urllib.parse import quote
from model import *
from werkzeug.utils import secure_filename

# Views
@login_manager.user_loader
def load_user(UserID):
    return User.query.get(UserID)

@app.route('/')
def home():
    # Check if user is loggedinp
    return render_template('login.html')

    if 'loggedin' in session:
        # User is loggedin show them the home page
        return render_template('Home.html', username=session['username'])
        # User is not loggedin redirect to login page
        return redirect(url_for('login'))


@app.route('/login/', methods=['GET', 'POST'])
def login():

    if request.method == 'POST':
        email = request.form.get('userName')
        password = request.form.get('password')

        user = User.query.filter_by(email=email).first()
        # check if the user actually exists
        # take the user-supplied password, hash it, and compare it to the hashed password in the database
        if not user or not check_password_hash(user.PasswordHash, password):
            flash('Invalid Email or Password.', email)
            return redirect(url_for('login'))  # if the user doesn't exist or password is wrong, reload the page

        elif user.EmailConfirmed == False: #Check if the users email is confirmed
            flash('Your email is not confirmed, please confirm your email to login.', email)
            return redirect(url_for('login'))

        elif user.AccountStatus == False: #Check if the users account is activated
            flash('Your account has not been activated please contact the admin.', email)
            return redirect(url_for('login'))
        else:
            # if the above check passes, then we know the user has the right credentials
            session['loggedin'] = True
            session['username'] = user.email
            login_user(user)
            return redirect(url_for('connect'))
    else:
        return render_template('login.html')

    return render_template('login.html')

@app.route('/Home/', methods=['GET', 'POST'])
@login_required
def connect():
    level = 0
    if request.method == 'POST':
        QueryNum = request.form.get('value')
        QueryUpload = request.files.get('QueryUpload')
        GetStartDate = request.form.get('startdate')
        GetEndDate = request.form.get('enddate')
        if QueryUpload != None:
            QueryUpload = request.files.get('QueryUpload').filename


        ip = request.form.get('ip')
        if ip == None:
            ip =  request.args.get('ip')
        try:
            server = ip+'\\SMARTCARE40'
            app.config['SQLALCHEMY_BINDS'] = {'mssql_bind': "mssql+pymssql://sa:%s@%s/cdc_fdb_db" % (quote('m7r@n$4mAz'),server )}
            app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
            con = pymssql.connect(server=server, user=user, password=pwd, database=database)
            cursor = con.cursor()
            cursor.execute("select value from setting where name = 'AggregationLevel'")
            AggregationLevel = cursor.fetchone()
            Aggregation = AggregationLevel[0]
            if Aggregation == 'Facility':
                level = 1
            elif Aggregation == 'District':
                level = 2
            elif Aggregation == 'Province':
                level = 3
            elif Aggregation == 'National':
                level = 4

            try:
                if QueryUpload != '' and QueryUpload != None:
                    QueryUpload = request.files['QueryUpload']
                    isExist = os.path.exists("Uploaded")

                    if not isExist:
                        os.makedirs("Uploaded")
                    QueryUpload.save(os.path.join("Uploaded",(secure_filename(QueryUpload.filename))))

                    with open('Uploaded/'+QueryUpload.filename, 'r') as file:
                        startDate = GetStartDate.replace('-', '')
                        endDate = GetEndDate.replace('-', '')
                        StratDateReplace = file.read().replace('StartDateValue', startDate)
                        EndDateReplace = StratDateReplace.replace('EndDateValue', endDate)
                        data0 = EndDateReplace.replace('[', '')
                        data = data0.replace(']', '')
                        cursor.execute(data)
                        Result = cursor.fetchall()
                        num_fields = len(cursor.description)
                        field_names = [i[0]
                                       for i in cursor.description
                                       ]

                        flash(ip)
                        return render_template('Home.html', Result=Result, Header= field_names, length=num_fields, Query = QueryNum, level = level, GetStartDate = GetStartDate, GetEndDate = GetEndDate)

                elif QueryNum != '' and QueryNum != None:
                    with open('Queries/'+QueryNum+'.sql', 'r') as file:
                        startDate = GetStartDate.replace('-', '')
                        endDate = GetEndDate.replace('-', '')
                        StratDateReplace = file.read().replace('StartDateValue', startDate)
                        EndDateReplace = StratDateReplace.replace('EndDateValue', endDate)
                        cursor.execute(EndDateReplace)
                        Result = cursor.fetchall()
                        num_fields = len(cursor.description)
                        field_names = [i[0]
                                       for i in cursor.description
                                       ]

                        flash(ip)
                        return render_template('Home.html', Result=Result, Header= field_names, length=num_fields, Query = QueryNum, level = level, GetStartDate = GetStartDate, GetEndDate = GetEndDate)

                else:
                    flash('Connected', ip)
                    con.close()
                return render_template('Home.html', Header=None, length=0, level = level)


            except Exception as e:
                error = e
                flash(ip, 'ip')
                flash('DatabaseError', error)

                return render_template('Home.html', Header=None, length=0, level = level)
            else:

                flash('Connected', ip)
                con.close()
            return render_template('Home.html', Header=None, length=0, level = level)
        except Exception as e:
            error = e
            flash(ip, 'ip')
            flash('error', error)

            return render_template('Home.html', Header=None, length=0, level = level)
    return render_template('Home.html',Header= None, length = 0)
@app.route('/register', methods=['POST'])
def register():
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    a = uuid.uuid1()
    b=uuid.uuid1()
    c=uuid.uuid1()
    # Check if "username", "password" and "email" POST requests exist (user submitted form)
    # code to validate and add user to database goes
    UserID = str(uuid.uuid1())
    FirstName = request.form.get('FirstName')
    LastName = request.form.get('LastName')
    Username = request.form.get('email')
    email = request.form.get('email')
    password = request.form.get('password')
    AccountType = "User"
    ActivationCode = str(uuid.uuid1())
    RecoveryCode = str(uuid.uuid1())
    EmailConfirmed = False
    AccountStatus = False
    LinkCreatedDate = datetime.utcnow()
    LinkExpiryDate = datetime.utcnow()
    user = User.query.filter_by(
        Username=Username).first()  # if this returns a user, then the email already exists in database

    if user:  # if a user is found, we want to redirect back to signup page so user can try again
        error = '"The email address "'+email+' "is already in use"'
        return redirect(url_for('login', error = error, name = FirstName, surname = LastName, email = email ))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(UserID=UserID, FirstName=FirstName, LastName=LastName, Username=Username, email=email,
                    PasswordHash=generate_password_hash(password, method='sha256'),
                    AccountType=AccountType, ActivationCode=ActivationCode, RecoveryCode=RecoveryCode,
                    EmailConfirmed=EmailConfirmed, AccountStatus=AccountStatus, LinkCreatedDate=LinkCreatedDate,
                    LinkExpiryDate=LinkExpiryDate)

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    userR = User.query.filter_by(UserID=UserID).first()  # if this returns a user, then the email already exists in database
    if userR.AccountType == "Admin":
        userRole = UserRoles(UserID=UserID, RoleID=1)
        db.session.add(userRole)
        db.session.commit()
        verify(email, ActivationCode);
        return redirect(url_for('login', error='success'))

    elif userR.AccountType == "User":
        userRole = UserRoles(UserID=UserID, RoleID=2)
        db.session.add(userRole)
        db.session.commit()
        verify(email, ActivationCode);
        return redirect(url_for('login', error='success'))


    # Check if user is logged in
    if 'loggedin' in session:
        cursor.execute('SELECT * FROM users WHERE id = %s', [session['id']])
        account = cursor.fetchone()
        # Show the profile page with account info
        return render_template('profile.html', account=account)

    # User is not logged in redirect to login page'''
   # return redirect(url_for('login'))


mail = Mail(app)
app.config["MAIL_SERVER"]='smtp.gmail.com'
app.config["MAIL_PORT"] = 465
app.config["MAIL_USERNAME"] = 'smartcaredext@gmail.com'
app.config['MAIL_PASSWORD'] = '123123@DExT'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

def verify(email, ActivationCode):
    verifyUrl = "http://localhost:5000/EmailConfirmed/?code=";
    link = verifyUrl + ActivationCode;
    body = "<html><body><br/><br/>We are excited to tell you that your Smartcare TExD account has been successfully created. Please click link below to confirm your email and request for account activation" + " <br/><br/><a  href='" + link + "'>ClickHere</a></body></html>";
    subject = 'Email Confirmation Request'

    msg = Message(subject ,sender = 'username@gmail.com', recipients = [email])
    msg.html = body
    mail.send(msg)


@app.route('/EmailConfirmed/', methods=['GET', 'POST'])
def EmailConfirmed():
    ActivationCode = request.args.get('code')
    if ActivationCode != "" and ActivationCode != None:
        user = User.query.filter_by(ActivationCode = ActivationCode).first()
        useremail = user.email
        user.EmailConfirmed = True
        db.session.commit()
        ActivationRequest(ActivationCode, useremail)

    return render_template('EmailConfirmed.html', code = ActivationCode)

def ActivationRequest(ActivationCode, useremail):
    Admin = 'Admin'
    user = User.query.filter_by(AccountType = Admin).first()
    email = user.email

    verifyUrl = "http://localhost:5000/AccountActivated/?code=";
    link = verifyUrl + ActivationCode;
    body = "<html><body><br/><br/>An account with email address "+ useremail +" requires activation. Click the  link below to activate " + " <br/><br/><a  href='" + link + "'>ClickHere</a></body></html>";
    subject = 'Account Activation Request'

    msg = Message(subject, sender='username@gmail.com', recipients=[email])
    msg.html = body
    mail.send(msg)

@app.route('/AccountActivated/', methods=['GET', 'POST'])
def AccountActivated():
    ActivationCode = request.args.get('code')
    link = "http://localhost:5000/login/"
    body = "<html><body><br/><br/>Your SmartCareDExT account has been activated you can now login by clicking link below" + " <br/><br/><a  href='" + link + "'>Login</a></body></html>";
    subject = 'Account Activated'
    if ActivationCode != "" and ActivationCode != None:
        user = User.query.filter_by(ActivationCode=ActivationCode).first()
        user.AccountStatus = True
        email = user.email
        db.session.commit()
        msg = Message(subject, sender='username@gmail.com', recipients=[email])
        msg.html = body
        mail.send(msg)


    return render_template('AccountActivated.html', code=ActivationCode)

@app.route('/resetPassword/', methods=['GET', 'POST'])
def resetPassword():

    if request.method == 'POST':
        email = request.form.get('email')
        user = User.query.filter_by(email = email).first()
        if user:
            code = user.RecoveryCode
            link = "http://localhost:5000/ChangePassword/?code=" + code
            body = "<html><body><br/><br/>A request has been made to reset your SmartCareDExT password clicking link below to do so" + " <br/><br/><a  href='" + link + "'>Reset Password</a></body></html>";
            subject = 'Reset Password'
            msg = Message(subject, sender='username@gmail.com', recipients=[email])
            msg.html = body
            mail.send(msg)
            flash('An email has been sent to '+email+' for password reset')

            return render_template('resetPassword.html')
        else:
            flash('No account exists with the email ' + email + ', please check the email you enter')
            return render_template('resetPassword.html')

    else:
        return render_template('resetPassword.html')

@app.route('/ChangePassword/', methods=['GET', 'POST'])
def ChangePassword():
    code = request.form.get('code')
    if request.method == 'POST':
        password = request.form.get('password')
        PasswordHash = generate_password_hash(password, method='sha256')
        user = User.query.filter_by(RecoveryCode = code).first()
        user.PasswordHash = PasswordHash
        db.session.commit()

        flash('Your password has been reset')
        return render_template('ChangePassword.html')

    else:
        return render_template('ChangePassword.html')



@app.route('/Profile/', methods=['GET'])
@login_required
def Profile():
    email = session['username']

    user = User.query.filter_by(email=email).first()
    user = [user.UserID,user.FirstName, user.LastName, user.email, user.AccountType, user.AccountStatus, user.EmailConfirmed, user.LinkCreatedDate]

    #return jsonify({'data':render_template('Home.html', length=0, Header = user), 'value':user})
    return jsonify(user)


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    """Logout the current user."""
    user = current_user
    user.authenticated = False
    logout_user()
    isExist = os.path.exists("Uploaded")

    if isExist:
        shutil.rmtree("Uploaded")

    return render_template("login.html")