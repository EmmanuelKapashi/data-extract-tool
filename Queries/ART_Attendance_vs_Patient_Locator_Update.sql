USE [cdc_fdb_db];

DECLARE   
	@StartDate DATETIME,
	@EndDate DATETIME, 
    @ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FG VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@PN VARCHAR(100), 
	@DN VARCHAR(100), 
	@FN VARCHAR(100), 
	@NHC VARCHAR(10), 
	@OHC VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME

SET @Now = GETDATE()
SET @StartDate = '20210501'
SET @EndDate = EOMONTH(@StartDate)
SET @EndDate = DATEADD(SECOND, 86399, @EndDate) --adding 23:59:59
IF(@EndDate > @Now) SET @EndDate = @Now -- Prevent future date 
SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode')
SET @FG = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid')
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId')
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId')
SET @LTFUThreshold = 30
SET @OnArtMode = 1 -- 1 = CURRENT, 0 = EVER 

SELECT 
    @PN = p.Name, 
    @DN=d.Name, 
    @FN=f.FacilityName, 
    @NHC=f.HMISCode, 
    @OHC=m.OldHMISCode 
FROM Facility f 
LEFT JOIN district d ON f.DistrictId = d.Code 
LEFT JOIN province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FG;


IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

--****************************** ART Attendance Cohort *******************************************
if(object_id('tempdb..#Cohort') is not null) drop table #Cohort
CREATE TABLE #Cohort(
	PatIdInt INT,
	NUPN VARCHAR(50),
	PatGUID VARCHAR(50),
	ArtNumber VARCHAR(50)
)

-- By Pharmacy ARV dispensation
if(object_id('tempdb..#Dispensations') is not null) drop table #Dispensations
select dense_rank() over(partition by PatientId, InteractionID order by VisitDate, GenericName) DC, * 
into #Dispensations 
from (
	select distinct 
		DD.InteractionID, 
		P.PatientReportingId PatientId, 
		P.InteractionDate VisitDate, 
		P.NextVisitDatm NextAppointmentDate, 
		D.GenericName, 
		D.ComponentCount,
		DD.UnitQuantityPerDose, 
		MF.FrequencyString, 
		MF.DosesPerDay, 
		DD.UnitsDispensed, 
		case
			when D.GenericName = 'Dolutegravir + emtricitabine + Tenofovir alafenamide' then 'TAF+FTC+DTG'
			when D.GenericName = 'abacavir+lamivudine+zidovudine' then 'ABC+3TC+AZT'
			when D.GenericName = 'amprenavir' then 'APV'
			when D.GenericName = 'atazanavir+ritanovir' then 'ATV/r'
			when D.GenericName in ('efavirenz+lamivudine+tenofovir', 'Tenofovir, Lamivudine and Efavirenz') then 'TDF+3TC+EFV'
			when D.GenericName = 'efavirenz+emtricitabine+tenofovir' then 'TDF+FTC+EFV'
			when D.GenericName = 'lamivudine+nevirapine+stavudine' then 'D4T+3TC+NVP'
			when D.GenericName = 'lamivudine+nevirapine+zidovudine' then 'AZT+3TC+NVP'
			when D.GenericName = 'lamivudine+stavudine' then '3TC+D4T'
			when D.GenericName = 'lopinavir+ritonavir' then 'LPV/r'
			when D.GenericName in ('zidovudine syrup', 'zidovudine') then 'AZT'
			else D.Synonym
		end Abbr
	from crctMedicationDispensingDetails DD
	left join Interaction P on DD.InteractionID = P.InteractionID
	left join DrugProductsCachedView D on DD.MedDrugId = D.MedDrugId
	left join MedFrequency MF on DD.Frequency = MF.FrequencyID
		where D.DrugClass = 'Antiretrovirals'
		and D.WHOSubClass LIKE 'Antiretroviral - %'
) X
	where  VisitDate >= @StartDate
	AND VisitDate <= @EndDate
	order by PatientId, VisitDate, DC

-- By HIV monitoring Labs
IF(OBJECT_ID('tempdb..#HIVLabs') IS NOT NULL) DROP TABLE #HIVLabs
SELECT 
	i.PatientReportingId PatientId,
	a.InteractionID, 
	a.EditLocation, 
	CASE
        WHEN a.LabOrderDate IS NULL THEN i.InteractionDate 
        ELSE a.LabOrderDate END LabOrderDate, 
	a.LabTestID, 
	c.AbbrvSyn, 
	b.LabTestValue, 
	c.Units 
INTO #HIVLabs
FROM crctLaboratoryOrderDetails a
JOIN crctLaboratoryResultDetails b ON a.InteractionID = b.InteractionID AND a.LaboratoryOrderID = b.LaboratoryOrderID
LEFT JOIN Interaction i ON a.InteractionID = i.InteractionID AND ServiceCode = 402 -- 402: Lab orders and results
LEFT JOIN LabTestsDictionary c ON a.LabTestID = c.LabTestID
	WHERE a.LabTestID IN (102, 103) --CD4 and VL
	AND LabOrderDate BETWEEN @StartDate AND @EndDate
	AND a.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
	ORDER BY PatientId, LabOrderDate DESC

--By Clinical visit
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit
SELECT 
	c.PatientId,
    c.InteractionDate
INTO #CurrentOnARTByClinicalVisit
FROM (	
		SELECT 
            c.PatientId,
            InteractionDate,
            ROW_NUMBER() OVER (PARTITION BY c.PatientId ORDER BY InteractionDate DESC) AS seq
        FROM ClientClinicalCareDates c --@PC: Restricted to ART Clinical and HIV-related Lab Visits 
        LEFT JOIN ServiceCodes sc ON c.VisitType = sc.ServiceCode
	        WHERE ((sc.ServiceCategoryID = 4) --ART visits only
					OR (c.InteractionId IN (SELECT DISTINCT InteractionID FROM #HIVLabs)
					OR (c.PatientId in (SELECT DISTINCT PatientId FROM #Dispensations)))
					) -- @PC: Only ART Visits and HIV Labs
	        AND sc.Deprecated = 0
            AND c.InteractionDate >= dateadd(day, -@LTFUThreshold, @EndDate)
            AND c.InteractionDate <= @EndDate 
            AND c.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
                
    ) c
    WHERE c.seq = 1;

INSERT INTO #Cohort(
	PatIdInt,
	NUPN,
	PatGUID,
	ArtNumber
)
SELECT DISTINCT 
	a.PatientId ,
	r.PatientId,
	g.MappedGuid,
	r.ArtNumber
FROM (
	SELECT PatientId FROM #CurrentOnARTByClinicalVisit
	UNION
	SELECT PatientId FROM #Dispensations
	UNION
	SELECT PatientId FROM #HIVLabs
) a LEFT JOIN crtRegistrationInteraction r ON a.PatientId = r.patientId_int
LEFT JOIN GuidMap g ON a.PatientId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid;

--************************* End ART Cohort **********************************

IF(OBJECT_ID('tempdb..#NewLocator') IS NOT NULL) DROP TABLE #NewLocator
SELECT DISTINCT a.PatientId 
INTO #NewLocator
FROM crtPatientLocator a 
LEFT JOIN crtRegistrationInteraction r ON a.PatientId = r.patientId_int
	WHERE a.EditLocation IN (SELECT DISTINCT SubSiteId FROM #CurrentFacility)
	AND a.PatientId IN (SELECT DISTINCT PatIdInt FROM #Cohort)
	AND a.VisitDate BETWEEN @StartDate AND @EndDate
	AND a.VisitDate > r.VisitDate
	AND DATEDIFF(DAY, r.VisitDate, a.VisitDate) > 10
	AND a.PatientId IN (SELECT DISTINCT PatIdInt FROM #Cohort);

DECLARE
	@ArtAttn INT,
	@NewPL INT;
SET @ArtAttn = (SELECT COUNT(DISTINCT PatIdInt) FROM #Cohort);
SET @NewPL = (SELECT COUNT(DISTINCT PatientId) FROM #NewLocator);

SELECT 
	@PN Province,
	@DN District,
	CASE @hmiscode
		--Chilanga Aliases
		WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Market Community Post'
		WHEN '50060106A' THEN 'George Community Post Unknown (1)'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ngombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		ELSE @FN
	END Facility,
	@hmiscode FacilityHMISCode,
	@ArtAttn ArtAttendance,
	@NewPL ClientsWithNewLocatorForm