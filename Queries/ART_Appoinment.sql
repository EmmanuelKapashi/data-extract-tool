use cdc_fdb_db;
--go
--CREATE PROCEDURE [dbo].NextAppointmentDates
declare
	@StartDate  DATETIME,
    @EndDate    DATETIME,
	@ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME
	
set @StartDate = 'StartDateValue'
set @EndDate = 'EndDateValue'
SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode')
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid')
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId')
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId')
SET @LTFUThreshold = 30
SET @OnArtMode = 1 -- 1 = CURRENT, 0 = EVER 

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;
--AS
--BEGIN
declare @qtr2_startdate date
declare @qtr2_enddate date
declare @diff int 

declare @qtr3_startdate date
declare @qtr3_enddate date

set @qtr2_startdate = @StartDate
set @qtr2_enddate = @EndDate

--number of days to add to second set of dates to avoid confusion when the user set date less than a month

set @diff =  DATEDIFF(d, @qtr2_startdate, @qtr2_enddate)

set @qtr3_startdate = dateadd(d, @diff, @qtr2_startdate)
set @qtr3_enddate = dateadd(d, @diff ,@qtr2_enddate)



;with p_active as(
select max (case when visittype = 12 or VisitType = 38 then 'Pharm' else 'Clinical' end) Visittype_ ,patientid, max(artnumber) artnumber, max(nextappointmentdate) nexapp, max( visitdate) lastvisit, datediff(dd, max(nextappointmentdate), @qtr2_enddate) dayslate
from (select patientid,visitdate, nextappointmentdate, visittype, artnumber from crtclinicalvisit 
	union select l.patientid, l.visitdate, l.nextappointmentdate, l.visittype, i.artnumber from crtlaboratoryvisit l left join crtihap i on l.patientid = i.patientid) allvisits
where 
convert(varchar(8), visitdate, 112) <= @qtr2_enddate
and
visittype in
(24,25,27,28
--,29
,30
--,33
,39,40, 41,42,44,47,48,50
--,53
,59,315,330,328,339
--,333
--,329
--,359
,350
--353
,256,402,259,25,260,257--select * from servicecodes select * from crtlaboratoryvisit where patientid = 385539
,337,317,344,347,327)
group by patientid
--having datediff(dd, max(nextappointmentdate), @qtr2_enddate) < 90 or max(visitdate) between @qtr2_startdate and @qtr2_enddate
),--select * from p_active where patientid = 29063
pharm_active as(
select max (case when visittype = 12 or VisitType = 38 then 'Pharm' else 'Clinical' end) Visittype_ ,patientid, max(artnumber) artnumber, max(nextappointmentdate) nexapp, max( visitdate) lastvisit, datediff(dd, max(nextappointmentdate), @qtr2_enddate) dayslate
from (select patientid,visitdate, nextappointmentdate, visittype, artnumber from crtclinicalvisit 
	union select l.patientid, l.visitdate, l.nextappointmentdate, l.visittype, i.artnumber from crtlaboratoryvisit l left join crtihap i on l.patientid = i.patientid) allvisits
where 
convert(varchar(8), visitdate, 112) <= @qtr2_enddate
and
visittype in
(12,38)
group by patientid
--having datediff(dd, max(nextappointmentdate), @qtr2_enddate) < 90 or max(visitdate) between @qtr2_startdate and @qtr2_enddate

),
drugs as
(
 --  select * from
 --  (
	--select 
	--	v.patientid,
	--	visitdate, v.NextAppointmentDate, v.InteractionID,d.GenericIngredients,
	--	ROW_NUMBER() over (partition by patientid order by v.visitdate desc) seq--,
	----	q.GenericIngredients
	--from crtPharmacyVisit v
	--join crctPharmacydispensation p on v.InteractionID = p.InteractionID
	--join DrugProductsCachedView d on p.PhysicalDrugId = d.MedDrugId and d.DrugClass='Antiretrovirals'
	----join view_DrugProducts q on p.PhysicalDrugId = q.MedDrugId
	--where v.ArvDrugDispensed = 1
	--)d where seq = 1
select * from(
	select 
		v.patientid,
		visitdate, v.NextAppointmentDate, v.InteractionID,
		ROW_NUMBER() over (partition by v.patientid order by v.visitdate desc) seq,d.GenericIngredients,
		--MedDrugId, 
		d.Synonym, 
		case when GenericName = 'amprenavir' then 'apv' else
		SUBSTRING(d.synonym, 1,case CHARINDEX(',',d.synonym)+CHARINDEX('-',d.synonym) when 0 then LEN(d.Synonym) else CHARINDEX(',',d.synonym)+CHARINDEX('-',d.synonym)-1 end)
		end AS genericcomb1,
		Dense_RANK()over(partition by patientid order by convert(varchar(8),v.visitdate,112) desc)
		as disp_count
		,d.ComponentCount
		,location
	--into #disps
	from crtPharmacyVisit v
	join crctPharmacydispensation p on v.InteractionID = p.InteractionID
	join DrugProductsCachedView d on p.PhysicalDrugId = d.MedDrugId and d.DrugClass='Antiretrovirals'
	where v.ArvDrugDispensed = 1
	--order by PatientId, 5,4
)drgs  where seq = 1
	


),
--longsingle as (
--select patientid, VisitDate,NextAppointmentDate,[Location],InteractionID, disp_count, split.a.value('.', 'VARCHAR(100)') comp
--	--into #longsingle
--	from
--		(select 
--		*, CAST('<m>'+REPLACE(genericcomb1,'+','</m><m>')+'</m>' as XML) DATA 
--		from drugs
--		) AS A CROSS APPLY DATA.nodes ('/m') as split(a)
--),
Viraload as
(
	select * from(
	select *,
	  case when datediff(dd, convert(date, ViralLoadOrderDate, 103), @qtr2_enddate) <= 365.25 and ViralLoadResult is not null then 'Yes'
		      when ViralLoadOrderDate is null and ViralLoadResult is not null then 'Unknown'
			  when ViralLoadOrderDate is null and ViralLoadResult is null then 'No'
		 else 
			  'No' 
	     end HasViralLoadInLast12Months
	 from(
	select o.PatientId, r.InteractionID, o.LabOrderDate ViralLoadOrderDate, r.InteractionDate ResultDate, o.InteractionDataEntryLocation, r.LabTestID, 
	       LabTestValue ViralLoadResult,
	       case when ISNUMERIC(r.LabTestValue) = 1 then convert(decimal(10,0), r.LabTestValue) end NumericVLResult,
		   case when ISNUMERIC(r.LabTestValue) = 0 then r.LabTestValue end NonNumericVLResult,
		   row_number()over(partition by o.patientid order by o.laborderdate desc) lastvisit 
	from (
		  select o.PatientId, o.InteractionID, o.EditLocation, o.EditLocationSeqNumber, o.InteractionDate, o.InteractionDataEntryLocation,
				 d.LabOrderDate, d.LabTestID
		  from crtLaboratoryOrders o
		  join crctLaboratoryOrderDetails d on (o.InteractionID = d.InteractionID and o.EditLocation = d.EditLocation and o.EditLocationSeqNumber = d.EditLocationSeqNumber)
		  ) o
	left join (
			   select r.PatientId, r.InteractionID, r.EditLocation, r.EditLocationSeqNumber, r.InteractionDate, r.InteractionDataEntryLocation,
					  d.LabTestID, d.LabTestValue
			   from crtLaboratoryResults r
			   join crctLaboratoryResultDetails d on (r.InteractionID = d.InteractionID and r.EditLocation = d.EditLocation and r.EditLocationSeqNumber = d.EditLocationSeqNumber)
			   ) r
	on o.PatientId = r.PatientId and o.InteractionID =r.InteractionID and o.LabTestID = r.LabTestID
	where o.LabTestID = 102 and LabTestValue not in ('.', '.39', '-')
	) x )vl where HasViralLoadInLast12Months = 'Yes' and lastvisit = 1
), -- * from Viraload,
startdates as(
	select crtpharmacyvisit.patientid, min(visitdate) firstpharm 
	from crtpharmacyvisit join p_active  on crtpharmacyvisit.patientid = p_active.patientid 
	where arvdrugdispensed = 1
 group by crtpharmacyvisit.patientid
),
transferins as(
	select distinct patientid,PatientTransfer from crtpatientlocator where patienttransfer = 2 and visitdate between @qtr2_startdate and @qtr2_enddate
), patstatus as(
select patientid, visitdate, case when patienttransferout = 1 then 'TO' when patientmadeinactivereason = 3 then 'A-LTFU' when patientmadeinactivereason = 2 then 'StoppedART' when patientmadeinactive = 1 then 'MadeInactive' when patientdied = 1 then 'Died' when patientreactivated = 1 then 'Active' end pstatus, row_number() over(partition by patientid order by visitdate desc) latestvisit 
from crtpatientstatus --where patientid = 385539
where visitdate between @qtr2_startdate and @qtr2_enddate
), patinactive as(
	select * from patstatus where latestvisit = 1 and pstatus != 'Active'
),
transferins3 as(
	select distinct patientid, PatientTransfer from crtpatientlocator where patienttransfer = 2 and visitdate between @qtr3_startdate and @qtr3_enddate
), Reg as (
	select patientId_int PatientId, max(r.PatientId) NUPN, max(r.FirstName) FirstName, max(r.SurName) SurName, max(r.DateOfBirth) DateOfBirth, max(r.Sex) Sex, max(PhoneContact) PhoneContact, max(PhoneNumber) PhoneNumber, 
			max(MobilePhoneNumber) MobilePhoneNumber, max(HousePlotNumber) HousePlotNumber, max(StreetName) StreetName, max(CommunityName) CommunityName, max(CommunityNickname) CommunityNickname 
		from crtRegistrationInteraction r
			join GuidMap g on r.patientId_int = g.NaturalNumber
			--join registration r2 on g.OwningGuid = r2.PatientGUID --Test correct guid 3406755
			left join [Address] a on g.MappedGuid = a.PatientGUID and AddressType = 'Current'
		group by r.patientId_int
)
--datepart(ww,i.InteractionTime)
select 
	@ProvinceName Province,
	@DistrictName District,
	CASE @hmiscode
		--Chilanga Aliases
		WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Market Community Post'
		WHEN '50060106A' THEN 'George Community Post Unknown (1)'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ngombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		ELSE @FacilityName
	END Facility,
	@hmiscode FacilityHMIS,
	Reg.NUPN, 
	reg.SurName, 
	reg.FirstName,
	Reg.Sex,
	Reg.DateOfBirth,
	DATEDIFF(DAY, Reg.DateOfBirth, @EndDate)/365.25 AgeAtEnd, 
	p.artnumber,
	p.lastvisit Clinical_Lastvisit,
	p.nexapp Clinical_Appoint, 
	datepart(ww,p.nexapp) Clinical_Appoint_WeekNO,
	f.lastvisit Pharm_Lastvist, 
	f.nexapp Pharm_Appoint,
	datepart(ww,f.nexapp) Pharm_Appoint_WeekNO,
	v.NumericVLResult LastVLResult, v.ResultDate VL_Order_Date,d.GenericIngredients CurrentRegimen,--DATENAME(weekday, p.nexapp) [WeekDay],-- DATEPART(DAY, p.nexapp) [WeekDay],
	reg.PhoneContact, reg.PhoneNumber, reg.MobilePhoneNumber, reg.HousePlotNumber,reg.CommunityName, reg.StreetName
    from p_active p
	join Reg on p.PatientId = Reg.PatientId 
	left join startdates s on p.patientid = s.patientid 
	left join clienthivsummary h on p.patientid = h.patientid
	left join transferins t on p.patientid = t.patientid
	left join patinactive i on p.patientid = i.patientid
	left join pharm_active f on p.PatientId = f.PatientId 
	left join Viraload v on p.PatientId = v.PatientId
	left join drugs d on p.PatientId = d.PatientId
	
	--left join p_active3 p3 on p.patientid = p3.patientid
	--left join patinactive3 i3 on i3.patientid = p.patientid
--where (i.pstatus is null or i.visitdate < p.lastvisit ) and (h.artstartdate is not null or s.firstpharm is not null) and (h.artstartdate <= @qtr2_enddate or s.firstpharm<=@qtr2_enddate) 
		where (i.pstatus is null or i.visitdate < p.lastvisit ) 
		and (h.artstartdate is not null or s.firstpharm is not null) 
		and (p.nexapp between @qtr2_startdate and @qtr2_enddate or f.nexapp between @qtr2_startdate and @qtr2_enddate)-- and p.PatientId = 1669865
		AND DATEDIFF(DAY, Reg.DateOfBirth, @EndDate)/365.25 < 15
		order by p.nexapp
-- and (h.artstartdate <= @qtr2_enddate or s.firstpharm<=@qtr2_enddate)
--end