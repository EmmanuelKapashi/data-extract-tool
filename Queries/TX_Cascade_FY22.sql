use cdc_fdb_db;
set ansi_nulls on;
set quoted_identifier on;

declare   
	@StartDate  DATETIME,
    @EndDate DATETIME,
	@PrevStart datetime,
	@PrevEnd datetime,
	@PId int,
	@DId int,
	@FId varchar(10),
    @QP nvarchar(10),
	@LTFUThreshold int,
	@PN varchar(100),
	@DN varchar(100),
	@FN varchar(100),
	@FG nvarchar(100),
	@NHC varchar(10),
	@OHC nvarchar(10)
--set start and end dates
set @StartDate = '20211001'
set @EndDate = eomonth(@StartDate, 2)
set @EndDate = dateadd(second, 86399, @EndDate)
set @PrevStart = dateadd(month, -1, @StartDate)
set @PrevEnd = eomonth(@PrevStart)
set @PrevEnd = dateadd(second, 86399, @PrevEnd)
set @QP = (
		CASE DATEDIFF(MONTH, @StartDate, @EndDate) 
			WHEN 0 THEN convert(varchar(6), @StartDate,112) 
			WHEN 2 THEN 
				CASE
					WHEN  DATEPART(MONTH, @StartDate) IN (10, 11, 12) THEN CONCAT('FY', RIGHT((DATENAME(YEAR, @StartDate) +1), 2), 'Q1')
					WHEN  DATEPART(MONTH, @StartDate) NOT IN (10, 11, 12) THEN CONCAT('FY', RIGHT(DATENAME(YEAR, @StartDate), 2), 'Q', (DATEPART(QUARTER, @StartDate)+1))
				END
			WHEN 11 THEN CONCAT('12M ', CONVERT(VARCHAR(6), @EndDate, 112))
			WHEN 12 THEN CONCAT('12M ', CONVERT(VARCHAR(6), @EndDate, 112))
			ELSE 'UNDEFINED'
		END
)
set @LTFUThreshold = 30
set @FG = (select [Value] from Setting where [Name] = 'FacilityGuid')
set @PId = (select [Value] from setting where [Name] = 'ProvinceId')
set @DId = (select [Value] from setting where [Name] = 'DistrictId')
set @FId = (select [Value] from setting where [Name] = 'HmisCode')

SELECT TOP 1 @PN = p.Name, @DN=d.Name, @FN=f.FacilityName, @NHC=M.NewHMISCode, @OHC=M.OldHMISCode FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG;

IF(OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId INTO #CurrentFacility FROM FacilitySubSite 
	where FacilityGuid = @FG;

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(object_id('tempdb..#AllCurrPrEPClients') IS NOT NULL) DROP TABLE #AllCurrPrEPClients;
if(object_id('tempdb..#TX_Curr_Prev') is not null) drop table #TX_Curr_Prev
if(object_id('tempdb..#TX_New_Prev') is not null) drop table #TX_New_Prev
if(object_id('tempdb..#TX_New') is not null) drop table #TX_New
if(object_id('tempdb..#Cohort') is not null) drop table #Cohort
if(object_id('tempdb..#CohortFin') is not null) drop table #CohortFin
if(object_id('tempdb..#CohortFinale') is not null) drop table #CohortFinale
if(object_id('tempdb..#VIP') is not null) drop table #VIP
if(object_id('tempdb..#Active') is not null) drop table #Active
if(object_id('tempdb..#Phones') is not null) drop table #Phones
if(object_id('tempdb..#DataSet') is not null) drop table #DataSet
if(object_id('tempdb..#NewOnArt') is not null) drop table #NewOnArt;
--if(object_id('tempdb..#X') is not null) drop table #X
--if(object_id('tempdb..#X') is not null) drop table #X
    
CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
create index DiscontinuedPats on #Discontinued(PatientId)

CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
create index DeadPats on #DeadClients(PatientId)


CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);


CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);

CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId       INT PRIMARY KEY,
    InteractionDate DATE
);

CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
    
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
create index CurrentPats on #CurrentOnArt(PatientId)

CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);
create index PrepPats on #AllCurrPrEPClients (PatientId)

CREATE TABLE #NewOnArt(
	PatientId			INT,
	ArtStartDate		DATETIME,
	AgeAtArtStart		INT,
	Sex					VARCHAR(1),
	ArtStartEditLocation INT,
	IhapDate			DATETIME,
	IhapEditLocation	INT,
	OldestHivPosTestDate DATETIME,
	RoC_Status			VARCHAR(50),
	TX_ML_Date			DATETIME,
	NUPN				VARCHAR(50),
	ArtNumber			VARCHAR(50),
	HtsToArtStartDays	INT,
	QcFlag				INT
);


exec DataTech.dbo.proc_GetTxCurr_Base
	@PrevStart,
	@PrevEnd;

EXEC DataTech.dbo.proc_GetTxNew_base
	@PrevStart,
	@PrevEnd;

select * into #TX_Curr_Prev from #CurrentOnArt

-- TX New Previous period
SELECT 
	PatientId, 
	ArtStartDate, 
	AgeAtArtStart AgeArtStart, 
	Sex
INTO #TX_New_Prev
FROM #NewOnArt;

CREATE INDEX Indx_TX_New_Prev_PatientId ON #TX_New_Prev (PatientId);


IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) truncate TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) truncate TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) truncate TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) truncate TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) truncate TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) truncate TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) truncate TABLE #CurrentOnArt;
IF(object_id('tempdb..#AllCurrPrEPClients') IS NOT NULL) truncate TABLE #AllCurrPrEPClients;
IF(object_id('tempdb..#NewOnArt') IS NOT NULL) truncate TABLE #NewOnArt;
IF(object_id('tempdb..#BfMothers') IS NOT NULL) DROP TABLE #BfMothers;

exec DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate;

EXEC DataTech.dbo.proc_GetTxNew_base
	@StartDate,
	@EndDate;

-- BreastFeeding status
SELECT DISTINCT * 
INTO #BfMothers
FROM (
		SELECT * FROM (
				SELECT *,
				ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY VisitDate DESC) seq
				FROM [dbo].[fn_MotherBreastfeedingStatus](@StartDate, @EndDate, @FId)
		) a
			WHERE a.seq = 1
) b;

-- TX New current period
SELECT 
	c.PatientId, 
	c.ArtStartDate, 
	c.AgeAtArtStart AgeArtStart, 
	c.Sex,
	CASE 
		WHEN bf.PatientId IS NOT NULL THEN 1
		ELSE 0
	END IsBF
INTO #TX_New
FROM #NewOnArt c
LEFT JOIN #BfMothers bf ON c.PatientId = bf.PatientId; 

CREATE INDEX Indx_TX_New_PatientId ON #TX_New (PatientId);

--Discontinued patients includes those made inactive and transfers out
IF(OBJECT_ID('tempdb..#TX_ML') IS NOT NULL) DROP TABLE #TX_ML
SELECT PatientId, DATEADD(dd, DATEDIFF(dd, 0, VisitDate), 0) VisitDate, DiscontinuationReason
INTO #TX_ML FROM (
		SELECT 
			PatientId,
            VisitDate,
            ROW_NUMBER() OVER (PARTITION BY patientid ORDER BY VisitDate DESC) seq,
			CASE
				WHEN ((PatientMadeInactive = 1 and PatientMadeInactiveReason <>2) and ((PatientTransferOut is null or PatientTransferOut = 0) and PatientStoppedART <> 1))
					THEN 'InActive'
				when (PatientMadeInactive = 1 and PatientMadeInactiveReason =2) or PatientStoppedART =1 
					then 'StoppedART'
				WHEN (PatientTransferOut = 1) and (PatientMadeInactive = 0 or PatientMadeInactive is null or PatientStoppedART = 0 or PatientStoppedART is null)
					THEN 'TO'
				WHEN (PatientMadeInactive = 1 OR PatientStoppedART = 1) and PatientTransferOut = 1
					THEN 'InActive/TO'
			END DiscontinuationReason
		FROM crtPatientStatus
			WHERE (PatientMadeInactive = 1 OR PatientTransferOut = 1 or PatientStoppedART =1)
			and PatientDied <>1
			AND VisitDate <= @EndDate
			AND EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
		) g
WHERE seq = 1;
CREATE INDEX Indx_Discontinued_PatientId ON #TX_ML (PatientId);


select distinct PatientId into #VIP from (
	select distinct PatientId from #TX_Curr_Prev
	union
	select distinct PatientId from #CurrentOnArt
	union
	select distinct PatientId from #TX_New_Prev
	union
	select distinct PatientId from #TX_New
) Pats;

with ServiceIds as (
select ServiceCode from ServiceCodes 
	where Deprecated = 0	
	and (ServiceCategoryID = 4 --ART Service category
	and (ServiceName not like '%Case%Surveillance' and ServiceName not like 'Pre-Exposure Prophylaxis%' and ServiceName not like 'Cancer%' 
	and ServiceName not in ('Patient Locator', 'ART ARV Eligibility', 'ART ARV Initiation') and ServiceName not like '%Patient Status' 
	and ServiceName not like 'HIV-TB%' and ServiceName not like '%ARV Initiation' and ServiceName not like '%ARV Eligibility' and ServiceName not like '%Adherence'))
	--include MCH EMTCT visits
), -- Service codes of ART IHP and Clinical Visit forms

ClinicalVisits as (
select distinct  PatientReportingId, convert(date, InteractionDate) VisitDate, convert(date, NextVisitDatm) NextVisitDate from Interaction
	where ServiceCode in (select * from ServiceIds)
), --All interactions of type in 'ServiceIds'

CVofInterest as (
select rank() over(partition by PatientReportingId order by convert(date, VisitDate) desc) Visit_Count, * from ClinicalVisits
), --Adding a count to Interactions

MyCVs as (
select PatientReportingId PID, VisitDate, NextVisitDate, datediff(day, NextVisitDate, @EndDate) DaysLate from CVofInterest where Visit_Count = 1
), --Latest visit per client

AllARVPharms as (
select rank() over(partition by PatientId order by convert(date, VisitDate) desc) VisitCount, * from 
	(select PatientId, convert(date, VisitDate) VisitDate, convert(date, NextAppointmentDate) NextVisitDate from crtPharmacyVisit where ArvDrugDispensed = 1)A
), --All ART pharmacy dispensations

Pharms as (
select *, datediff(day, NextVisitDate, @EndDate) DaysLate  from AllARVPharms where VisitCount = 1
) --Latest ART Pharmacy Visits per client


-- Getting latest ART Pharmacy or ART Clinical Visit per client
select PatientId, max(VisitDate) VisitDate, max(NextVisitDate) AppointmentDate, min(DaysLate) DaysLate into #Active from (
	select PatientId, VisitDate, NextVisitDate, DaysLate from Pharms
	union
	select * from MyCVs
) X
	group by PatientId;


--Cohort of Clients being reviewed
select distinct 
	C.PatientId, R.Sex, R.DateOfBirth, datediff(month, cs.ArtStartDate, @EndDate) DurationOnART, 
	case when A.VisitDate is not null then datediff(day, R.DateOfBirth, A.VisitDate)/365.25 else datediff(day, R.DateOfBirth, @EndDate)/365.25 end AgeLastVisit, 
	A.DaysLate, X.DiscontinuationReason,
	case when C.PatientId in (select distinct PatientId from #TX_Curr_Prev) then 1 else 0 end TX_Curr_m0, --TX_Curr previous month
	case when C.PatientId in (select distinct PatientId from #TX_New_Prev) then 1 else 0 end TX_New_m0, --TX_New previous month
	case when C.PatientId in (select distinct PatientId from #CurrentOnArt) then 1 else 0 end TX_Curr_m1, --TX_Curr current month
	case when C.PatientId in (select distinct PatientId from #TX_New) then 1 else 0 end TX_New_m1, --TX_New current month
	CASE WHEN C.PatientId in (select distinct PatientId from #TX_New WHERE IsBF = 1) then 1 else 0 end TX_New_m1_BF, --TX_New_BF current month
	case when C.PatientId in (select distinct PatientId from crtPatientLocator where PatientTransfer = 2 and VisitDate between @StartDate and @EndDate) then 1 else 0 end TransIn
into #Cohort from #VIP C
left join (
	select distinct * from #TX_ML where VisitDate between @PrevStart and @EndDate 
	union 
	select distinct * from #DeadClients where VisitDate between @PrevStart and @EndDate) X on C.PatientId = X.PatientId
left join #Active A on C.PatientId = A.PatientId
left join crtRegistrationInteraction R on C.PatientId = R.patientId_int
left join ClientHivSummary cs on C.PatientId = cs.PatientId



select *,
case
	when DurationOnART >= 0 and DurationOnART < 3 then '<3 months'
	when DurationOnART >= 3 and DurationOnART < 6 then '3-5 months'
	when DurationOnART >= 6 then '6+ months'
end DurationOnARTCat,
case 
	when TX_Curr_m0 = 0 and TX_Curr_m1 = 1 and TX_New_m1 = 0 and TransIn = 0 then 1
	else 0
end RTT,
case 
	when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 
		and ((PatientId in (select PatientId from crtPatientStatus where PatientDied = 1 and PatientDiedDate between @StartDate and @EndDate)) 
				or DiscontinuationReason = 'Death') then 1
	else 0 
end Died,
case 
	when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason in ('InActive', 'InActive/TO') then 1
	when TX_Curr_m1 = 0 and TX_Curr_m0 = 0 and TX_New_m1 = 1 and DiscontinuationReason in ('InActive', 'InActive/TO') then 1
	when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason is null then 1
	when TX_Curr_m1 = 0 and TX_Curr_m0 = 0 and TX_New_m1 = 1 and DiscontinuationReason is null then 1
	else 0
end MadeInactive,
case
	when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason = 'StoppedART' then 1 else 0
end StoppedART,
case when  TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason = 'TO' then 1 else 0 end TransOut,
case when TX_Curr_m0 = 1 and TX_Curr_m1 = 1 
	and datepart(month, DateOfBirth) = datepart(month, @PrevStart) and ((AgeLastVisit >=15.0 and AgeLastVisit <16.0) or (AgeLastVisit>=20.0 and AgeLastVisit <21.0))
	then 1 else 0 end Graduated, 
case
	when AgeLastVisit >= 0 and AgeLastVisit < 1 then '<01'
	when AgeLastVisit >= 1 and AgeLastVisit < 5 then '''01-04'
	when AgeLastVisit >= 5 and AgeLastVisit < 10 then '''05-09'
	when AgeLastVisit >= 10 and AgeLastVisit < 15 then '''10-14'
	when AgeLastVisit >= 15 and AgeLastVisit < 20 then '15-19'
	when AgeLastVisit >= 20 and AgeLastVisit < 25 then '20-24'
	when AgeLastVisit >= 25 and AgeLastVisit < 30 then '25-29'
	when AgeLastVisit >= 30 and AgeLastVisit < 35 then '30-34'
	when AgeLastVisit >= 35 and AgeLastVisit < 40 then '35-39'
	when AgeLastVisit >= 40 and AgeLastVisit < 45 then '40-44'
	when AgeLastVisit >= 45 and AgeLastVisit < 50 then '45-49'
	when AgeLastVisit >= 50 and AgeLastVisit < 55 then '50-54'
	when AgeLastVisit >= 55 and AgeLastVisit < 60 then '55-59'
	when AgeLastVisit >= 60 and AgeLastVisit < 65 then '60-64'
	when AgeLastVisit >= 65 then '65+'
	else 'Uncategorised'
end AgeCatFine,
case
	when AgeLastVisit <15 then '<15'
	when AgeLastVisit >= 15 and AgeLastVisit < 20 then '15-19'
	when AgeLastVisit >=20 then '20+'
end AgeCatCourse
into #CohortFin from #Cohort

--*************************** TX_RTT cliens that where LTFU for 12 months or more **************************************
if(object_id('tempdb..#TXRTTCohort') is not null) drop table #TXRTTCohort
select PatientId into #TXRTTCohort from #CohortFin where RTT = 1 --and DurationOnART >= 12

if(object_id('tempdb..#TXRTTCohortLTFU12') is not null) drop table #TXRTTCohortLTFU12


--latest ARV pickups in the period for all clients of interest
IF (OBJECT_ID('tempdb..#Pharms') IS NOT NULL) DROP TABLE #Pharms
IF (OBJECT_ID('tempdb..#Clinicals') IS NOT NULL) DROP TABLE #Clinicals

;with RTTCohort as (
		select distinct PatientId from #CohortFin where RTT = 1
),

AllClinicals as (
		SELECT c.PatientId, InteractionId, InteractionDate
			from (select *, row_number() over (partition by PatientId order by InteractionDate desc) Row_Count from 
				(select * from ClientClinicalCareDates 
					where ArtOrNonArtClinicalCare = 1
					and EditLocation  in (select distinct * from #CurrentFacility)
					and InteractionDate < @StartDate
					and NextVisitDate < @StartDate
					and VisitType not in (24, 25, 28, 29, 38, 40, 41, 42, 44, 59, 329, 333, 344, 352, 353, 354, 355, 356, 367, 359, 402, 590)) V) as c 
					inner join #TXRTTCohort i ON c.PatientId = i.PatientId
			where Row_Count = 1
),

AllPharms as (
		select PatIdPharm, InteractionID, VDate from (
				select PatientId as PatIdPharm, InteractionID, convert(date, VisitDate) VDate, Visit_Count from (
						select *, row_number() over (partition by PatientId order by VisitDate desc) Visit_Count from crtPharmacyVisit
							where ArvDrugDispensed = 1
							and EditLocation in (select distinct  * from #CurrentFacility)
							and VisitDate < @StartDate
							and NextAppointmentDate < @StartDate
				) P
					where Visit_Count = 1
		)P1
		inner join #TXRTTCohort c on P1.PatIdPharm=c.PatientId
			
),

AllVisits as (
		select row_number() over(partition by PatientId order by InteractionDate desc) rc, * from (
				select top 0 * from AllClinicals
				union
				select * from AllPharms
		) X
),

RTT_LTFU12 as (
		select A.PatientId, A.InteractionDate, A.InteractionId, datediff(day, i.NextVisitDatm, c.VisitDate)*12/365.25 MonthsSinceLTFU 
		from AllVisits A 
		inner join #Active c on A.PatientId = c.PatientId and A.rc = 1
		inner join Interaction i on A.PatientId = i.PatientReportingId and A.InteractionId = i.InteractionID
			
)

select distinct 
	a.PatientId PatId, 
	b.MonthsSinceLTFU,
	case
		when b.MonthsSinceLTFU <3.0 then '<3 Months'
		when b.MonthsSinceLTFU >=3.0 AND b.MonthsSinceLTFU <6.0 then '3-5 Months'
		when b.MonthsSinceLTFU >=6.0 then '>=6 Months'
		else 'Uncategorised'
	end RTT_LTFU_Cat
into #TXRTTCohortLTFU12 
from RTTCohort a
left join RTT_LTFU12 b on a.PatientId = b.PatientId;

CREATE INDEX Indx_TXRTT_LTFU_PatientId ON #TXRTTCohortLTFU12 (PatId);

select DISTINCT
	a.*, 
	case
		when a.RTT = 1 and b.RTT_LTFU_Cat = '<3 Months' then 1
		else 0
	end RTT_LessThan_3,
	case
		when a.RTT = 1 and b.RTT_LTFU_Cat = '3-5 Months' then 1
		else 0
	end RTT_3_TO_5,
	case
		when a.RTT = 1 and b.RTT_LTFU_Cat = '>=6 Months' then 1
		else 0
	end RTT_6_Plus,
	case
		when a.RTT = 1 and b.RTT_LTFU_Cat = 'Uncategorised' then 1
		else 0
	end RTT_Unk
into #CohortFinale
from #CohortFin a
left join #TXRTTCohortLTFU12 b on a.PatientId = b.PatId
/*
select RTT_LTFU_Cat, count(PatId) N from #TXRTTCohortLTFU12 group by RTT_LTFU_Cat
*/

--*************************** End TX_RTT cliens that where LTFU for 12 months or more **************************************
/*
select distinct * into #Phones from (
	select row_number() over(partition by PatientGUID order by CreatedTime desc) N, NaturalNumber PatientID, A.PatientGUID,
		case
			when 
				(PhoneContact = '' or PhoneContact is null) 
				and (PhoneNumber = '' or PhoneNumber is null) 
				and (MobilePhoneNumber = '' or MobilePhoneNumber is null) 
				then null
			when 
				(PhoneContact != '' and PhoneContact is not null) 
				and (PhoneNumber = '' or PhoneNumber is null) 
				and (MobilePhoneNumber = '' or MobilePhoneNumber is null) 
				then replace(PhoneContact, ',', '')
			when 
				(PhoneContact = '' or PhoneContact is null) 
				and (PhoneNumber != '' and PhoneNumber is not null) 
				and (MobilePhoneNumber = '' or MobilePhoneNumber is null) 
				then replace(PhoneNumber, ',', '')
			when 
				(PhoneContact = '' or PhoneContact is null) 
				and (PhoneNumber = '' or PhoneNumber is null) 
				and (MobilePhoneNumber != '' and MobilePhoneNumber is not null)
				then replace(MobilePhoneNumber, ',', '')
			when 
				(PhoneContact != '' and PhoneContact is not null) 
				and (PhoneNumber != '' and PhoneNumber is not null) 
				and (MobilePhoneNumber != '' and MobilePhoneNumber is not null)
				then concat(replace(PhoneContact, ',', ''), ' | ', replace(PhoneNumber, ',', ''), ' | ', replace(MobilePhoneNumber, ',', ''))
			when 
				(PhoneContact = '' or PhoneContact is null) 
				and (PhoneNumber != '' and PhoneNumber is not null) 
				and (MobilePhoneNumber != '' and MobilePhoneNumber is not null)
				then concat(replace(PhoneNumber, ',', ''), ' | ', replace(MobilePhoneNumber, ',', ''))
			when 
				(PhoneContact != '' and PhoneContact is not null) 
				and (PhoneNumber = '' or PhoneNumber is null) 
				and (MobilePhoneNumber != '' and MobilePhoneNumber is not null)
				then concat(replace(PhoneContact, ',', ''), ' | ', replace(MobilePhoneNumber, ',', ''))
			when 
				(PhoneContact != '' and PhoneContact is not null) 
				and (PhoneNumber != '' and PhoneNumber is not null) 
				and (MobilePhoneNumber = '' or MobilePhoneNumber is null) 
				then concat(replace(PhoneContact, ',', ''), ' | ', replace(PhoneNumber, ',', ''))
		end PNumber 
	from [Address] A join guidmap g on a.PatientGUID = g.MappedGuid
		where AddressType = 'Current'
) Ads
	where N = 1
--use both PatientGUID and PatientId to link on joins
create index PhonesId on #Phones(PatientId)
*/


if(object_id('tempdb..#SubDataSet') is not null) drop table #SubDataSet
select 
	@PN Province, @DN District, 
	case @FId
		--Chilanga Aliases
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010005T' THEN 'Tubalange Mini Hospital'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'Desai Health Post'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		-- Chirundu Aliases
		-- Luangwa Aliases
		-- Rufunsa Aliases
		WHEN '500700241' THEN 'Lubalashi Health Post'
		else @FN
	end Facility, 
	@FId FacilityCode,
	Sex,
	AgeCatFine,
	AgeCatCourse,
	DurationOnARTCat,
	sum(TX_Curr_m0) TX_Curr_PrevMonth,
	sum(TX_New_m1) TX_New,
	sum(TX_New_m1_BF) TX_New_BF,
	sum(RTT) RTT_total,
	sum(RTT_LessThan_3) RTT_LessThan_3,
	sum(RTT_3_TO_5) RTT_3_TO_5,
	sum(RTT_6_Plus) RTT_6_Plus,
	sum(RTT_Unk) RTT_Unk,
	sum(TransIn) TransIn,
	sum(Died) Died,
	sum(TransOut) TransOut,
	sum(StoppedART) StoppedART,
	sum(MadeInactive) LTFU,
	sum(TX_Curr_m1) TX_Curr_RepMonth,
	sum(Graduated) Graduating
into #SubDataSet 
from #CohortFinale
	group by Sex, AgeCatFine, AgeCatCourse, DurationOnARTCat
	order by Sex, AgeCatFine, AgeCatCourse, DurationOnARTCat;
	
if(object_id('tempdb..#DataSet') is not null) drop table #DataSet
select 
	Province, District, Facility, FacilityCode, Sex, AgeCatFine, AgeCatCourse, 
	sum(TX_Curr_PrevMonth) TX_Curr_PrevMonth, 
	sum(TX_New) TX_New,
	sum(TX_New_BF) TX_New_BF,
	sum(RTT_total) TX_RTT,
	sum(RTT_LessThan_3) RTT_LessThan_3,
	sum(RTT_3_TO_5) RTT_3_TO_5,
	sum(RTT_6_Plus) RTT_6_Plus,
	sum(RTT_Unk) RTT_Unk, 
	sum(TransIn) TransIn,
	sum(Died) Died, 
	sum(TransOut) TransOut, 
	sum(StoppedART) StoppedART, 
	sum(LTFU) LTFU, 
	sum(TX_Curr_RepMonth) TX_Curr_RepMonth,
	sum(Graduating) Graduated
into #DataSet 
from #SubDataSet
	group by Province, District, Facility, FacilityCode, Sex, AgeCatFine, AgeCatCourse;

select DISTINCT
	@QP [Period],
	d.Province, 
	d.District, 
	d.Facility, 
	d.FacilityCode, 
	d.Sex, 
	d.AgeCatFine,
	d.AgeCatCourse,
	d.TX_Curr_PrevMonth, 
	d.TX_New, 
	d.TX_New_BF, 
	(d.TX_RTT - d.RTT_Unk) TX_RTT,
	d.RTT_LessThan_3,
	d.RTT_3_TO_5,
	D.RTT_6_Plus,
	d.RTT_Unk Silent_TransIn, 
	d.TransIn, 
	d.Died, 
	d.TransOut, 
	d.StoppedART, 
	case when a.LTFU is not null then a.LTFU else 0 end LTFU_Less_Than_3Months_On_ART, 
	case when b.LTFU is not null then b.LTFU else 0 end LTFU_3_to_5_Months_On_ART,
	case when c.LTFU is not null then c.LTFU else 0 end LTFU_6_Months_Plus_On_ART,
	d.TX_Curr_RepMonth,
	d.Graduated
from #DataSet d
left join (select * from #SubDataSet where DurationOnARTCat = '<3 months') a on d.Sex = a.Sex and d.AgeCatFine = a.AgeCatFine
left join (select * from #SubDataSet where DurationOnARTCat = '3-5 months') b on d.Sex = b.Sex and d.AgeCatFine = b.AgeCatFine
left join (select * from #SubDataSet where DurationOnARTCat = '6+ months') c on d.Sex = c.Sex and d.AgeCatFine = c.AgeCatFine;
