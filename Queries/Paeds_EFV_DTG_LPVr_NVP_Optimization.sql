--1.	Patients that started treatment between oct 2018 to Jan 2019 in Lusaka urban district --done
--2.	For those patients, I need all their bio and demographic data
--3.	In addition, please share their status at 12 months post treatment � use the 30 days late definition for ltfu  --done
--4.	For these patients, kindly pull all their associated lab results
--5.	Also pull their first and last drug regimen in 12 months period

use cdc_fdb_db;

declare 
	@startdate dateTIME,
	@enddate dateTIME,
    @QP varchar(10),
	@ProvinceId VARCHAR(2),
	@DistrictId VARCHAR(2),
	@FacilityGUID varchar(50),
	@LoginName VARCHAR(50),
	@LTFUThreshold INTEGER = 30,
	@hmiscode varchar(10),
	@provincename varchar(100),
	@districtname varchar(100),
	@facilityname varchar(100),
	@NHC VARCHAR(9),
	@OHC VARCHAR(9);


SET @StartDate  = '20210701';
SET @EndDate = EOMONTH(@StartDate, 0);
SET @EndDate = dateadd(second, 86399, @EndDate);
set @QP = (
		CASE DATEDIFF(MONTH, @StartDate, @EndDate) 
			WHEN 0 THEN convert(nvarchar(6), @StartDate,112) 
			WHEN 2 THEN 
				CASE
					WHEN  DATEPART(MONTH, @StartDate) IN (10, 11, 12) THEN CONCAT('FY', RIGHT((DATENAME(YEAR, @StartDate) +1), 2), (DATEPART(QUARTER, @StartDate)+1))
					WHEN  DATEPART(MONTH, @StartDate) NOT IN (10, 11, 12) THEN CONCAT('FY', RIGHT(DATENAME(YEAR, @StartDate), 2), 'Q', (DATEPART(QUARTER, @StartDate)+1))
				END
			ELSE 'UNDEFINED'
		END
);

set @hmiscode = (select value from setting where name = 'hmiscode')
set @ProvinceId = (select max([Value]) v from setting where [Name] = 'ProvinceId')
set @DistrictId = (select max([Value]) v from setting where [Name] = 'DistrictId')
set @FacilityGUID = (select max([Value]) v from setting where [Name] = 'FacilityGUID')

SELECT @provincename = p.Name, @districtname =d.Name, @facilityname =f.FacilityName, @NHC=M.NewHMISCode, @OHC=M.OldHMISCode FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FacilityGUID;
	

DECLARE
		@PID2 VARCHAR(2) = '50',
		@DID2 VARCHAR(2) = '33',
		--@CFID VARCHAR(50),
		@FID2 VARCHAR(9);

	--SET @CFID = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGUID')
	SET @FID2 = (
				CASE @FacilityGUID
					WHEN 'tvhd4akqn785db9xjaxk8oqrlb1lka08' THEN '500400251' --Chikoka
					WHEN 'g0gdw9d53dm32raz0h46vrsqgbr9417l' THEN '500400253' --Kafue East
					WHEN 'tuzn5ssrponmtkuw06wsr1plserg46h3' THEN '500400251' --Mtendere
					WHEN '7f2pvfn4p4t60gs4uez20t6hxvjm2ftv' THEN '500400021' --Mungu
					WHEN 'uk1oj1tva6bjmiknoj664bin2bkkdjn8' THEN '500400260' --Kabweza
					WHEN 'n48r3u184v9ty34953tr4fduagd12wfv' THEN '500400080' --Old Kabweza
				END
	)

IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode)
UNION
SELECT SubSiteId FROM dbo.fn_GetFacilitySubSiteIds(@PID2, @DID2, @FID2);



IF OBJECT_ID('tempdb..#Discontinued') IS NOT NULL
        DROP TABLE #Discontinued;
    IF OBJECT_ID('tempdb..#DeadClients') IS NOT NULL
        DROP TABLE #DeadClients;
    IF OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL
        DROP TABLE #MostRecentHIVStatusAsOfEndDate;
    IF OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL
        DROP TABLE #CurrentOnARTByClinicalVisit;
    IF OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL
        DROP TABLE #CurrentOnARTByDrugs;
    IF OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL
        DROP TABLE #CurrentOnARTByVisitAndDrugs;
    IF OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL
        DROP TABLE #CurrentOnArt;
    IF (object_id('tempdb..#AllCurrPrEPClients')) IS NOT NULL
        DROP TABLE #AllCurrPrEPClients;
	IF (object_id('tempdb..#MERQ1')) IS NOT NULL
        DROP TABLE #MERQ1;



    CREATE TABLE #Discontinued
    (
        PatientId             INT,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #DeadClients
    (
        PatientId             INT PRIMARY KEY,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #MostRecentHIVStatusAsOfEndDate
    (
        PatientId  INT PRIMARY KEY,
        TestResult INT,
        TestDate   DATE
    );
    CREATE TABLE #CurrentOnARTByClinicalVisit
    (
        PatientId             INT PRIMARY KEY,
        Sex                   VARCHAR(1),
        InteractionDate       DATE,
        DateOfBirth           DATE,
        AgeAsAtEnd            INT,
        AgeArtStart           INT,
        CurrentHivStatus      INT,
        CurrentHivStatusDate  DATE,
        OldestHivPosTestDate  DATE,
        ArtStartDate          DATE,
        ArtStartLocation      VARCHAR(9),
        ArtStartEditLocation  INT,
        CareStartDate         DATETIME,
        CareStartLocation     VARCHAR(9),
        CareStartEditLocation INT
    );
    CREATE TABLE #CurrentOnARTByDrugs
    (
        PatientId       INT PRIMARY KEY,
        InteractionDate DATE
    );
    CREATE TABLE #CurrentOnARTByVisitAndDrugs
    (
        PatientId                    INT PRIMARY KEY,
        InteractionDate              DATE,
        DeadBeforeEnd                BIT,
        NotActive                    BIT,
        DeadDate                     DATE,
        DiscontinuationDateBeforeEnd DATE,
        CurrentHivStatus             INT,
        DateOfBirth                  DATE,
        CareStartDate                DATE,
        ArtStartDate                 DATE,
        OldestHivPosTestDate         DATE,
        ArtStartEditLocation         INT,
        Sex                          VARCHAR(1),
        AgeAsAtEnd                   INT,
        AgeArtStart                  INT
    );
    CREATE TABLE #CurrentOnArt
    (
        PatientId    INT PRIMARY KEY,
		EditLocation INT,
        Age          INT,
        AgeLastVisit INT,
        Sex          VARCHAR(1),
        ArtStartDate DATE
    );
	    CREATE TABLE #AllCurrPrEPClients
    (
        PatientId         INT PRIMARY KEY,
        Sex               VARCHAR(1),
        Age               INT,
        InitVisitDate     DATE,
        FollowUpVisitDate DATE
    );

EXEC DataTech.[dbo].[proc_GetTxCurr_base]  
	@StartDate, 
	@enddate, 
	@LTFUThreshold;

 IF (object_id('tempdb..#TX_New_ClientsOfInterest')) IS NOT NULL
			DROP TABLE #TX_New_ClientsOfInterest;
		SELECT distinct chs.PatientId, chs.ArtStartDate, dbo.fn_age(reg.DateOfBirth, chs.ArtStartDate) AgeArtStart, dbo.fn_age(reg.DateOfBirth, @EndDate) Age, chs.Sex
		INTO #TX_New_ClientsOfInterest
		FROM #CurrentOnArt chs 
		JOIN crtRegistrationInteraction reg ON chs.PatientId = reg.patientId_int
		left join crtPatientLocator loc on chs.PatientId = loc.PatientId
		WHERE ArtStartDate IS NOT NULL
		  AND ArtStartDate >= @StartDate
		  AND ArtStartDate <= @EndDate
		  --filter out transfer in AND remove records WHERE clients started ART in diferent facility 
		  AND (loc.PatientTransfer <> 2 OR loc.PatientTransfer is null)
		  AND loc.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility);
 
 if (object_id('tempdb..#nottocontinue')) IS NOT NULL
        drop table #nottocontinue
    select PatientId, VisitDate, DiscontinuationReason
    into #nottocontinue
    FROM #Discontinued
	union
	SELECT PatientId, VisitDate, DiscontinuationReason 
	FROM #DeadClients;
	
 if (object_id('tempdb..#reg')) IS NOT NULL
			 drop table #reg	
	select patientId_int PatientId, max(ArtNumber)ArtNumber,max(r.FirstName) FirstName, max(r.SurName) SurName, max(r.DateOfBirth) DateOfBirth, max(r.Sex) Sex, max(PhoneContact) PhoneContact, max(PhoneNumber) PhoneNumber, 
			max(MobilePhoneNumber) MobilePhoneNumber, max(HousePlotNumber) HousePlotNumber, max(StreetName) StreetName, max(CommunityName) CommunityName, max(CommunityNickname) CommunityNickname
		into #reg
		from crtRegistrationInteraction r
			join GuidMap g on r.patientId_int = g.NaturalNumber
			--join registration r2 on g.OwningGuid = r2.PatientGUID --Test correct guid 3406755
			left join [Address] a on g.MappedGuid = a.PatientGUID and AddressType = 'Current'
		group by r.patientId_int

		 alter table #TX_New_ClientsOfInterest add MaturityDate datetime
		
			update #TX_New_ClientsOfInterest
			set MaturityDate = DATEADD(mm, 12, Artstartdate)
			FROM #TX_New_ClientsOfInterest c
					 

	if object_id('tempdb..#disps') is not null
	begin
	drop table #disps
	end

	if object_id('tempdb..##dispsfinal') is not null
	begin
	drop table ##dispsfinal
	end
	--select * from DrugProductsCachedView where BrandName = 'Avonza'
	select 
		v.patientid,
		visitdate, v.NextAppointmentDate, v.InteractionID,
		--MedDrugId, 
		d.Synonym, d.GenericIngredients,
		case when GenericName = 'Tenofovir, Lamivudine and Efavirenz' then 'TDF + 3TC + EFV' else
		SUBSTRING(d.synonym, 1,case CHARINDEX(',',d.synonym)+CHARINDEX('-',d.synonym) when 0 then LEN(d.Synonym) else CHARINDEX(',',d.synonym)+CHARINDEX('-',d.synonym)-1 end)
		end AS genericcomb1,
		Dense_RANK()over(partition by patientid order by convert(varchar(8),v.visitdate,112) desc)
		as disp_count
		,d.ComponentCount
		,location
	into #disps
	from crtPharmacyVisit v
	join crctPharmacydispensation p on v.InteractionID = p.InteractionID
	join DrugProductsCachedView d on p.PhysicalDrugId = d.MedDrugId and d.DrugClass='Antiretrovirals'
	--join ClientArvsDate c on v.PatientId = c.PatientId
	where v.ArvDrugDispensed = 1 --and  patientid = 1117905
	order by PatientId, 5,4
	--select * from crtPharmacyVisit where ComponentCount  < 3
				 
	
	if object_id('tempdb..#longsingle') is not null
	begin
	drop table #longsingle
	end
	select patientid, VisitDate,NextAppointmentDate,[Location],InteractionID, ComponentCount,disp_count, split.a.value('.', 'VARCHAR(100)') comp
	into #longsingle
	from (
				select 
				*, CAST('<m>'+REPLACE(genericcomb1,'+','</m><m>')+'</m>' as XML) DATA 
				from #disps
		) A CROSS APPLY DATA.nodes ('/m') as split(a);


		if object_id('tempdb..#longsingle1') is not null
		begin
		drop table #longsingle1
		end
		select * into #longsingle1 from
		(
		select patientid,ComponentCount ,STUFF((select distinct '+' + i.comp 
	    from #longsingle i where i.patientid = l.patientid and i.disp_count=l.disp_count 
		order by 1 for xml path('')), 1,1,'') Regimen, l.VisitDate PharmacyVisitDate,
		row_number() over(partition by l.PatientId order by l.VisitDate desc) seq--,
		from #longsingle l
		) x where seq = 1	
		
		
		 IF OBJECT_ID('tempdb..#viralload') IS NOT NULL
			DROP TABLE #viralload

			select o.PatientId, r.InteractionID, o.LabOrderDate ViralLoadOrderDate, r.InteractionDate ResultDate, o.InteractionDataEntryLocation, r.LabTestID, 
	       LabTestValue ViralLoadResult,
	       case when ISNUMERIC(r.LabTestValue) = 1 then convert(decimal(10,0), cast(r.LabTestValue as float)) end NumericVLResult,
		   case when ISNUMERIC(r.LabTestValue) = 0 then r.LabTestValue end NonNumericVLResult,
		   row_number()over(partition by o.patientid order by o.laborderdate desc) lastvisit 
			into #viralload
			from (
				  select o.PatientId, o.InteractionID, o.EditLocation, o.EditLocationSeqNumber, o.InteractionDate, o.InteractionDataEntryLocation,
						 d.LabOrderDate, d.LabTestID
				  from crtLaboratoryOrders o
				  join crctLaboratoryOrderDetails d on (o.InteractionID = d.InteractionID and o.EditLocation = d.EditLocation and o.EditLocationSeqNumber = d.EditLocationSeqNumber)
				  ) o
			left join (
					   select r.PatientId, r.InteractionID, r.EditLocation, r.EditLocationSeqNumber, r.InteractionDate, r.InteractionDataEntryLocation,
							  d.LabTestID, d.LabTestValue
					   from crtLaboratoryResults r
					   join crctLaboratoryResultDetails d on (r.InteractionID = d.InteractionID and r.EditLocation = d.EditLocation and r.EditLocationSeqNumber = d.EditLocationSeqNumber)
					   ) r
			on o.PatientId = r.PatientId and o.InteractionID =r.InteractionID and o.LabTestID = r.LabTestID
			where 
			o.LabTestID = 102 and 
			LabTestValue not in ('.', '.39', '-')

		IF OBJECT_ID('tempdb..#viralload2') IS NOT NULL
		DROP TABLE #viralload2
	    
		select *
			into #viralload2
			from(select *,
				 case when datediff(dd, convert(date, ViralLoadOrderDate, 103), @enddate) <= 365.25 and ViralLoadResult is not null then 'Yes'
					  when ViralLoadOrderDate is null and ViralLoadResult is not null then 'Unknown'
					  when ViralLoadOrderDate is null and ViralLoadResult is null then 'No'
				 else 
					  'No' 
				 end HasViralLoadInLast12Months,
				 case 
					when (NumericVLResult < 1000 and NonNumericVLResult is null)
						or (NumericVLResult is null and NonNumericVLResult in ('Not detected')) 
						then 'Suppressed' 
				    when (NumericVLResult is null and NonNumericVLResult in ('INSUF'))
						or (NumericVLResult is null and NonNumericVLResult is null)
						then null
					when (NumericVLResult >= 1000 and NonNumericVLResult is null) then 'Unsuppressed'
				 end VL_status
				 from #viralload 
				 where lastvisit = 1) t where HasViralLoadInLast12Months = 'Yes'

		

		--select * from #CURRENTONART WHERE AGE < 15
		
	
--select distinct VL_status from #viralload2			

		if object_id('tempdb..#PaedsCurrentOnArt') is not null
		drop table #PaedsCurrentOnArt

		select 
			c.PatientId, 
			c.Age,
			CASE 
				WHEN c.Age >= 0 AND c.Age < 1 THEN '<01'
				WHEN c.Age >= 1 AND c.Age < 5 THEN '''01-04'
				WHEN c.Age >= 5 AND c.Age < 10 THEN '''05-09'
				WHEN c.Age >= 10 AND c.Age < 15 THEN '''10-14'
				WHEN c.Age >15 THEN '>=15'
				ELSE 'Uncategorised'
			END AgeCat, 
			c.Sex, 
			c.ArtStartDate, 
			l.PharmacyVisitDate, 
			l.Regimen,
			len(regimen) - len(replace(regimen,'+','')) components
		into #PaedsCurrentOnArt  
		from
		#CurrentOnArt c 
		left join #longsingle1 l on c.PatientId = l.PatientId
		--left join #viralload2 v on c.PatientId = v.PatientId
		where age < 15

		--select * from #PaedsCurrentOnArt where components >2
		--SELECT 
		--		CHARINDEX('+', regimen) position, Regimen  from #PaedsCurrentOnArt

		--		--Select Count(+) From STRING_SPLIT(regimen,' ')  from #PaedsCurrentOnArt;
		--		declare @myvar varchar(20)
		--			set @myvar = 'Hello World'

		--			select len(regimen) - len(replace(regimen,'+','')) components, Regimen from #PaedsCurrentOnArt


if object_id('tempdb..#paedsEFV') is not null
drop table #paedsEFV

if(object_id('tempdb..#paedsEFV_VL') is not null) drop table #paedsEFV_VL
if(object_id('tempdb..#paedsNVP_VL') is not null) drop table #paedsNVP_VL
if(object_id('tempdb..#paedsDTG_VL') is not null) drop table #paedsDTG_VL
if(object_id('tempdb..#paedsLPV_VL') is not null) drop table #paedsLPV_VL
if(object_id('tempdb..#paedsOtherRegimen_VL') is not null) drop table #paedsOtherRegimen_VL
if(object_id('tempdb..#paedsNoRegimen_VL') is not null) drop table #paedsNoRegimen_VL

if object_id('tempdb..#paedsEFV_VlSuppressed') is not null
drop table #paedsEFV_VlSuppressed

if object_id('tempdb..#paedsNVP_VlSuppressed') is not null
drop table #paedsNVP_VlSuppressed

if object_id('tempdb..#paedsDTG_VlSuppressed') is not null
drop table #paedsDTG_VlSuppressed

if object_id('tempdb..#paedsLPV_VlSuppressed') is not null
drop table #paedsLPV_VlSuppressed

					
if object_id('tempdb..#allClients') is not null
drop table #allClients
					

select *  
into #paedsEFV
from #PaedsCurrentOnArt 
	where Regimen like '%EFV%' and components >= 2
--select * from #PaedsCurrentOnArt order by AgeCat

select v.PatientId, p.AgeCat  
into #paedsEFV_VlSuppressed
from #paedsEFV p 
join #viralload2 v on p.PatientId = v.PatientId
	where v.VL_status = 'Suppressed' 

select v.PatientId, P.AgeCat  
into #paedsEFV_VL
from #paedsEFV p 
join #viralload2 v on p.PatientId = v.PatientId
	where v.VL_status is not null 


			 
if object_id('tempdb..#paedsNVP') is not null
drop table #paedsNVP

select *  
into #paedsNVP
from #PaedsCurrentOnArt where Regimen like '%NVP%' and components >= 2

select v.PatientId, p.AgeCat  
into #paedsNVP_VlSuppressed
from #paedsNVP p join #viralload2 v on p.PatientId = v.PatientId
where VL_status = 'Suppressed'

select v.PatientId, p.AgeCat  
into #paedsNVP_VL
from #paedsNVP p join #viralload2 v on p.PatientId = v.PatientId
where VL_status is not null

--select * from #paedsNVP

if object_id('tempdb..#paedsDTG') is not null
drop table #paedsDTG

select *  
into #paedsDTG
from #PaedsCurrentOnArt where Regimen like '%DTG%' and components >= 2

select v.PatientId, p.AgeCat  
into #paedsDTG_VlSuppressed
from #paedsDTG p join #viralload2 v on p.PatientId = v.PatientId
where VL_status = 'Suppressed'

select v.PatientId, p.AgeCat  
into #paedsDTG_VL
from #paedsDTG p join #viralload2 v on p.PatientId = v.PatientId
where VL_status is not null


if object_id('tempdb..#paedsLPV') is not null
drop table #paedsLPV

select *  
into #paedsLPV
from #PaedsCurrentOnArt where Regimen like '%LPV%' and patientid not in (select patientid from #paedsEFV) and  components >= 2

select v.PatientId, p.AgeCat  
into #paedsLPV_VlSuppressed
from #paedsLPV p join #viralload2 v on p.PatientId = v.PatientId
where VL_status = 'Suppressed'

select v.PatientId, p.AgeCat  
into #paedsLPV_VL
from #paedsLPV p join #viralload2 v on p.PatientId = v.PatientId
where VL_status is not null
					
select * into #allClients  from
	(
	select * from #paedsEFV
	union
	select * from #paedsDTG
	union
	select * from #paedsLPV
	union
	select * from #paedsNVP
) x

select v.PatientId, p.AgeCat  
into #paedsOtherRegimen_VL
from #PaedsCurrentOnArt p 
join #viralload2 v on p.PatientId = v.PatientId
	where VL_status is not null
	and p.PatientId not in (select PatientId from #allClients)
	and p.Regimen is not null

select v.PatientId, p.AgeCat  
into #paedsNoRegimen_VL
from #PaedsCurrentOnArt p 
join #viralload2 v on p.PatientId = v.PatientId
	where VL_status is not null
	and p.PatientId not in (select PatientId from #allClients)
	and p.Regimen is null

--select distinct patientid from #allClients where PatientId not in (select PatientId )
--	select distinct PatientId from #PaedsCurrentOnArt where PatientId not in (select PatientId from #allClients)
declare 
	@efvpaeds5 int,
	@efvVL5 int,
	@efvsuppressed5 int,
	@nvppaeds5 int,
	@nvpVLs5 int,
	@nvpsuppressed5 int,
	@dtgpaeds5 int,
	@dtgVLs5 int,
	@dtgsuppressed5 int,
	@lpvpaeds5 int,
	@lpvVLs5 int,
	@lpvsuppressed5 int,
	@otherRegimen5 int,
	@otherRegimenVLs5 INT,
	@otherRegimensuppressed5 INT,
	@nonregimen5 INT,
	@nonregimenVLs5 INT,
	@nonregimensuppressed5 INT,
	@currentonart5 INT,
	@efvpaeds10 INT,
	@efvVL10 INT,
	@efvsuppressed10 INT,
	@nvppaeds10 INT,
	@nvpVLs10 INT,
	@nvpsuppressed10 INT,
	@dtgpaeds10 INT,
	@dtgVLs10 INT,
	@dtgsuppressed10 INT,
	@lpvpaeds10 INT,
	@lpvVLs10 INT,
	@lpvsuppressed10 INT,
	@otherRegimen10 INT,
	@otherRegimenVLs10 INT,
	@otherRegimensuppressed10 INT,
	@nonregimen10 INT,
	@nonregimenVLs10 INT,
	@nonregimensuppressed10 INT,
	@currentonart10 INT,
	@efvpaeds15 INT,
	@efvVL15 INT,
	@efvsuppressed15 INT,
	@nvppaeds15 INT,
	@nvpVLs15 INT,
	@nvpsuppressed15 INT,
	@dtgpaeds15 INT,
	@dtgVLs15 INT,
	@dtgsuppressed15 INT,
	@lpvpaeds15 INT,
	@lpvVLs15 INT,
	@lpvsuppressed15 INT,
	@otherRegimen15 INT,
	@otherRegimenVLs15 INT,
	@otherRegimensuppressed15 INT,
	@nonregimen15 INT,
	@nonregimenVLs15 INT,
	@nonregimensuppressed15 INT,
	@currentonart15 int 

set @efvpaeds5 = (select count(patientid) from #paedsEFV where AgeCat IN ('<01', '''01-04') AND patientid not in (select  PatientId from #paedsNVP union select  PatientId from  #paedsDTG union select  PatientId from #paedsLPV ))
set @efvVL5 = (select count(patientid) from #paedsEFV_VL WHERE AgeCat IN ('<01', '''01-04'))
set @efvsuppressed5 = (select count(patientid) from #paedsEFV_VlSuppressed WHERE AgeCat IN ('<01', '''01-04'))
set @nvppaeds5 = (select count(patientid) from #paedsNVP where AgeCat IN ('<01', '''01-04') AND  patientid not in (select  PatientId from #paedsEFV union select  PatientId from  #paedsDTG union select  PatientId from #paedsLPV ))
set @nvpVLs5 = (select count(patientid) from #paedsNVP_VL WHERE AgeCat IN ('<01', '''01-04'))
set @nvpsuppressed5 = (select count(patientid) from #paedsNVP_VlSuppressed WHERE AgeCat IN ('<01', '''01-04') )
set @dtgpaeds5 = (select count(patientid) from #paedsDTG where AgeCat IN ('<01', '''01-04') AND  patientid not in (select  PatientId from #paedsNVP union select  PatientId from #paedsLPV ))
set @dtgVLs5 = (select count(patientid) from #paedsDTG_VL WHERE  AgeCat IN ('<01', '''01-04'))
set @dtgsuppressed5 = (select count(patientid) from #paedsDTG_VlSuppressed WHERE  AgeCat IN ('<01', '''01-04'))
set @lpvpaeds5 = (select count(patientid) from #paedsLPV where  AgeCat IN ('<01', '''01-04') AND patientid not in (select  PatientId from #paedsEFV union select  PatientId from #paedsNVP union select  PatientId from  #paedsDTG ))
set @lpvVLs5 = (select count(patientid) from #paedsLPV_VL WHERE  AgeCat IN ('<01', '''01-04'))
set @lpvsuppressed5 = (select count(patientid) from #paedsLPV_VlSuppressed WHERE  AgeCat IN ('<01', '''01-04'))
set @otherRegimen5 =(select count(patientid) from #PaedsCurrentOnArt where  AgeCat IN ('<01', '''01-04') AND PatientId not in (select PatientId from #allClients ) and Regimen is not null)
set @otherRegimenVLs5 = (select count(patientid) from #paedsOtherRegimen_VL WHERE AgeCat IN ('<01', '''01-04') )
set @otherRegimensuppressed5 = (select count(p.patientid) from #PaedsCurrentOnArt p join #viralload2 v on p.PatientId = v.PatientId where AgeCat IN ('<01', '''01-04') AND VL_status = 'Suppressed' and  p.patientid  not in (select PatientId from #allClients ))
set @nonregimen5 =  (select count(patientid) from #PaedsCurrentOnArt where  AgeCat IN ('<01', '''01-04') AND Regimen is null )
set @nonregimenVLs5 = (select count(patientid) from #paedsNoRegimen_VL WHERE  AgeCat IN ('<01', '''01-04'))
set @nonregimensuppressed5 =( select count(p.patientid) from #PaedsCurrentOnArt p join #viralload2 v on p.PatientId = v.PatientId where  AgeCat IN ('<01', '''01-04') AND Regimen is null ) 
set @currentonart5 = (select distinct count(patientid) from #PaedsCurrentOnArt where AgeCat IN ('<01', '''01-04') )

set @efvpaeds10 = (select count(patientid) from #paedsEFV where AgeCat = '''05-09' AND patientid not in (select  PatientId from #paedsNVP union select  PatientId from  #paedsDTG union select  PatientId from #paedsLPV ))
set @efvVL10 = (select count(patientid) from #paedsEFV_VL WHERE AgeCat = '''05-09')
set @efvsuppressed10 = (select count(patientid) from #paedsEFV_VlSuppressed WHERE AgeCat = '''05-09')
set @nvppaeds10 = (select count(patientid) from #paedsNVP where AgeCat = '''05-09' AND  patientid not in (select  PatientId from #paedsEFV union select  PatientId from  #paedsDTG union select  PatientId from #paedsLPV ))
set @nvpVLs10 = (select count(patientid) from #paedsNVP_VL WHERE AgeCat = '''05-09')
set @nvpsuppressed10 = (select count(patientid) from #paedsNVP_VlSuppressed WHERE AgeCat = '''05-09' )
set @dtgpaeds10 = (select count(patientid) from #paedsDTG where AgeCat = '''05-09' AND  patientid not in (select  PatientId from #paedsNVP union select  PatientId from #paedsLPV ))
set @dtgVLs10 = (select count(patientid) from #paedsDTG_VL WHERE  AgeCat = '''05-09')
set @dtgsuppressed10 = (select count(patientid) from #paedsDTG_VlSuppressed WHERE  AgeCat = '''05-09')
set @lpvpaeds10 = (select count(patientid) from #paedsLPV where  AgeCat = '''05-09' AND patientid not in (select  PatientId from #paedsEFV union select  PatientId from #paedsNVP union select  PatientId from  #paedsDTG ))
set @lpvVLs10 = (select count(patientid) from #paedsLPV_VL WHERE  AgeCat = '''05-09')
set @lpvsuppressed10 = (select count(patientid) from #paedsLPV_VlSuppressed WHERE  AgeCat = '''05-09')
set @otherRegimen10 =(select count(patientid) from #PaedsCurrentOnArt where  AgeCat = '''05-09' AND PatientId not in (select PatientId from #allClients ) and Regimen is not null)
set @otherRegimenVLs10 = (select count(patientid) from #paedsOtherRegimen_VL WHERE AgeCat = '''05-09' )
set @otherRegimensuppressed10 = (select count(p.patientid) from #PaedsCurrentOnArt p join #viralload2 v on p.PatientId = v.PatientId where AgeCat = '''05-09' AND VL_status = 'Suppressed' and  p.patientid  not in (select PatientId from #allClients ))
set @nonregimen10 =  (select count(patientid) from #PaedsCurrentOnArt where  AgeCat = '''05-09' AND Regimen is null )
set @nonregimenVLs10 = (select count(patientid) from #paedsNoRegimen_VL WHERE  AgeCat = '''05-09')
set @nonregimensuppressed10 =( select count(p.patientid) from #PaedsCurrentOnArt p join #viralload2 v on p.PatientId = v.PatientId where  AgeCat = '''05-09' AND Regimen is null ) 
set @currentonart10 = (select distinct count(patientid) from #PaedsCurrentOnArt where AgeCat = '''05-09' )

set @efvpaeds15 = (select count(patientid) from #paedsEFV where AgeCat = '''10-14' AND patientid not in (select  PatientId from #paedsNVP union select  PatientId from  #paedsDTG union select  PatientId from #paedsLPV ))
set @efvVL15 = (select count(patientid) from #paedsEFV_VL WHERE AgeCat = '''10-14')
set @efvsuppressed15 = (select count(patientid) from #paedsEFV_VlSuppressed WHERE AgeCat = '''10-14')
set @nvppaeds15 = (select count(patientid) from #paedsNVP where AgeCat = '''10-14' AND  patientid not in (select  PatientId from #paedsEFV union select  PatientId from  #paedsDTG union select  PatientId from #paedsLPV ))
set @nvpVLs15 = (select count(patientid) from #paedsNVP_VL WHERE AgeCat = '''10-14')
set @nvpsuppressed15 = (select count(patientid) from #paedsNVP_VlSuppressed WHERE AgeCat = '''10-14' )
set @dtgpaeds15 = (select count(patientid) from #paedsDTG where AgeCat = '''10-14' AND  patientid not in (select  PatientId from #paedsNVP union select  PatientId from #paedsLPV ))
set @dtgVLs15 = (select count(patientid) from #paedsDTG_VL WHERE  AgeCat = '''10-14')
set @dtgsuppressed15 = (select count(patientid) from #paedsDTG_VlSuppressed WHERE  AgeCat = '''10-14')
set @lpvpaeds15 = (select count(patientid) from #paedsLPV where  AgeCat = '''10-14' AND patientid not in (select  PatientId from #paedsEFV union select  PatientId from #paedsNVP union select  PatientId from  #paedsDTG ))
set @lpvVLs15 = (select count(patientid) from #paedsLPV_VL WHERE  AgeCat = '''10-14')
set @lpvsuppressed15 = (select count(patientid) from #paedsLPV_VlSuppressed WHERE  AgeCat = '''10-14')
set @otherRegimen15 =(select count(patientid) from #PaedsCurrentOnArt where  AgeCat = '''10-14' AND PatientId not in (select PatientId from #allClients ) and Regimen is not null)
set @otherRegimenVLs15 = (select count(patientid) from #paedsOtherRegimen_VL WHERE AgeCat = '''10-14' )
set @otherRegimensuppressed15 = (select count(p.patientid) from #PaedsCurrentOnArt p join #viralload2 v on p.PatientId = v.PatientId where AgeCat = '''10-14' AND VL_status = 'Suppressed' and  p.patientid  not in (select PatientId from #allClients ))
set @nonregimen15 =  (select count(patientid) from #PaedsCurrentOnArt where  AgeCat = '''10-14' AND Regimen is null )
set @nonregimenVLs15 = (select count(patientid) from #paedsNoRegimen_VL WHERE  AgeCat = '''10-14')
set @nonregimensuppressed15 =( select count(p.patientid) from #PaedsCurrentOnArt p join #viralload2 v on p.PatientId = v.PatientId where  AgeCat = '''10-14' AND Regimen is null ) 
set @currentonart15 = (select distinct count(patientid) from #PaedsCurrentOnArt where AgeCat = '''10-14' )
					 
			 


select
	@QP	[Period],
	@provincename Province,
	@districtname District,
	case @hmiscode
		--Chilanga Aliases
		WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'George Community Post Unknown (1)'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		--when @hmiscode = '' then ''
		--when @hmiscode = '' then ''
		else @facilityname 
	end Facility,
	@hmiscode FacilityCode,
	a.*
FROM (
			SELECT 
				'<05' AgeCat,
				@currentonart5 CurrentOnArt,
				@nvppaeds5 PaedsOnNVP,
				@nvpVLs5  PaedsOnNVPwithVLdone,
				@nvpsuppressed5 NVPSupressed,
				'' blank1, 
				@efvpaeds5 PaedsOnEFV,
				@efvVL5 PaedsOnEFVwithVLdone,
				@efvsuppressed5 EFVSupressed, 
				'' blank2,
				@lpvpaeds5 PaedsOnLPV,
				@lpvVLs5  PaedsOnLPVwithVLdone, 
				@lpvsuppressed5 LPVSupressed,
				'' blank3,
				@dtgpaeds5 PaedsOnDTG,
				@dtgVLs5 PaedsOnDTGwithVLdone,
				@dtgsuppressed5 DTGSupressed,
				'' blank4,
				@otherRegimen5 Other_Regimens,
				@otherRegimenVLs5 PaedsOnOtherRegimenWithVLdone,
				@otherRegimensuppressed5 Other_Regimens_Suppressed,
				'' blank5,
				@nonregimen5 Nonregimen,
				@nonregimenVLs5 PaedsOnNonRegimenWithVLdone, 
				@nonregimensuppressed5 NonRegimenSuppressed
					  
			UNION
			select
				'''05-09' AgeCat,
				@currentonart10 CurrentOnArt,
				@nvppaeds10 PaedsOnNVP,
				@nvpVLs10  PaedsOnNVPwithVLdone,
				@nvpsuppressed10 NVPSupressed,
				'' blank1, 
				@efvpaeds10 PaedsOnEFV,
				@efvVL10 PaedsOnEFVwithVLdone,
				@efvsuppressed10 EFVSupressed, 
				'' blank2,
				@lpvpaeds10 PaedsOnLPV,
				@lpvVLs10  PaedsOnLPVwithVLdone, 
				@lpvsuppressed10 LPVSupressed,
				'' blank3,
				@dtgpaeds10 PaedsOnDTG,
				@dtgVLs10 PaedsOnDTGwithVLdone,
				@dtgsuppressed10 DTGSupressed,
				'' blank4,
				@otherRegimen10 Other_Regimens,
				@otherRegimenVLs10 PaedsOnOtherRegimenWithVLdone,
				@otherRegimensuppressed10 Other_Regimens_Suppressed,
				'' blank5,
				@nonregimen10 Nonregimen,
				@nonregimenVLs10 PaedsOnNonRegimenWithVLdone, 
				@nonregimensuppressed10 NonRegimenSuppressed

			UNION
			select
				'''10-14' AgeCat,
				@currentonart15 CurrentOnArt,
				@nvppaeds15 PaedsOnNVP,
				@nvpVLs15  PaedsOnNVPwithVLdone,
				@nvpsuppressed15 NVPSupressed,
				'' blank1, 
				@efvpaeds15 PaedsOnEFV,
				@efvVL15 PaedsOnEFVwithVLdone,
				@efvsuppressed15 EFVSupressed, 
				'' blank2,
				@lpvpaeds15 PaedsOnLPV,
				@lpvVLs15  PaedsOnLPVwithVLdone, 
				@lpvsuppressed15 LPVSupressed,
				'' blank3,
				@dtgpaeds15 PaedsOnDTG,
				@dtgVLs15 PaedsOnDTGwithVLdone,
				@dtgsuppressed15 DTGSupressed,
				'' blank4,
				@otherRegimen15 Other_Regimens,
				@otherRegimenVLs15 PaedsOnOtherRegimenWithVLdone,
				@otherRegimensuppressed15 Other_Regimens_Suppressed,
				'' blank5,
				@nonregimen15 Nonregimen,
				@nonregimenVLs15 PaedsOnNonRegimenWithVLdone, 
				@nonregimensuppressed15 NonRegimenSuppressed
	) a					
					  

					