USE cdc_fdb_db;

DECLARE   
	@StartDate DATETIME,
    @EndDate DATETIME,
	@PId INT, 
	@DId INT,
	@FId VARCHAR(10),
    @QP VARCHAR(10),
	@LTFUThreshold INT,
	@PN VARCHAR(100),
	@DN VARCHAR(100),
	@FN VARCHAR(100),
	@FG VARCHAR(100),
	@NHC VARCHAR(10),
	@OHC VARCHAR(10),
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME,
	@FYStart DATETIME


--set start and end dates
SET @Now = GETDATE();
SET @FYStart = '20201001';
SET @StartDate = @FYStart;
SET @EndDate = EOMONTH(@StartDate);
SET @EndDate = dateadd(second, 86399, @EndDate); --adding 23:59:59
SET @QP = convert(VARCHAR(6), @StartDate,112);
SET @LTFUThreshold = 30;
SET @FG = (SELECT [Value] FROM cdc_fdb_db.dbo.Setting WHERE [Name] = 'FacilityGuid');
SET @PId = (SELECT [Value] FROM cdc_fdb_db.dbo.Setting WHERE [Name] = 'ProvinceId');
SET @DId = (SELECT [Value] FROM cdc_fdb_db.dbo.Setting WHERE [Name] = 'DistrictId');
SET @FId = (SELECT [Value] FROM cdc_fdb_db.dbo.Setting WHERE [Name] = 'HmisCode');
SET @OnArtMode = 1; -- 1 = CURRENT, 0 = EVER 

SELECT 
	@PN = p.Name, 
	@DN=d.Name, 
	@FN=f.FacilityName, 
	@NHC=M.NewHMISCode,		
	@OHC=M.OldHMISCode 
FROM Facility f 
LEFT JOIN cdc_fdb_db.dbo.District d ON f.DistrictId = d.Code 
LEFT JOIN cdc_fdb_db.dbo.Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN cdc_fdb_db.dbo.FacilityOldHMISMap M ON M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG;

IF(EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tempdb].[dbo].[CummTxCurr]') AND type in (N'U')) ) 
	BEGIN
		/*
		TRUNCATE TABLE [tempdb].[dbo].[CummTxCurr]
		*/
		--/*
		DROP TABLE [tempdb].[dbo].[CummTxCurr]
		CREATE TABLE [tempdb].[dbo].[CummTxCurr](
			PatientId    INT,
			EditLocation INT,
			Age          INT,
			AgeLastVisit INT,
			Sex          VARCHAR(1),
			ArtStartDate DATE
		)
		--*/
	END
ELSE CREATE TABLE [tempdb].[dbo].[CummTxCurr](
    PatientId    INT,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
)

DECLARE
		@PID2 VARCHAR(2) = '50',
		@DID2 VARCHAR(2) = '33',
		--@CFID VARCHAR(50),
		@FID2 VARCHAR(9);

	--SET @CFID = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGUID')
	SET @FID2 = (
				CASE @FG
					WHEN 'tvhd4akqn785db9xjaxk8oqrlb1lka08' THEN '500400251' --Chikoka
					WHEN 'g0gdw9d53dm32raz0h46vrsqgbr9417l' THEN '500400253' --Kafue East
					WHEN 'tuzn5ssrponmtkuw06wsr1plserg46h3' THEN '500400251' --Mtendere
					WHEN '7f2pvfn4p4t60gs4uez20t6hxvjm2ftv' THEN '500400021' --Mungu
					WHEN 'uk1oj1tva6bjmiknoj664bin2bkkdjn8' THEN '500400260' --Kabweza
					WHEN 'n48r3u184v9ty34953tr4fduagd12wfv' THEN '500400080' --Old Kabweza
				END
	)

IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@PId, @DId, @NHC)
UNION
SELECT SubSiteId FROM dbo.fn_GetFacilitySubSiteIds(@PID2, @DID2, @FID2);


IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL) DROP TABLE #AllCurrPrEPClients;

CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);
CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);
CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId       INT PRIMARY KEY,
    InteractionDate DATE
);
CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    	EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);

WHILE @StartDate < @Now
BEGIN 

EXEC Analysis.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@LTFUThreshold,
	@OnArtMode;

INSERT INTO [tempdb].[dbo].CummTxCurr
SELECT * FROM #CurrentOnArt 

SET @StartDate = DATEADD(MONTH, 1, @StartDate);
SET @EndDate = EOMONTH(@StartDate);
SET @EndDate = dateadd(second, 86399, @EndDate); --adding 23:59:59

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) TRUNCATE TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) TRUNCATE TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) TRUNCATE TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) TRUNCATE TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL) TRUNCATE TABLE #AllCurrPrEPClients;


END 

IF (OBJECT_ID('tempdb..#TxCurrWomenOfAge15To49') IS NOT NULL) DROP TABLE #TxCurrWomenOfAge15To49
SELECT DISTINCT a.*, g.MappedGuid PatientGuid
INTO #TxCurrWomenOfAge15To49
FROM [tempdb].[dbo].CummTxCurr a
LEFT JOIN GuidMap g on a.PatientId = g.NaturalNumber and g.MappedGuid = g.OwningGuid
	WHERE a.Sex = 'F' AND a.AgeLastVisit BETWEEN 15 AND 49


IF (OBJECT_ID('tempdb..#HivResults') IS NOT NULL) DROP TABLE #HivResults
SELECT * 
INTO #HivResults 
FROM Analysis.dbo.fn_GetAllHivTestResults_RevisedTxCurr(DEFAULT)

IF (OBJECT_ID('tempdb..#HivPosResults') IS NOT NULL) DROP TABLE #HivPosResults
SELECT * 
INTO #HivPosResults 
FROM #HivResults 
	WHERE TestResult = 1

IF (OBJECT_ID('tempdb..#HivNegResults') IS NOT NULL) DROP TABLE #HivNegResults
SELECT * 
INTO #HivNegResults 
FROM #HivResults
	WHERE TestResult = 2 
	AND PatientId NOT IN (SELECT PatientId FROM #HivPosResults)


IF (OBJECT_ID('tempdb..#TxCurrMothers') IS NOT NULL) DROP TABLE #TxCurrMothers
SELECT 
	b.PatientId,
	b.PrimaryGuid PatientGuid,
	a.AgeLastVisit Age,
	g.NaturalNumber ChildIdInt,
	b.SecondaryGuid ChildGuid,
	r.Sex ChildSex,
	r.DateOfBirth ChildDateOfBirth,
	DATEDIFF(DAY, r.DateOfBirth, @EndDate)/365.25 ChildAge,
	b.Notes
INTO #TxCurrMothers
FROM #TxCurrWomenOfAge15To49 a
JOIN cdc_fdb_db.dbo.crtRelation b ON a.PatientId = b.PatientId
LEFT JOIN cdc_fdb_db.dbo.Registration r ON b.SecondaryGuid = r.PatientGuid
LEFT JOIN cdc_fdb_db.dbo.GuidMap g on b.SecondaryGuid = g.MappedGuid and g.MappedGuid = g.OwningGuid
	WHERE b.RelationType IN ('Child', 'Parent')
	AND DATEDIFF(DAY, r.DateOfBirth, @EndDate)/365.25 <20.0
	AND (a.AgeLastVisit - (DATEDIFF(DAY, r.DateOfBirth, @EndDate)/365.25)) >= 12.0;

INSERT INTO #TxCurrMothers
SELECT 
	a.PatientId PatientId,
	a.PatientGuid,
	a.AgeLastVisit Age,
	b.PatientId ChildIdInt,
	b.PrimaryGuid ChildGuid,
	r.Sex ChildSex,
	r.DateOfBirth ChildDateOfBirth,
	DATEDIFF(DAY, r.DateOfBirth, @EndDate)/365.25 ChildAge,
	b.Notes
FROM #TxCurrWomenOfAge15To49 a
JOIN cdc_fdb_db.dbo.crtRelation b ON a.PatientGuid = b.SecondaryGuid
LEFT JOIN cdc_fdb_db.dbo.crtRegistrationInteraction r ON b.PatientId = r.patientId_int
	WHERE b.RelationType IN ('Child', 'Parent')
	AND DATEDIFF(DAY, r.DateOfBirth, @EndDate)/365.25 <20.0
	AND (a.AgeLastVisit - (DATEDIFF(DAY, r.DateOfBirth, @EndDate)/365.25)) >= 12.0;


IF (OBJECT_ID('tempdb..#LinkedChildren') IS NOT NULL) DROP TABLE #LinkedChildren
SELECT
	a.ChildIdInt, a.ChildGuid, MAX(a.PatientId) MotherId, MIN(b.TestResult) HivStatus
INTO #LinkedChildren
FROM #TxCurrMothers a
JOIN #HivResults b ON a.ChildIdInt = b.PatientId
	GROUP BY a.ChildIdInt, a.ChildGuid


DECLARE
	@TxCurrWomen INT = 0,
	@TxMothers INT = 0,
	@LinkedKids INT = 0,
	@TestedLinkedKids INT = 0,
	@WomenWithTestedLinkedKids INT = 0;

SET @TxCurrWomen = (SELECT COUNT(DISTINCT PatientId) FROM #TxCurrWomenOfAge15To49);
SET @TxMothers = (SELECT COUNT(DISTINCT PatientId) FROM #TxCurrMothers);
SET @LinkedKids = (SELECT COUNT(DISTINCT ChildIdInt) FROM #TxCurrMothers);
SET @TestedLinkedKids = (SELECT COUNT(DISTINCT ChildIdInt) FROM #LinkedChildren);
SET @WomenWithTestedLinkedKids = (SELECT COUNT(DISTINCT MotherId) FROM #LinkedChildren);

SELECT
	@PN Province,
	@DN District,
	@FId FacilityCode,
	CASE @FId
		--Chilanga Aliases
		WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'George Community Post Unknown (1)'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		ELSE @FN
	END Facility,
	@TxCurrWomen TxCurrWomen,
	@TxMothers TxCurrMothers,
	@LinkedKids LinkedChildren,
	@WomenWithTestedLinkedKids TxCurrMothersWithChildrenTested,
	@TestedLinkedKids LinkedChildrenTested

