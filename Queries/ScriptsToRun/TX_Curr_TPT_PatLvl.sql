USE cdc_fdb_db;

DECLARE   
	@RC INT,
	@StartDate  DATETIME,
    @EndDate    DATETIME, 
	@StopDate datetime,
	@FId VARCHAR(10),
    @QP VARCHAR(10),
	@LTFUThreshold INT,
	@PN VARCHAR(100),
	@PId INT,
	@DN VARCHAR(100),
	@DId INT,
	@FN VARCHAR(100),
	@FG VARCHAR(100),
	@NHC VARCHAR(10),
	@OHC VARCHAR(10)
--SET start AND END dates
SET @StartDate = '2021-Nov-01';
SET @EndDate = '2021-Nov-30 23:59:59';
--SET @StopDate = '20200630'
SET @QP = convert(VARCHAR(6), @StartDate,112);
SET @LTFUThreshold = 30;
SET @FG = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
SET @FId = (SELECT value FROM setting WHERE name = 'HmisCode');

SELECT 
	@PN = p.Name, 
	@PId = p.ProvinceSeq, 
	@DN=d.Name, 
	@DId = d.DistrictSeq, 
	@FN=f.FacilityName, 
	@NHC=M.NewHMISCode, 
	@OHC=M.OldHMISCode 
FROM Facility f 
JOIN district d ON f.DistrictId = d.Code 
JOIN province p ON d.ProvinceSeq = p.ProvinceSeq
JOIN FacilityOldHMISMap M ON M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG

--*************************** TX_CURR *******************************************************
IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@PId, @DId, @FId);


IF OBJECT_ID('tempdb..#Discontinued') IS NOT NULL
    DROP TABLE #Discontinued;
IF OBJECT_ID('tempdb..#DeadClients') IS NOT NULL
    DROP TABLE #DeadClients;
IF OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL
    DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL
    DROP TABLE #CurrentOnARTByClinicalVisit;
IF OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL
    DROP TABLE #CurrentOnARTByDrugs;
IF OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL
    DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL
    DROP TABLE #CurrentOnArt;
IF (OBJECT_ID('tempdb..#AllCurrPrEPClients')) IS NOT NULL
    DROP TABLE #AllCurrPrEPClients;
CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);
CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    INTeractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEND            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);
CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId       INT PRIMARY KEY,
    INTeractionDate DATE
);
CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    INTeractionDate              DATE,
    DeadBeforeEND                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEND DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEND                   INT,
    AgeArtStart                  INT
);
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    	EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);
 
exec [dbo].[proc_GetTxCurr_Base] 
	@StartDate,
    @EndDate,
    @LTFUThreshold

--********************* END TX_Curr **************************************************
--*********************************************** TB Screening ******************************************************************

IF (OBJECT_ID('tempdb..#Allinteractions') IS NOT NULL) DROP TABLE #Allinteractions;	
SELECT * INTO #Allinteractions FROM
	(
	SELECT 
	visitdat visitdate,
	ROW_NUMBER() OVER(PARTITION BY patient_guid ORDER BY overview_interaction_time DESC) latest,
	patient_guid,
	source.interaction_id,
	overview_interaction_location location,
	overview_interaction_time vtime,
	overview_deprecated deprecated,
	[review_of_systems_fever] fever,
	[review_of_systems_weight_loss] weightloss,
	[review_of_systems_productive_cough] prodcough,
	[review_of_systems_non_productive_cough] nonprodcough,
	[review_of_systems_hemoptysis] hemoptysis,
	[review_of_systems_difficulty_breathing] breathing,
	[review_of_systems_night_sweats] nightsweats,
	nocomplaint nocomplaint, 
	Sputum sputum,
	chest_x_ray chest_x_ray
	
	FROM
	(

		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		[review_of_systems_fever],
		[review_of_systems_weight_loss],
		[review_of_systems_productive_cough],
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		[review_of_systems_night_sweats],
		presenting_complaint_complaints_no_complaint nocomplaint,
		[investigation_and_referrals_paed_investigations_sputum_for_afb] Sputum,
		[investigation_and_referrals_paed_investigations_chest_x_ray] chest_x_ray
		FROM

		[dbo].[PaedsInitialHistoryAndPhysicalInteraction2]
		UNION 

		--SELECT * FROM [PaedsInitialHistoryAndPhysicalInteraction2]
	
		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		COALESCE(presenting_complaint_complaints_fever,[review_of_systems_fever]) fever,
		COALESCE(presenting_complaint_complaints_weight_loss,[review_of_systems_weight_loss]) weightloss,
		COALESCE(presenting_complaint_complaints_cough,[review_of_systems_productive_cough]) cough,
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		COALESCE(presenting_complaint_complaints_night_sweats,[review_of_systems_night_sweats])nightsweats,
		presenting_complaint_complaints_no_complaint,
		[investigation_and_referrals_paed_investigations_sputum_for_afb],
		[investigation_and_referrals_paed_investigations_chest_x_ray]
		FROM

		[dbo].[Art45_PaedsInitialHistoryAndPhysicalInteraction1]
		UNION

		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		[review_of_systems_fever],
		[review_of_systems_weight_loss],
		[review_of_systems_productive_cough],
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		[review_of_systems_night_sweats],
		presenting_complaint_and_medications_complaints_no_complaint,
		[investigation_and_referrals_paed_investigations_sputum_for_afb],
		[investigation_and_referrals_paed_investigations_chest_x_ray]
		FROM
		[dbo].[PaedsClinicalFollowUpInteraction2]

		UNION

		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		[review_of_systems_fever],
		[review_of_systems_weight_loss],
		[review_of_systems_productive_cough],
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		[review_of_systems_night_sweats],
		presenting_complaint_complaints_no_complaint,
		[plan_prescriptions_investigations_referrals_investigations_sputum_for_afb],
		[plan_prescriptions_investigations_referrals_investigations_chest_x_ray]

		FROM

		[dbo].[Art3_InitialHistoryAndPhysicalInteraction3]
		UNION
	
	
		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		[presenting_complaint_fever],
		[presenting_complaint_weight_loss],
		presenting_complaint_cough,
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		[presenting_complaint_night_sweats],
		presenting_complaint_complaints_no_complaint,
		[investigation_and_referrals_art_investigations_sputum_for_afb],
		[investigation_and_referrals_art_investigations_chest_x_ray]
		FROM


		[dbo].[Art45_InitialHistoryAndPhysicalInteraction1]
		UNION

		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		[review_of_systems_fever],
		[review_of_systems_weight_loss],
		[review_of_systems_productive_cough],
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		[review_of_systems_night_sweats],
		presenting_complaint_and_medications_complaints_no_complaint,
		[investigation_and_referrals_investigations_sputum_for_afb],
		[investigation_and_referrals_investigations_chest_x_ray]

		FROM

		[dbo].[Art3_ClinicalFollowUpInteraction3]
		UNION

		SELECT
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		[presenting_complaint_fever],
		[presenting_complaint_weight_loss],
		[presenting_complaint_cough],
		[review_of_systems_non_productive_cough],
		[review_of_systems_hemoptysis],
		[review_of_systems_difficulty_breathing],
		[presenting_complaint_night_sweats],
		presenting_complaint_no_complaint,
		[investigation_and_referrals_art_investigations_sputum_for_afb],
		[investigation_and_referrals_art_investigations_chest_x_ray]
		FROM

		[dbo].Art45_ClinicalFollowUpInteraction1
	
		UNION 
	
		SELECT 
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		presenting_complaint_and_medications_fever  presenting_complaint_and_medications_fever,
		[presenting_complaint_and_medications_weight_loss] presenting_complaint_and_medications_weight_loss,
		presenting_complaint_and_medications_cough presenting_complaint_and_medications_cough,
		presenting_complaint_and_medications_cough presenting_complaint_and_medications_cough2,
		NULL,
		NULL,
		presenting_complaint_and_medications_night_sweats presenting_complaint_and_medications_night_sweats,
		presenting_complaint_and_medications_no_complaint nocomplaint,
		[investigation_and_referrals_investigations_sputum_for_afb],
		[investigation_and_referrals_investigations_chest_x_ray]

		FROM Art45_shortvisitInteraction1 s 
		UNION
	
		SELECT 
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		presenting_complaint_and_medications_fever ,
		presenting_complaint_and_medications_weight_loss ,
		presenting_complaint_and_medications_cough ,
		presenting_complaint_and_medications_cough ,
		NULL,
		NULL,
		presenting_complaint_and_medications_night_sweats,
		presenting_complaint_and_medications_complaints_no_complaint,
		[investigation_and_referrals_paed_investigations_sputum_for_afb],
		[investigation_and_referrals_paed_investigations_chest_x_ray]

		FROM Art45_PaedsshortvisitInteraction1 s  WHERE overview_deprecated = 0
		UNION
		SELECT 
		overview_interaction_time visitdat,
		patient_guid,
		interaction_id,
		overview_interaction_location,
		overview_interaction_time,
		overview_deprecated,
		history_and_examination_fever ,
		history_and_examination_weight_loss ,
		history_and_examination_cough,
		history_and_examination_cough,
		NULL,
		NULL,
		history_and_examination_night_sweats,
		history_and_examination_no_complaint_no_complaint, 
		[investigations_and_referrals_art_investigations_sputum_for_afb],
		[investigations_and_referrals_art_investigations_chest_x_ray]
		FROM Art45_ArvEligibilityInteraction1 
	

	)source --JOIN crtRegistrationInteraction r ON source.patient_guid = r.patientId_int 

) x 
	WHERE visitdate BETWEEN DATEADD(year, -3, @StartDate) AND @EndDate

IF(OBJECT_ID('tempdb..#DS2') IS NOT NULL) DROP TABLE #DS2
SELECT patient_guid PatID, INTeraction_id, latest,
CASE
	WHEN fever IS NULL AND weightloss IS NULL AND prodcough IS NULL AND nonprodcough IS NULL AND hemoptysis IS NULL
		AND breathing IS NULL AND nightsweats IS NULL AND (nocomplaint IS NULL OR nocomplaint = 0)
		THEN 'Not Screened'
	WHEN fever IS NULL AND weightloss IS NULL AND prodcough IS NULL AND nonprodcough IS NULL AND hemoptysis IS NULL
		AND breathing IS NULL AND nightsweats IS NULL AND nocomplaint = 1
		THEN 'Asymptomatic'
	WHEN fever = 0 AND weightloss = 0 AND prodcough = 0 AND nonprodcough = 0 AND hemoptysis = 0
		AND breathing = 0 AND nightsweats = 0 AND (nocomplaint = 1 OR nocomplaint IS NULL)
		THEN 'Asymptomatic'
	WHEN fever = 1 OR weightloss = 1 OR prodcough = 1 OR nonprodcough = 1 OR hemoptysis = 1
		or breathing = 1 OR nightsweats = 1 
		THEN 'Symptomatic'
	ELSE 'Not Screened'
END TB_Screened
INTO #DS2 FROM #Allinteractions
	WHERE patient_guid IN (SELECT PatientId FROM #CurrentOnArt)
	AND latest = 1
--********************************************* END TB Screening ***************************************************************

--************************************************ TPT and TB history *****************************************************************
IF(OBJECT_ID('tempdb..#TB_FDCs') IS NOT NULL) DROP TABLE #TB_FDCs;
IF(OBJECT_ID('tempdb..#Drug_INH') IS NOT NULL) DROP TABLE #Drug_INH;
IF(OBJECT_ID('tempdb..#TBHistory') IS NOT NULL) DROP TABLE #TBHistory;
IF(OBJECT_ID('tempdb..#TPTHistory') IS NOT NULL) DROP TABLE #TPTHistory;
IF(OBJECT_ID('tempdb..#TBSummary') IS NOT NULL) DROP TABLE #TBSummary;
IF(OBJECT_ID('tempdb..#TPTSummary') IS NOT NULL) DROP TABLE #TPTSummary;
IF(OBJECT_ID('tempdb..#TB_Dx') IS NOT NULL) DROP TABLE #TB_Dx;

--Anti-TB
SELECT DISTINCT
	ep.PatientId,
	ep.InteractionID,
	sc.ServiceName,
	ep.EditLocation,
	ep.InteractionTime,
	pe.OnsetDate,
	pe.ResolvedDate,
	pe.Certainty,
	icpc2.Title [Full Diagnosis],
	icpc2.ShortTitle [Short Diagnosis],
	icd10.Title [Comments]
INTO #TB_Dx
FROM crctProblemEpisode pe
JOIN crtProblemEpisode ep ON pe.InteractionID = ep.InteractionID
JOIN #CurrentOnArt c ON ep.PatientId = c.PatientId
JOIN ServiceCodes sc ON ep.ServiceCode = sc.ServiceCode
LEFT JOIN ICPC2eCodes icpc2 ON pe.ICPCProblem = icpc2.ICPCConceptID
LEFT JOIN ICD10Lookup icd10 ON pe.ICDProblem = icd10.ICD10ConceptID
LEFT JOIN Hia1MappingData hia on icpc2.ICPC2eCode = hia.Icpc2Code OR icd10.ICD10ID = hia.Icd10Code
	WHERE COALESCE(pe.OnsetDate, ep.InteractionTime) IS NOT NULL
	AND (icpc2.Title LIKE '%Tuberculosis%' OR  icpc2.Title LIKE '%TB%' OR icd10.Title LIKE '%Tuberculosis%' OR  icd10.Title LIKE '%TB%')
	AND pe.OnsetDate BETWEEN DATEADD(YEAR, -4, @EndDate) AND @EndDate
	ORDER BY ep.PatientId;

SELECT MedDrugId
INTO #TB_FDCs
FROM DrugProductsCachedView
	WHERE (Synonym LIKE '%INH%' OR DrugClass = 'Anti-tuberculosis')
	AND Synonym != 'INH';

SELECT b.PatientId, b.PatientArtNumber, a.InteractionID, b.VisitDate, CAST(a.UnitsDispensed as INT) UnitsDispensed, 
	DATEADD(DAY, CAST(a.UnitsDispensed as INT), b.VisitDate) NextRefillDate 
INTO #TBHistory 
FROM crctMedicationDispensingDetails a
LEFT JOIN crtPharmacyVisit b ON a.InteractionID = b.InteractionID
	WHERE a.MedDrugId IN (SELECT DISTINCT MedDrugId FROM #TB_FDCs)
	AND b.PatientId IN (SELECT DISTINCT PatientId FROM #TB_Dx)
	ORDER BY b.PatientId, b.VisitDate;

SELECT a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate, 
	MIN(b.VisitDate) Earliest_TBDx_Date, 
	MAX(b.NextRefillDate) NextRefillDate,
	SUM(b.UnitsDispensed) ATT_Tabs_Collected
INTO #TBSummary
FROM ClientHivSummary a
INNER JOIN #TBHistory b ON a.PatientId = b.PatientId
	WHERE b.VisitDate <= @EndDate
	AND b.NextRefillDate >= DATEADD(YEAR, -3, @StartDate)
	GROUP BY a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate;
	
-- TPT
SELECT MedDrugId 
INTO #Drug_INH 
FROM DrugProductsCachedView
	WHERE GenericName = 'Isoniazid' OR Synonym = 'INH';

SELECT b.PatientId, b.PatientArtNumber, a.InteractionID, b.VisitDate, CAST(a.UnitsDispensed as INT) UnitsDispensed, 
	DATEADD(DAY, CAST(a.UnitsDispensed as INT), b.VisitDate) NextRefillDate 
INTO #TPTHistory 
FROM crctMedicationDispensingDetails a
LEFT JOIN crtPharmacyVisit b ON a.InteractionID = b.InteractionID
	WHERE a.MedDrugId IN (SELECT DISTINCT MedDrugId FROM #Drug_INH)
	AND b.PatientId IN (SELECT DISTINCT PatientId FROM #CurrentOnArt)
	ORDER BY b.PatientId, b.VisitDate;



SELECT a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate, 
	MIN(b.VisitDate) Earliest_TPT_Date, 
	MAX(b.NextRefillDate) NextRefillDate,
	SUM(b.UnitsDispensed) INH_Tabs_Collected
INTO #TPTSummary 
FROM ClientHivSummary a
INNER JOIN #TPTHistory b ON a.PatientId = b.PatientId
	WHERE b.VisitDate <= @EndDate
	AND b.NextRefillDate >= DATEADD(YEAR, -3, @StartDate)
	GROUP BY a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate;


--********************************************** END TPT and TB History ****************************************************************

--**************************************** Current Address ********************************************
IF(OBJECT_ID('tempdb..#Address') IS NOT NULL) DROP TABLE #Address;
SELECT DISTINCT * INTO #Address FROM (
	SELECT ROW_NUMBER() OVER(PARTITION BY PatientGUID ORDER BY CreatedTime DESC) N, NaturalNumber PatientID, A.PatientGUID,
		CASE
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN NULL
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN REPLACE(PhoneContact, ',', '')
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN REPLACE(PhoneNumber, ',', '')
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN REPLACE(MobilePhoneNumber, ',', '')
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN CONCAT(REPLACE(PhoneContact, ',', ''), ' | ', REPLACE(PhoneNumber, ',', ''), ' | ', REPLACE(MobilePhoneNumber, ',', ''))
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN CONCAT(REPLACE(PhoneNumber, ',', ''), ' | ', REPLACE(MobilePhoneNumber, ',', ''))
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN CONCAT(REPLACE(PhoneContact, ',', ''), ' | ', REPLACE(MobilePhoneNumber, ',', ''))
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN CONCAT(REPLACE(PhoneContact, ',', ''), ' | ', REPLACE(PhoneNumber, ',', ''))
		END PNumber 
	FROM [Address] A JOIN guidmap g ON a.PatientGUID = g.MappedGuid
		WHERE AddressType = 'Current'
) Ads
	WHERE N = 1;
--use both PatientGUID AND PatientId to link ON JOINs


--************************************** END Current Address *******************************************

SELECT DISTINCT 
	@QP [Period],
	@PN Province,
	@DN District,
	case @FId
		--Chilanga Aliases
		--WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Market Community Post'
		WHEN '50060106A' THEN 'George Community Post'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		else @FN 
	end Facility, 
	@FId HMISCode,
	d.PatientId NUPN, d.SurName, d.FirstName, d.Sex, CAST(d.DateOfBirth AS DATE) DateOfBirth,
	a.AgeLastVisit Age, d.ArtNumber, a.ArtStartDate,
	CASE
		WHEN c.TB_Screened IS NOT NULL THEN c.TB_Screened
		ELSE 'Not Screened'
	END TB_Screened, 
	CASE
		WHEN b.Earliest_TPT_Date BETWEEN @StartDate AND @EndDate THEN 'New on TPT'
		WHEN tb.Earliest_TBDx_Date BETWEEN @StartDate AND @EndDate THEN 'New on ATT'
		WHEN b.Earliest_TPT_Date < @StartDate AND b.NextRefillDate >= @StartDate THEN 'Current on TPT'
		WHEN tb.Earliest_TBDx_Date < @StartDate AND tb.NextRefillDate >= @StartDate THEN 'Current on ATT'
		WHEN (b.Earliest_TPT_Date < @StartDate AND b.NextRefillDate < @StartDate AND DATEDIFF(MONTH, b.NextRefillDate, @StartDate) >= 36) 
			OR (tb.Earliest_TBDx_Date < @StartDate AND tb.NextRefillDate < @StartDate AND DATEDIFF(MONTH, tb.NextRefillDate, @StartDate) >= 36)
			THEN 'Due for TPT'
		WHEN (b.Earliest_TPT_Date < @StartDate AND b.NextRefillDate < @StartDate AND DATEDIFF(MONTH, b.NextRefillDate, @StartDate) < 36)  
			THEN 'Previously on TPT'
		WHEN (tb.Earliest_TBDx_Date < @StartDate AND tb.NextRefillDate < @StartDate AND DATEDIFF(MONTH, tb.NextRefillDate, @StartDate) < 36)
			THEN 'Previously on ATT'
		WHEN (b.Earliest_TPT_Date IS NULL AND tb.Earliest_TBDx_Date IS NULL) THEN 'Never on TPT or ATT'
	END TPT_Status, 
	COALESCE(b.NextRefillDate, tb.NextRefillDate) NextTPTRefillDate,	
	e.PNumber PhoneContacts 
FROM #CurrentOnArt a
LEFT JOIN #TPTSummary b ON a.PatientId = b.PatientId
LEFT JOIN #TBSummary tb ON a.PatientId = tb.PatientId
LEFT JOIN #DS2 c ON a.PatientId	 = c.PatID
LEFT JOIN crtRegistrationInteraction d ON a.PatientId = d.patientId_int
LEFT JOIN #Address e ON a.PatientId = e.PatientID
	ORDER BY ArtStartDate DESC;