use [cdc_fdb_db];

declare   
	@StartDate  DATETIME,
    @EndDate    DATETIME,
	@FId varchar(10),
    @QP nvarchar(10),
	@TH nvarchar(10),
	@Threshold int,
	@PN varchar(100),
	@DN varchar(100),
	@FN varchar(100),
	@FG nvarchar(100),
	@NHC varchar(10),
	@OHC nvarchar(10);
--set start and end dates
set @StartDate = '20211001';
set @EndDate = eomonth(@StartDate, 2);
set @EndDate = dateadd(second, 86399, @EndDate); --adding 23:59:59
--set @QP = convert(nvarchar(6), @StartDate, 112);
set @Threshold = 30;
--set @TH = convert(varchar, @Threshold)+' Days'
set @FG = (select [Value] from Setting where [Name] = 'FacilityGuid');
set @FId = (select [Value] from Setting where [Name] = 'HmisCode');
set @QP = (
		CASE DATEDIFF(MONTH, @StartDate, @EndDate) 
			WHEN 0 THEN convert(nvarchar(6), @StartDate,112) 
			WHEN 2 THEN 
				CASE
					WHEN  DATEPART(MONTH, @StartDate) IN (10, 11, 12) THEN CONCAT('FY', RIGHT((DATENAME(YEAR, @StartDate) +1), 2), 'Q1')
					WHEN  DATEPART(MONTH, @StartDate) NOT IN (10, 11, 12) THEN CONCAT('FY', RIGHT(DATENAME(YEAR, @StartDate), 2), 'Q', (DATEPART(QUARTER, @StartDate)+1))
				END
			ELSE 'UNDEFINED'
		END
);

SELECT TOP 1 @PN = p.Name, @DN=d.Name, @FN=f.FacilityName, @NHC=M.NewHMISCode, @OHC=M.OldHMISCode FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG;

--SmartCare TXCurr and TXNew
  DECLARE @PROVINCEID VARCHAR(2)
  DECLARE @DISTRICTID VARCHAR(2)
set @ProvinceId = (select max([Value]) v from setting where [Name] = 'ProvinceId')
set @DistrictId = (select max([Value]) v from setting where [Name] = 'DistrictId')



  --From facilities, get and filter only the current facility in the current province and district
  DECLARE
		@PID2 VARCHAR(2) = '50',
		@DID2 VARCHAR(2) = '33',
		--@CFID VARCHAR(50),
		@FID2 VARCHAR(9);

	--SET @CFID = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGUID')
	SET @FID2 = (
				CASE @FG
					WHEN 'tvhd4akqn785db9xjaxk8oqrlb1lka08' THEN '500400251' --Chikoka
					WHEN 'g0gdw9d53dm32raz0h46vrsqgbr9417l' THEN '500400253' --Kafue East
					WHEN 'tuzn5ssrponmtkuw06wsr1plserg46h3' THEN '500400251' --Mtendere
					WHEN '7f2pvfn4p4t60gs4uez20t6hxvjm2ftv' THEN '500400021' --Mungu
					WHEN 'uk1oj1tva6bjmiknoj664bin2bkkdjn8' THEN '500400260' --Kabweza
					WHEN 'n48r3u184v9ty34953tr4fduagd12wfv' THEN '500400080' --Old Kabweza
				END
	)

IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @NHC)
UNION
SELECT SubSiteId FROM dbo.fn_GetFacilitySubSiteIds(@PID2, @DID2, @FID2);
    
IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL) DROP TABLE #AllCurrPrEPClients;
IF(OBJECT_ID('tempdb..#NewOnArt') IS NOT NULL)DROP TABLE #NewOnArt;
	


CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
create index DiscontinuedPats on #Discontinued(PatientId)

CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
create index DeadPats on #DeadClients(PatientId)


CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);


CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);

CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId       INT PRIMARY KEY,
    InteractionDate DATE
);

CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
    
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
create index CurrentPats on #CurrentOnArt(PatientId)

CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);
create index PrepPats on #AllCurrPrEPClients (PatientId);

CREATE TABLE #NewOnArt(
	PatientId			INT,
	ArtStartDate		DATETIME,
	AgeAtArtStart		INT,
	Sex					VARCHAR(1),
	ArtStartEditLocation INT,
	IhapDate			DATETIME,
	IhapEditLocation	INT,
	OldestHivPosTestDate DATETIME,
	RoC_Status			VARCHAR(50),
	TX_ML_Date			DATETIME,
	NUPN				VARCHAR(50),
	ArtNumber			VARCHAR(50),
	HtsToArtStartDays	INT,
	QcFlag				INT
);

exec DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@Threshold,
	1;

EXEC DataTech.dbo.[proc_GetTxNew_base]
	@StartDate,
	@EndDate,
	@Threshold;

IF(OBJECT_ID('tempdb..#Regimen') IS NOT NULL) DROP TABLE #Regimen;
CREATE TABLE #Regimen(
	Interaction_Id	INT NOT NULL,-- PRIMARY KEY,
	PatId			INT NOT NULL,
	VisitDate		DATETIME NOT NULL,
	NextVisitDate	DATETIME,
	DailyDrugStock	INT,
	ArvRegimen		VARCHAR(100),
	EditLocation	INT,
	ServiceArea		VARCHAR(100),
	ArtPasserby		INT,
	PepDispensation	INT
);

EXEC DataTech.[dbo].[proc_ArvRegimenDatesAndQuanitity];  


IF (object_id('tempdb..#TX_New_ClientsOfInterest') IS NOT NULL) DROP TABLE #TX_New_ClientsOfInterest;
SELECT 
	chs.PatientId, 
	chs.ArtStartDate, 
	chs.AgeAtArtStart AgeArtStart, 
	chs.Sex
INTO #TX_New_ClientsOfInterest
FROM #NewOnArt chs;

CREATE INDEX Indx_TX_New_ClientsOfInterest_PatientId ON #TX_New_ClientsOfInterest (PatientId);

--My code
if(object_id('tempdb..#TLDStart') is not null) drop table #TLDStart;
if(object_id('tempdb..#TLDCohort') is not null) drop table #TLDCohort;
if(object_id('tempdb..#TLEStart') is not null) drop table #TLEStart;
if(object_id('tempdb..#TLECohort') is not null) drop table #TLECohort;
if(object_id('tempdb..#TafEDStart') is not null) drop table #TafEDStart;
if(object_id('tempdb..#TafEDCohort') is not null) drop table #TafEDCohort;
if(object_id('tempdb..#TXNew') is not null) drop table #TXNew;
if(object_id('tempdb..#TXCurr') is not null) drop table #TXCurr;
if(object_id('tempdb..#Dispensations') is not null) drop table #Dispensations;
--if(object_id('tempdb..#Name') is not null) drop table #Name



-- TLE, TLD and TafED cohorts
select 
	rank() over(partition by PatId order by VisitDate) RxCount, 
	* 
into #TLECohort 
from #Regimen 
	where (VisitDate <= @EndDate and NextVisitDate >= dateadd(day, -@Threshold, @StartDate)) 
	and (ArvRegimen like '%TDF%' and ArvRegimen like '%[3F]TC%' and ArvRegimen like '%EFV%' and ArvRegimen not like '%DTG%');
select 
	rank() over(partition by PatId order by VisitDate) RxCount, 
	* 
into #TafEDCohort 
from #Regimen 
	where (VisitDate <= @EndDate and NextVisitDate >= dateadd(day, -@Threshold, @StartDate)) 
	and (ArvRegimen like '%TAF%' and ArvRegimen like '%[3F]TC%' and ArvRegimen like '%DTG%' and ArvRegimen not like '%EFV%');
select 
	rank() over(partition by PatId order by VisitDate) RxCount, 
	* 
into #TLDCohort 
from #Regimen 
	where (VisitDate <= @EndDate and NextVisitDate >= dateadd(day, -@Threshold, @StartDate)) 
	and (ArvRegimen like '%TDF%' and ArvRegimen like '%[3F]TC%' and ArvRegimen like '%DTG%' and ArvRegimen not like '%EFV%');
select distinct * into #TafEDStart from #TafEDCohort where RxCount = 1 and VisitDate between @StartDate and @EndDate;
select distinct * into #TLEStart from #TLECohort where RxCount = 1 and VisitDate between @StartDate and @EndDate;
select distinct * into #TLDStart from #TLDCohort where RxCount = 1 and VisitDate between @StartDate and @EndDate;
select distinct *,
case when AgeArtStart <15 then '<15' else '>=15' end Course_AgeCat,
case
	when AgeArtStart >=0 and AgeArtStart <1 then '<01'
	when AgeArtStart >=1 and AgeArtStart <5 then '''01-04'
	when AgeArtStart >=5 and AgeArtStart <10 then '''05-09'
	when AgeArtStart >=10 and AgeArtStart <15 then '''10-14'
	when AgeArtStart >=15 and AgeArtStart <20 then '15-19'
	when AgeArtStart >=20 and AgeArtStart <25 then '20-24'
	when AgeArtStart >=25 and AgeArtStart <30 then '25-29'
	when AgeArtStart >=30 and AgeArtStart <35 then '30-34'
	when AgeArtStart >=35 and AgeArtStart <40 then '35-39'
	when AgeArtStart >=40 and AgeArtStart <45 then '40-44'
	when AgeArtStart >=45 and AgeArtStart <50 then '45-49'
	when AgeArtStart >=50 and AgeArtStart <55 then '50-54'
	when AgeArtStart >=55 and AgeArtStart <60 then '55-59'
	when AgeArtStart >=60 and AgeArtStart <65 then '60-64'
	when AgeArtStart >=65 then '65+'
	else 'Unknown'
end Fine_AgeCat
into #TXNew 
from #TX_New_ClientsOfInterest;

select distinct *,
case when AgeLastVisit <15 then '<15' else '>=15' end Course_AgeCat,
case
	when AgeLastVisit >=0 and AgeLastVisit <1 then '<01'
	when AgeLastVisit >=1 and AgeLastVisit <5 then '''01-04'
	when AgeLastVisit >=5 and AgeLastVisit <10 then '''05-09'
	when AgeLastVisit >=10 and AgeLastVisit <15 then '''10-14'
	when AgeLastVisit >=15 and AgeLastVisit <20 then '15-19'
	when AgeLastVisit >=20 and AgeLastVisit <25 then '20-24'
	when AgeLastVisit >=25 and AgeLastVisit <30 then '25-29'
	when AgeLastVisit >=30 and AgeLastVisit <35 then '30-34'
	when AgeLastVisit >=35 and AgeLastVisit <40 then '35-39'
	when AgeLastVisit >=40 and AgeLastVisit <45 then '40-44'
	when AgeLastVisit >=45 and AgeLastVisit <50 then '45-49'
	when AgeLastVisit >=50 and AgeLastVisit <55 then '50-54'
	when AgeLastVisit >=55 and AgeLastVisit <60 then '55-59'
	when AgeLastVisit >=60 and AgeLastVisit <65 then '60-64'
	when AgeLastVisit >=65 then '65+'
	else 'Unknown'
end Fine_AgeCat
into #TXCurr from #CurrentOnArt
create index TXCurrPId_index on #TXCurr(PatientId);

--Dataset with Sex and AgeCats as dependent variable
select 
	@QP [Period],
	@PN Province, 
	@DN District, 
	case @FId
		--Chilanga Aliases
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010005T' THEN 'Tubalange Mini Hospital'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'Desai Health Post'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		-- Chirundu Aliases
		-- Luangwa Aliases
		-- Rufunsa Aliases
		WHEN '500700241' THEN 'Lubalashi Health Post'
		else @FN 
	end Facility, @FId FacilityCode, * 
from (
		select 'TLE_Curr_On' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXCurr where PatientId in (select PatId from #TLECohort) and PatientId not in (select PatId from #TLDCohort) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TLE_New_Naive' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXNew where PatientId in (select PatId from #TLEStart) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TLE_New_Transitioned' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXCurr where PatientId in (select PatId from #TLEStart) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TLD_Curr_On' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXCurr where PatientId in (select PatId from #TLDCohort) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TLD_New_Naive' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXNew where PatientId in (select PatId from #TLDStart) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TLD_New_Transitioned' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXCurr where PatientId in (select PatId from #TLDStart) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TafED_Curr_On' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXCurr where PatientId in (select PatId from #TafEDCohort) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TafED_New_Naive' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXNew where PatientId in (select PatId from #TafEDStart) group by Sex, Fine_AgeCat, Course_AgeCat 
		union
		select 'TafED_New_Transitioned' Indicator, Sex, Fine_AgeCat, Course_AgeCat, count(patientid) PatCount from #TXCurr where PatientId in (select PatId from #TafEDStart) group by Sex, Fine_AgeCat, Course_AgeCat
) Dataset
order by Indicator, Sex, Fine_AgeCat, Course_AgeCat;


--SELECT COUNT(*) FROM #CurrentOnArt