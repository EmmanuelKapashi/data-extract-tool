USE cdc_fdb_db;

    DECLARE @StartDate DATE
    DECLARE @EndDate DATE
    DECLARE @ProvinceId VARCHAR(2)
    DECLARE @DistrictId VARCHAR(3)
    DECLARE @FacilityId VARCHAR(9)
    DECLARE @LoginName VARCHAR(50)
    DECLARE 
		@LTFUThreshold INTEGER,
		@FacilityGuid VARCHAR(100), 
		@ProvinceName VARCHAR(100), 
		@DistrictName VARCHAR(100), 
		@FacilityName VARCHAR(100); 
    
	SET @StartDate  = '1-oct-2021'
    SET @EndDate = '31-dec-2021 23:59:59'
    SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
	SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
	SET @FacilityId = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
	SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
    SET @LoginName = 'SmartCare Installer'
    SET @LTFUThreshold = 30;

	SELECT 
		@ProvinceName = p.[Name], 
		@DistrictName = d.[Name], 
		@FacilityName = f.FacilityName 
	FROM Facility f 
	JOIN District d ON f.DistrictId = d.Code 
	JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
		WHERE F.FacilityGuid = @FacilityGuid;

	--From facilities, get AND filter only the current facility in the current province AND district
    IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
        DROP TABLE #CurrentFacility -- Renamed FROM facility filter
    SELECT DISTINCT SubSiteId
    INTO #CurrentFacility
    FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @FacilityId);

-- TX_CURR	   
IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
    
CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);
CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);
CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId           INT PRIMARY KEY,
    InteractionDate     DATE
);
CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);
--************************* End Temp Table Declarations *****************************************
	--=================================== END TEST AREA ======================================= --PASS2

EXEC DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate;

IF(OBJECT_ID('tempdb..#Regimen') IS NOT NULL) DROP TABLE #Regimen;
CREATE TABLE #Regimen(
	Interaction_Id	INT NOT NULL,-- PRIMARY KEY,
	PatId			INT NOT NULL,
	VisitDate		DATETIME NOT NULL,
	NextVisitDate	DATETIME,
	DailyDrugStock	INT,
	ArvRegimen		VARCHAR(100),
	EditLocation	INT,
	ServiceArea		VARCHAR(100),
	ArtPasserby		INT,
	PepDispensation	INT
);
EXEC DataTech.dbo.[proc_ArvRegimenDatesAndQuanitity];

IF(OBJECT_ID('tempdb..#CurrentRegimen') IS NOT NULL) DROP TABLE #CurrentRegimen
SELECT DISTINCT 
	a.PatId,
	a.Interaction_Id,
	a.VisitDate,
	a.NextVisitDate,
	a.DailyDrugStock,
	a.ArvRegimen
INTO #CurrentRegimen
FROM (
		SELECT DISTINCT 
			ROW_NUMBER() OVER(PARTITION BY PatId ORDER BY VisitDate DESC) Seq,
			*
		FROM #Regimen
			WHERE (VisitDate >= @StartDate AND VisitDate <= @EndDate)
				OR (VisitDate < @StartDate AND NextVisitDate >= DATEADD(DAY, -@LTFUThreshold, @EndDate))
) a
	WHERE a.Seq = 1;
--All women with ANC visit in reporting period AND their HIV status
    IF (OBJECT_ID('tempdb..#ANCClientsWithHIVStatusDetailsAtEntry')) IS NOT NULL
        DROP TABLE #ANCClientsWithHIVStatusDetailsAtEntry
    SELECT DISTINCT *
    INTO #ANCClientsWithHIVStatusDetailsAtEntry
    FROM (
				SELECT 
					ANC.PatientId,
					CHS.TestResult HivStatus,
					DATEADD(dd, DATEDIFF(dd, 0, CHS.TestDate), 0) AS HivStatusDate,
					COALESCE(ANC.HIVTestResultV5, ANC.HIVTestResult) VisitTestResult, -- 0 = POS, 1 = NEG, 2 = IND
					DATEADD(dd, DATEDIFF(dd, 0, ANC.VisitDate), 0)     AS VisitDate,
					dbo.fn_Age(REG.DateOfBirth, ANC.VisitDate)         AS Age,
					CASE 
						WHEN (ihp.PatientId IS NULL AND ANC.PMTCTKnownStatus IS NOT NULL)
							OR (ihp.PatientId IS NOT NULL AND ANC.VisitDate <= ihp.VisitDate)
							THEN ANC.PMTCTKnownStatus
						WHEN ihp.PatientId IS NOT NULL AND ANC.VisitDate > ihp.VisitDate THEN 1
					END PMTCTKnownStatus -- 0 = NEG, 1 = POS, 2 = UNK
				FROM crtANCVisit AS ANC
				LEFT JOIN #MostRecentHIVStatusAsOfEndDate AS CHS ON CHS.PatientId = ANC.PatientId AND CHS.TestDate < ANC.VisitDate 
					AND ((CHS.TestResult = 1 AND CHS.TestDate >= DATEADD(MONTH, -3, ANC.VisitDate)) OR (CHS.TestResult = 2))
				JOIN crtRegistrationInteraction REG ON REG.patientId_int = ANC.PatientId
				LEFT JOIN crtIHAP ihp ON ANC.PatientId = ihp.PatientId AND ihp.VisitDate <= @EndDate
					  WHERE ANC.VisitType = 259
					  AND ANC.VisitDate >= @StartDate
					  AND ANC.VisitDate <= @EndDate
				  
		) ANC
    CREATE INDEX Indx_ANCClientsWithHIVStatusDetailsAtEntry_PatientId
        ON #ANCClientsWithHIVStatusDetailsAtEntry (PatientId);
		
	--Number of pregnant women with known HIV status (includes women who are known positives at entry AND those who were tested for HIV AND received their results)
    --All women who WHERE known positive prior to ANC Positive
    IF (object_id('tempdb..#WomenKnownPositivePriorANC')) IS NOT NULL
        DROP TABLE #WomenKnownPositivePriorANC
    SELECT *
    INTO #WomenKnownPositivePriorANC
    FROM (SELECT *
          FROM #ANCClientsWithHIVStatusDetailsAtEntry
          WHERE (HivStatus = 1 OR PMTCTKnownStatus = 1)
			AND VisitTestResult IS NULL) KnownPos

    --All women who were found positive in ANC
    IF (object_id('tempdb..#WomenIdentifiedPositiveInANC')) IS NOT NULL
        DROP TABLE #WomenIdentifiedPositiveInANC
    SELECT *
    INTO #WomenIdentifiedPositiveInANC
    FROM (SELECT *
          FROM #ANCClientsWithHIVStatusDetailsAtEntry
          WHERE VisitTestResult = 0) PosANC

    --All women who were found to be negative in ANC
    IF (object_id('tempdb..#WomenIdentifiedNegativeInANC')) IS NOT NULL
        DROP TABLE #WomenIdentifiedNegativeInANC
    SELECT *
    INTO #WomenIdentifiedNegativeInANC
    FROM (SELECT *
          FROM #ANCClientsWithHIVStatusDetailsAtEntry
          WHERE VisitTestResult = 1) NegANC

    --All women who are recent negative at entry
    IF (object_id('tempdb..#WomenRecentNegativeAtEntry')) IS NOT NULL
        DROP TABLE #WomenRecentNegativeAtEntry
    SELECT *
    INTO #WomenRecentNegativeAtEntry
    FROM (SELECT *
          FROM #ANCClientsWithHIVStatusDetailsAtEntry
          WHERE
            --consider adding a time period to be more specific as to what is recent
              HivStatusDate <= VisitDate
            AND (HivStatus = 2 OR PMTCTKnownStatus = 0)
			AND VisitTestResult IS NULL) RecentNeg

    --All women that know their HIV Status in ANC
    IF (object_id('tempdb..#WomenWhoKnowStatusInANC')) IS NOT NULL
        DROP TABLE #WomenWhoKnowStatusInANC
    SELECT *
    INTO #WomenWhoKnowStatusInANC
    FROM (SELECT *
          FROM #ANCClientsWithHIVStatusDetailsAtEntry
          WHERE VisitTestResult IS NOT NULL
            AND PMTCTKnownStatus IS NULL) KnowStatus
    --group all women who know their status inclusive of known positives before ANC visit
    IF (object_id('tempdb..#AllPregnantWomenThatKnowStatus')) IS NOT NULL
        DROP TABLE #AllPregnantWomenThatKnowStatus;
    CREATE TABLE #AllPregnantWomenThatKnowStatus
    (
        PatientId        INT      NOT NULL,
        HivStatus        INT      NULL,
        HivStatusDate    DATETIME NULL,
        HIVTestResultV5  INT      NULL,
        VisitDate        DATETIME NULL,
        Age              INT      NULL,
        PMTCTKnownStatus INT      NULL,
		PMTCT_STAT_Cat	 VARCHAR(50) NULL
    );
    --insert all prior known positives to AllWomenThatKnowStatus master table
    INSERT INTO #AllPregnantWomenThatKnowStatus
	SELECT *, 'Recent Negative' 
	FROM #WomenRecentNegativeAtEntry
	UNION
	SELECT *, 'New Negative' 
	FROM #WomenIdentifiedNegativeInANC
	UNION
	SELECT *, 'Known Positive' 
	FROM #WomenKnownPositivePriorANC
	UNION
	SELECT *, 'New Positive' 
	FROM #WomenIdentifiedPositiveInANC;

	--SELECT * FROM #AllPregnantWomenThatKnowStatus;

	SELECT 
		@ProvinceName Province,
		@DistrictName District,
		CASE @FacilityId
			--Chilanga Aliases
			WHEN '50010005A' THEN 'Mundengwa'
			WHEN '50010005T' THEN 'Tubalange Mini Hospital'
			WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
			--Chongwe Aliases
			WHEN '500300GC0' THEN 'Chongwe District Hospital'
			--Kafue Aliases
			WHEN '500400021' THEN 'Mungu'
			WHEN '500400053' THEN 'Lukolongo'
			--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
			WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
			WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025M' THEN 'Mtendere Urban Health Centre' -- Duplicate HMIS Code used
			--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
			WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
			--WHEN '500400261' THEN 'New Kabweza Health Post'
			-- Lusaka Aliases
			WHEN '500600308' THEN 'Chainda Market Community Post'
			WHEN '500600329' THEN 'Chaisa Urban Health Centre'
			WHEN '50060035Y' THEN 'John Howard - Discover Health'
			WHEN '50060035X' THEN 'Saint Lukes - JSI'
			WHEN '50060036B' THEN 'Chazanga Health Post'
			WHEN '50060036C' THEN 'Chazanga Community Post'
			WHEN '50060037X' THEN 'Chelstone CBTO'
			WHEN '50060037O' THEN 'Obama Market Community Post'
			WHEN '50060046X' THEN 'Chimwemwe Health Post'
			WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
			WHEN '50060046K' THEN 'Katambalala Community Post'
			WHEN '500600JH3' THEN 'State House Clinic'
			WHEN '500600JH0' THEN 'State House Clinic'
			WHEN '50060106B' THEN 'Desai Health Post'
			WHEN '50060106C' THEN 'Desai Health Post'
			WHEN '50060106A' THEN 'Desai Health Post'
			WHEN '50060106X' THEN 'Jordan Health Post'
			WHEN '50060124A' THEN 'Lumumba Market Community Post'
			WHEN '500601291' THEN 'Kabwata Market Community Post'
			WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
			WHEN '50060135B' THEN 'Comesa Market Community Post'
			WHEN '50060135A' THEN 'Lumbama Market  Community Post'
			WHEN '50060136U' THEN 'Bethel Linda Health Post'
			WHEN '50060136Y' THEN 'John Laing Health Post'
			WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
			WHEN '500601361' THEN 'Pamodzi Community Post'
			WHEN '50060137P' THEN 'Mbasela Health Post'
			WHEN '50060138B' THEN 'Garden House Health Post'
			WHEN '500601381' THEN 'Masauko Community Post'
			WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
			WHEN '50060143A' THEN 'Chawama Central Market Community Post'
			WHEN '50060180N' THEN 'Mandevu Community Post'
			WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
			WHEN '50060198N' THEN 'Mtendere Market Community Post'
			WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
			WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
			WHEN '50060243A' THEN 'Chachacha Market Community Post'
			WHEN '50060243C' THEN 'PPAZ'
			WHEN '5006XX96A' THEN 'Chunga Community Post'
			WHEN '5006XX96B' THEN 'Twikatane Community Post'
			-- Chirundu Aliases
			-- Luangwa Aliases
			-- Rufunsa Aliases
			WHEN '500700241' THEN 'Lubalashi Health Post'
			ELSE @FacilityName
		END Facility,
		@FacilityId FacilityHMIS,
		COALESCE(x.PMTCT_STAT_Cat, 'Status Unknown') PMTCT_STAT_Cat,
		x.ANC1Pats
	FROM (
			SELECT 
				b.PMTCT_STAT_Cat,
				COUNT(DISTINCT a.PatientId) ANC1Pats
			FROM #ANCClientsWithHIVStatusDetailsAtEntry a
			LEFT JOIN #AllPregnantWomenThatKnowStatus b ON a.PatientId = b.PatientId
				GROUP BY b.PMTCT_STAT_Cat
	) x