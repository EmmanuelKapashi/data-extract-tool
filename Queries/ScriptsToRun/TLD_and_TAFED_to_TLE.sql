
--For each month of FY20, [how many on TLE in present month/visit and] how many transittion to TLD from TLE in current month
use cdc_fdb_db;

declare   
	@StartDate  DATETIME,
    @EndDate DATETIME,
    @FYStartDate DATETIME,
    @FYStartMonth varchar(6),
	@StopDate datetime,
	@ReportingMonth varchar(6),
	@ReportingMonth2 varchar(6),
	@ReportingMonth3 varchar(6),
	@FId varchar(10),
    @Q varchar(6),
    @QP varchar(10),
	@Threshold int,
	@PN varchar(100),
	@DN varchar(100),
	@FN varchar(100),
	@FG nvarchar(100),
	@NHC varchar(10),
	@OHC nvarchar(10),
	@PROVINCEID VARCHAR(2),
	@DISTRICTID VARCHAR(2)
	
--set start and end dates
set @FYStartDate = '20210901'
set @FYStartMonth = convert(varchar(6), @FYStartDate, 112)
set @StartDate = @FYStartDate
set @EndDate = eomonth(@StartDate)
set @EndDate = dateadd(second, 86399, @EndDate) --adding 23:59:59
set @StopDate = getdate()
set @ReportingMonth = '202111'
--set @ReportingMonth2 = '202108'
--set @ReportingMonth3 = '202107'
set @Threshold = 30
set @Q = convert(varchar(6), @StartDate, 112);
set @QP = (
		CASE DATEDIFF(MONTH, @StartDate, @EndDate) 
			WHEN 0 THEN convert(nvarchar(6), @StartDate,112) 
			WHEN 2 THEN 
				CASE
					WHEN  DATEPART(MONTH, @StartDate) IN (10, 11, 12) THEN CONCAT('FY', RIGHT((DATENAME(YEAR, @StartDate) +1), 2), (DATEPART(QUARTER, @StartDate)+1))
					WHEN  DATEPART(MONTH, @StartDate) NOT IN (10, 11, 12) THEN CONCAT('FY', RIGHT(DATENAME(YEAR, @StartDate), 2), 'Q', (DATEPART(QUARTER, @StartDate)+1))
				END
			ELSE 'UNDEFINED'
		END
);
set @FG = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid')
set @FId = (SELECT [Value] FROM Setting WHERE [Name] = 'HmisCode')
set @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId')
set @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId')

SELECT TOP 1 
	@PN = p.Name, 
	@DN=d.Name, 
	@FN=f.FacilityName, 
	@NHC=M.NewHMISCode, 
	@OHC=M.OldHMISCode 
FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG

 --From facilities, get and filter only the current facility in the current province and district
  IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility -- Renamed from facility filter
  SELECT DISTINCT SubSiteId
      INTO #CurrentFacility
  FROM FacilitySubSite
	WHERE FacilityGuid = @FG

if(object_id('tempdb..#TLDpats') is not null) drop table #TLDpats
create table #TLDpats(
	PatID int not null,
	Sex varchar(1),
	CourseAgeCat varchar(10), 
	FineAgeCat varchar(10),
	ArtStartDate datetime,
	LastVisitDate datetime,
	Cohort varchar(6)
)

if(object_id('tempdb..#TafEDpats') is not null) drop table #TafEDpats
create table #TafEDpats(
	PatID int not null,
	Sex varchar(1),
	CourseAgeCat varchar(10), 
	FineAgeCat varchar(10),
	ArtStartDate datetime,
	LastVisitDate datetime,
	Cohort varchar(6)
)

if(object_id('tempdb..#TLEpats') is not null) drop table #TLEpats
create table #TLEpats(
	PatID int not null,
	Sex varchar(1),
	CourseAgeCat varchar(10), 
	FineAgeCat varchar(10),
	ArtStartDate datetime,
	LastVisitDate datetime,
	Cohort varchar(6)
)

while @EndDate <= @StopDate
begin

--Trancate the seconds off the start and end date time
  SET @StartDate = DATEADD(dd, DATEDIFF(dd, 0, @StartDate), 0)
  SET @EndDate = DATEADD(dd, DATEDIFF(dd, 0, @EndDate), 0)

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL) DROP TABLE #AllCurrPrEPClients;



CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
create index DiscontinuedPats on #Discontinued(PatientId)

CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
create index DeadPats on #DeadClients(PatientId)


CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);


CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);

CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId       INT PRIMARY KEY,
    InteractionDate DATE
);

CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
    
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
create index CurrentPats on #CurrentOnArt(PatientId)

CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);
create index PrepPats on #AllCurrPrEPClients (PatientId);

exec DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@Threshold,
	1

IF (OBJECT_ID('tempdb..#TX_New_ClientsOfInterest') IS NOT NULL) DROP TABLE #TX_New_ClientsOfInterest;
SELECT c.PatientId, c.ArtStartDate, c.Age AgeArtStart, c.Sex
INTO #TX_New_ClientsOfInterest
FROM #CurrentOnArt c 
LEFT JOIN crtPatientLocator loc on c.PatientId = loc.PatientId
	WHERE c.ArtStartDate IS NOT NULL
	AND c.ArtStartDate >= @StartDate
	AND c.ArtStartDate <= @EndDate
	AND (
		(loc.PatientTransfer <> 2 OR loc.PatientTransfer IS NULL) 
		AND loc.VisitDate between @StartDate and @EndDate 
		AND loc.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
		)
	

CREATE INDEX Indx_TX_New_ClientsOfInterest_PatientId
ON #TX_New_ClientsOfInterest (PatientId);


--My code
if(object_id('tempdb..#TLDStart') is not null) drop table #TLDStart
if(object_id('tempdb..#TLDCohort') is not null) drop table #TLDCohort
if(object_id('tempdb..#TafEDStart') is not null) drop table #TafEDStart
if(object_id('tempdb..#TafEDCohort') is not null) drop table #TafEDCohort
if(object_id('tempdb..#TLECohort') is not null) drop table #TLECohort
if(object_id('tempdb..#TXNew') is not null) drop table #TXNew
if(object_id('tempdb..#TXCurr') is not null) drop table #TXCurr
if(object_id('tempdb..#Dispensations') is not null) drop table #Dispensations
--if(object_id('tempdb..#Name') is not null) drop table #Name

--Drug Dispensations
select DISTINCT dense_rank() over(partition by InteractionID order by VisitDate, GenericName) DC, * 
into #Dispensations 
from (
	select distinct DD.InteractionID, P.PatientId, P.VisitDate, P.NextAppointmentDate, D.GenericName, 
		case
			when D.GenericName = 'Dolutegravir + emtricitabine + Tenofovir alafenamide' then 'TAF+FTC+DTG'
			when D.GenericName = 'abacavir+lamivudine+zidovudine' then 'ABC+3TC+AZT'
			when D.GenericName = 'amprenavir' then 'APV'
			when D.GenericName = 'atazanavir+ritanovir' then 'ATV/r'
			when D.GenericName in ('efavirenz+lamivudine+tenofovir', 'Tenofovir, Lamivudine and Efavirenz') then 'TDF+3TC+EFV'
			when D.GenericName = 'efavirenz+emtricitabine+tenofovir' then 'TDF+FTC+EFV'
			when D.GenericName = 'lamivudine+nevirapine+stavudine' then 'D4T+3TC+NVP'
			when D.GenericName = 'lamivudine+nevirapine+zidovudine' then 'AZT+3TC+NVP'
			when D.GenericName = 'lamivudine+stavudine' then '3TC+D4T'
			when D.GenericName = 'lopinavir+ritonavir' then 'LPV/r'
			when D.GenericName in ('zidovudine syrup', 'zidovudine') then 'AZT'
			else D.Synonym
		end Abbr
	from crctMedicationDispensingDetails DD
	left join crtPharmacyVisit P on DD.InteractionID = P.InteractionID
	left join DrugProductsCachedView D on DD.MedDrugId = D.MedDrugId
		where P.ArvDrugDispensed = 1
		and P.VisitDate <= @EndDate
		and D.DrugClass = 'Antiretrovirals'
		and D.GenericName not in ('Nifedipine', 'atenolol', 'Adrenaline, Iml amp', 'Amiodarone HCL', 'Amlodipine', 'Analgin',
		'Analgin', 'Atorvastatin', 'captopril', 'carvedilol', 'Clopidogrel', 'delavirdine mesylate', 'Dobutamine',
		'Enoxaparin Sodium Syringe (Pre-Fill)', 'Erythropoetin, 0.5ml Prefilled Syringes', 'Fentanyl', 'folic acid',
		'Gliclazide', 'Glimepiride', 'Hydroxyurea', 'Iron Sucrose', 'isosorbide dinitrate', 'Losartan + Hydrochlorthiazide',
		'Nifedipine, Sublingual', 'Normal Saline', 'Ondansetron (as hydrochloride)', 'Ondansetrone Mouth dissolving',
		'Pancuronium Bromide', 'Paracetamol + Codeine Phosphate', 'Propofol, 20ml', 'Telmisartan',
		'Thiamine+Riboflavin+Pyridoxine+Cyanocobalamin+Nicotinmide+calcium', 'Tranexamic Acid')
) X
	order by PatientId, VisitDate, DC

if(object_id('tempdb..#DX') is not null) drop table #DX
select max(DC) DCmax, InteractionID IID into #DX from #Dispensations
	group by InteractionID
	order by InteractionID;
--SELECT IID, COUNT(DCmax) FROM #DX GROUP BY IID HAVING COUNT(DCmax) >1

if(object_id('tempdb..#Regimen') is not null) drop table #Regimen;
create table #Regimen(
	Interaction_Id int not null, -- primary key,
	PatId int not null,
	VisitDate datetime not null,
	NextVisitDate datetime,
	ArvRegimen nvarchar(100)
)

insert into #Regimen 
select distinct S.InteractionID, S.PatientId, S.VisitDate, S.NextAppointmentDate, Abbr ArvRegimen
from #Dispensations S
left join #DX X on S.InteractionID = X.IID
	where X.DCmax = 1

insert into #Regimen 
select distinct SubA.InteractionID, SubA.PatientId, SubA.VisitDate, SubA.NextAppointmentDate, concat(SubA.Abbr,'+',SubB.Abbr) Regimen
from (
	select distinct S.DC, S.InteractionID, S.PatientId, S.VisitDate, S.NextAppointmentDate, Abbr
	from #Dispensations S
	left join #DX X on S.InteractionID = X.IID
		where X.DCmax = 2
		and S.DC = 1
) SubA
left join (
	select distinct S.DC, S.InteractionID, S.PatientId, S.VisitDate, S.NextAppointmentDate, Abbr
	from #Dispensations S
	left join #DX X on S.InteractionID = X.IID
		where X.DCmax = 2
		and S.DC = 2
) SubB on SubA.InteractionID = SubB.InteractionID

insert into #Regimen 
select distinct SubA.InteractionID, SubA.PatientId, SubA.VisitDate, SubA.NextAppointmentDate, concat(SubA.Abbr,'+',SubB.Abbr,'+',SubC.Abbr) Regimen
from (
	select distinct S.DC, S.InteractionID, S.PatientId, S.VisitDate, S.NextAppointmentDate, Abbr
	from #Dispensations S
	left join #DX X on S.InteractionID = X.IID
		where X.DCmax = 3
		and S.DC = 1
) SubA
left join (
	select distinct S.DC, S.InteractionID, S.PatientId, S.VisitDate, S.NextAppointmentDate, Abbr
	from #Dispensations S
	left join #DX X on S.InteractionID = X.IID
		where X.DCmax = 3
		and S.DC = 2
) SubB on SubA.InteractionID = SubB.InteractionID
left join (
	select distinct S.DC, S.InteractionID, S.PatientId, S.VisitDate, S.NextAppointmentDate, Abbr
	from #Dispensations S
	left join #DX X on S.InteractionID = X.IID
		where X.DCmax = 3
		and S.DC = 3
) SubC on SubA.InteractionID = SubC.InteractionID
create index RxIndex on #Regimen(Interaction_Id)

--TLD and TafED cohorts
select rank() over(partition by PatId order by VisitDate) RxCount, * into #TafEDCohort from #Regimen where ArvRegimen like '%TAF%' and ArvRegimen like '%[3F]TC%' and ArvRegimen like '%DTG%'
select rank() over(partition by PatId order by VisitDate) RxCount, * into #TLDCohort from #Regimen where ArvRegimen like '%TDF%' and ArvRegimen like '%[3F]TC%' and ArvRegimen like '%DTG%'
select rank() over(partition by PatId order by VisitDate) RxCount, * into #TLECohort from #Regimen where ArvRegimen like '%TDF%' and ArvRegimen like '%[3F]TC%' and ArvRegimen like '%EFV%'
select distinct * into #TafEDStart from #TafEDCohort where RxCount = 1 and VisitDate between @StartDate and @EndDate
select distinct * into #TLDStart from #TLDCohort where RxCount = 1 and VisitDate between @StartDate and @EndDate
select distinct *,
case 
	when AgeArtStart <15 then '<15'
	when AgeArtStart >=15 and AgeArtStart <20 then '15-19'
	when AgeArtStart >19 then '+20'
	else 'Unknown' end Course_AgeCat,
case
	when AgeArtStart <1 then '<01'
	when AgeArtStart >=1 and AgeArtStart <5 then '''01-04'
	when AgeArtStart >=5 and AgeArtStart <10 then '''05-09'
	when AgeArtStart >=10 and AgeArtStart <15 then '''10-14'
	when AgeArtStart >=15 and AgeArtStart <20 then '15-19'
	when AgeArtStart >=20 and AgeArtStart <25 then '20-24'
	when AgeArtStart >=25 and AgeArtStart <30 then '25-29'
	when AgeArtStart >=30 and AgeArtStart <35 then '30-34'
	when AgeArtStart >=35 and AgeArtStart <40 then '35-39'
	when AgeArtStart >=40 and AgeArtStart <45 then '40-44'
	when AgeArtStart >=45 and AgeArtStart <50 then '45-49'
	when AgeArtStart >=50 then '50+'
	else 'Unknown'
end Fine_AgeCat
into #TXNew from #TX_New_ClientsOfInterest

select distinct *,
case 
	when AgeLastVisit <15 then '<15'
	when AgeLastVisit >=15 and AgeLastVisit <20 then '15-19'
	when AgeLastVisit >19 then '+20'
	else 'Unknown' end Course_AgeCat,
case
	when AgeLastVisit <1 then '<01'
	when AgeLastVisit >=1 and AgeLastVisit <5 then '''01-04'
	when AgeLastVisit >=5 and AgeLastVisit <10 then '''05-09'
	when AgeLastVisit >=10 and AgeLastVisit <15 then '''10-14'
	when AgeLastVisit >=15 and AgeLastVisit <20 then '15-19'
	when AgeLastVisit >=20 and AgeLastVisit <25 then '20-24'
	when AgeLastVisit >=25 and AgeLastVisit <30 then '25-29'
	when AgeLastVisit >=30 and AgeLastVisit <35 then '30-34'
	when AgeLastVisit >=35 and AgeLastVisit <40 then '35-39'
	when AgeLastVisit >=40 and AgeLastVisit <45 then '40-44'
	when AgeLastVisit >=45 and AgeLastVisit <50 then '45-49'
	when AgeLastVisit >=50 then '50+'
	else 'Unknown'
end Fine_AgeCat
into #TXCurr from #CurrentOnArt
create index TXCurrPId_index on #TXCurr(PatientId);


insert into #TLDpats
select distinct X.PatId, Y.Sex, Y.Course_AgeCat, Y.Fine_AgeCat, Y.ArtStartDate, X.VisitDate, @Q Cohort from #TLDCohort X 
left join #TXCurr Y on X.PatId = Y.PatientId
	where X.VisitDate between @StartDate and @EndDate

insert into #TafEDpats
select distinct X.PatId, Y.Sex, Y.Course_AgeCat, Y.Fine_AgeCat, Y.ArtStartDate, X.VisitDate, @Q Cohort from #TafEDCohort X 
left join #TXCurr Y on X.PatId = Y.PatientId
	where X.VisitDate between @StartDate and @EndDate

insert into #TLEpats
select distinct X.PatId, Y.Sex, Y.Course_AgeCat, Y.Fine_AgeCat, Y.ArtStartDate, X.VisitDate, @Q Cohort from #TLECohort X 
left join #TXCurr Y on X.PatId = Y.PatientId
	where X.VisitDate between @StartDate and @EndDate

set @StartDate = dateadd(month, 1, @StartDate)
set @EndDate = eomonth(@StartDate)
set @EndDate = dateadd(second, 86399, @EndDate) --adding 23:59:59
set @Q = convert(varchar(6), @StartDate, 112)

end
--**********************************************************************************************************************

if(object_id('tempdb..#DS') is not null) drop table #DS;

--Last TLD or TafED dispensation replaced by TLE (or both dispensed)

with TLDPats as --Most recent TLD or TafED dispensation
(
	select distinct PatID, max(Sex) Sex, max(CourseAgeCat) CourseAgeCat, max(FineAgeCat) FineAgeCat, max(ArtStartDate) ArtStartDate, max(Drug) Drug, max(LastVisitDate) LastVisitDate, max(Cohort) Cohort from (
		select *, 'TLD' Drug from #TLDpats
		union
		select *, 'TafED' Drug from #TafEDpats
	) P
		where Sex is not null
		group by PatID
),
TLEPats as --Most recent TLE dispensation
(
	select distinct PatID, max(Sex) Sex, max(CourseAgeCat) CourseAgeCat, max(FineAgeCat) FineAgeCat, max(ArtStartDate) ArtStartDate, max(LastVisitDate) LastVisitDate, max(Cohort) Cohort from #TLEpats
		where Sex is not null
		group by PatID
)
--All current TLE pats that were previously dispensed TLD or TafED
select D.*, E.LastVisitDate DateTransitioned, E.Cohort Transtioned into #DS from TLDPats D
inner join TLEPats E on D.PatID = E.PatID
	where D.LastVisitDate < E.LastVisitDate --'<' will exclude pats that are dispensed with both TLE and TLD; '<=' includes them
;

with ThisMonth as (
		select @ReportingMonth ReportPeriod, @PN Province, @DN District, Transtioned, 
		case @FId
			--Chilanga Aliases
			WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
			WHEN '50010005A' THEN 'Mundengwa'
			WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
			--Chongwe Aliases
			WHEN '500300GC0' THEN 'Chongwe District Hospital'
			--Kafue Aliases
			WHEN '500400021' THEN 'Mungu'
			WHEN '500400053' THEN 'Lukolongo'
			--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
			WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
			WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
			--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
			WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
			--WHEN '500400261' THEN 'New Kabweza Health Post'
			-- Lusaka Aliases
			WHEN '500600308' THEN 'Chainda Market Community Post'
			WHEN '500600329' THEN 'Chaisa Urban Health Centre'
			WHEN '50060035Y' THEN 'John Howard - Discover Health'
			WHEN '50060035X' THEN 'Saint Lukes - JSI'
			WHEN '50060036B' THEN 'Chazanga Health Post'
			WHEN '50060036C' THEN 'Chazanga Community Post'
			WHEN '50060037X' THEN 'Chelstone CBTO'
			WHEN '50060037O' THEN 'Obama Market Community Post'
			WHEN '50060046X' THEN 'Chimwemwe Health Post'
			WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
			WHEN '50060046K' THEN 'Katambalala Community Post'
			WHEN '500600JH3' THEN 'State House Clinic'
			WHEN '500600JH0' THEN 'State House Clinic'
			WHEN '50060106B' THEN 'Desai Health Post'
			WHEN '50060106C' THEN 'Desai Market Community Post'
			WHEN '50060106A' THEN 'George Community Post Unknown (1)'
			WHEN '50060106X' THEN 'Jordan Health Post'
			WHEN '50060124A' THEN 'Lumumba Market Community Post'
			WHEN '500601291' THEN 'Kabwata Market Community Post'
			WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
			WHEN '50060135B' THEN 'Comesa Market Community Post'
			WHEN '50060135A' THEN 'Lumbama Market  Community Post'
			WHEN '50060136U' THEN 'Bethel Linda Health Post'
			WHEN '50060136Y' THEN 'John Laing Health Post'
			WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
			WHEN '500601361' THEN 'Pamodzi Community Post'
			WHEN '50060137P' THEN 'Mbasela Health Post'
			WHEN '50060138B' THEN 'Garden House Health Post'
			WHEN '500601381' THEN 'Masauko Community Post'
			WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
			WHEN '50060143A' THEN 'Chawama Central Market Community Post'
			WHEN '50060180N' THEN 'Mandevu Community Post'
			WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
			WHEN '50060198N' THEN 'Mtendere Market Community Post'
			WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
			WHEN '50060211N' THEN 'Ng'+'''ombe Market Community Post'
			WHEN '50060243A' THEN 'Chachacha Market Community Post'
			WHEN '50060243C' THEN 'PPAZ'
			WHEN '5006XX96A' THEN 'Chunga Community Post'
			WHEN '5006XX96B' THEN 'Twikatane Community Post'
			else @FN 
		end Facility, Sex, FineAgeCat, Drug DrugBefore_TLE, --Transtioned, 
		--paste aggregate or patient level code here
		count(distinct PatID) Pats_in_month from #DS
			where Transtioned IN (@ReportingMonth, @ReportingMonth2, @ReportingMonth3)
			group by Transtioned, Sex, FineAgeCat, Drug
			--order by Transtioned, Drug, Sex, FineAgeCat
),

UptoThisMonth as (
		select @QP ReportPeriod, @PN Province, @DN District,  
		case @FId
			--Chilanga Aliases
			when '500100051' then 'Kazimva RHC Unknown (1)'
			when '50010005A' then 'Mundengwa'
			when '50010006A' then 'Nakachenje Mini Hospital'
			--Chongwe Aliases
			when '500300GC0' then 'Chongwe District Hospital'
			--Kafue Aliases
			when '500400021' then 'Mungu'
			--when '500400080' then 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
			--when '500400080' then 'Old Kabweza Health Post' -- Duplicate HMIS Code used
			--when '500400251' then 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
			--when '500400251' then 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
			--when '500400251' then 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
			--when '500400260' then 'Shimabala Health Post' -- Duplicate HMIS Code used
			--when '500400260' then 'New Kabweza Health Post' -- Duplicate HMIS Code used
			when '500400261' then 'New Kabweza Health Post'
			-- Lusaka Aliases
			when '500600308' then 'Chainda Market Community Post'
			when '50060035Y' then 'John Howard - Discover Health'
			when '50060035X' then 'Saint Lukes - JSI'
			when '50060036B' then 'Chazanga Health Post'
			when '50060036C' then 'Chazanga Community Post'
			when '50060037X' then 'Chelstone CBTO'
			when '50060037O' then 'Obama Market Community Post'
			when '50060046X' then 'Chimwemwe Health Post'
			when '50060046Y' then 'Garden Site 3 Health Post'
			when '50060046K' then 'Katambalala Community Post'
			when '500600JH3' then 'State House Clinic'
			when '500600JH0' then 'State House Clinic'
			when '50060106B' then 'Desai Health Post'
			when '50060106A' then 'George Community Post Unknown (1)'
			when '50060106X' then 'Jordan Health Post'
			when '50060124A' then 'Lumumba Market Community Post'
			when '500601291' then 'Kabwata Market Community Post'
			when '50060131K' then 'Kalingalinga Market Community Post'
			when '50060135B' then 'Comesa Market Community Post'
			when '50060135A' then 'Lumbama Market  Community Post'
			when '50060136U' then 'Bethel Linda Health Post'
			when '50060136Y' then 'John Laing Health Post'
			when '50060136V' then 'Kanyama Salvation Army Health Post'
			when '500601361' then 'Pamodzi Community Post'
			when '50060137P' then 'Mbasela Health Post'
			when '50060138B' then 'Garden House Health Post'
			when '500601381' then 'Masauko Community Post'
			when '50060140K' then 'Kaunda Square Market Community Post'
			when '50060143A' then 'Chawama Central Market Community Post'
			when '50060180N' then 'Mandevu Community Post'
			when '50060183X' then 'Chunga Health Post - Discover Health'
			when '50060198N' then 'Mtendere Market Community Post'
			when '50060198X' then 'Kalikiliki Health Post - Discover Health'
			when '50060243A' then 'Chachacha Market Community Post'
			when '50060243C' then 'PPAZ'
			when '5006XX96A' then 'Chunga Community Post'
			when '5006XX96B' then 'Twikatane Community Post'
			else @FN 
		end Facility, Sex, FineAgeCat, Drug DrugBefore_TLE, --Transtioned, 
		--paste aggregate or patient level code here
		count(distinct PatID) Pats_in_FY22 
		from #DS
			where Transtioned >= @FYStartMonth
			and Transtioned <= @ReportingMonth
			group by Sex, FineAgeCat, Drug
			--order by Transtioned, Drug, Sex, FineAgeCat
)

select 
	b.*, 
	a.Pats_in_month 
from ThisMonth a
right join UptoThisMonth b on a.Sex = b.Sex and a.FineAgeCat = b.FineAgeCat and a.DrugBefore_TLE = b.DrugBefore_TLE

/*
	--Patient Level
	RI.PatientId NUPN, RI.SurName, RI.FirstName,RI.ArtNumber, 
	D.LastVisitDate Last_TLD_Date, D.DateTransitioned FirstTLE_Date from #DS D
left join crtRegistrationInteraction RI on RI.patientId_int = D.PatID
	where D.LastVisitDate >= D.DateTransitioned
	and Transtioned = @ReportingMonth
	order by D.Drug, D.DateTransitioned
*/

/*
	--Aggregate
	count(distinct PatID) Pats from #DS
	where Transtioned = @ReportingMonth
	group by Transtioned, Sex, FineAgeCat, Drug
	order by Drug, Transtioned, Sex, FineAgeCat */

/*
-- For testing patient level
select PatientId NUPN, SurName, FirstName, Sex, ArtNumber from crtRegistrationInteraction
	where patientid_int in (select distinct PatID from #DS where LastVisitDate >= DateTransitioned)
*/
