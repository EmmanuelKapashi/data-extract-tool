use cdc_fdb_db;
SET ansi_nulls on;
SET quoted_identifier on;

declare   
	@ReportingMonth VARCHAR(6),
	@ReportingStartDate DATETIME,
	@StartDate  DATETIME,
    @EndDate DATETIME,
	@PrevStart DATETIME,
	@PrevEnd DATETIME,
	@BeginDate DATETIME,
	@PId int,
	@DId int,
	@FId varchar(10),
    --@QP nvarchar(10),
	@LTFUThreshold int,
	@PN varchar(100),
	@DN varchar(100),
	@FN varchar(100),
	@FG nvarchar(100),
	@NHC varchar(10),
	@OHC nvarchar(10)
--SET start and end dates
SET @ReportingMonth = '202112';
SET @ReportingStartDate = CONCAT(@ReportingMonth,'01');
SET @BeginDate = DATEADD(YEAR, -2, @ReportingStartDate);
SET @StartDate = DATEADD(MONTH, -11, @ReportingStartDate);
SET @EndDate = EOMONTH(@ReportingStartDate, 0);
SET @EndDate = DATEADD(SECOND, 86399, @EndDate); --adding 23:59:59
SET @PrevStart = DATEADD(month, -1, @StartDate)
SET @PrevEnd = EOMONTH(@PrevStart)
SET @PrevEnd = DATEADD(SECOND, 86399, @PrevEnd) --adding 23:59:59
--SET @QP = CONVERT(nvarchar(6), @StartDate,112)
SET @LTFUThreshold = 30;
SET @FG = (select [Value] from Setting where [Name] = 'FacilityGuid');
SET @PId = (select value from setting where name = 'ProvinceId');
SET @DId = (select value from setting where name = 'DistrictId');
SET @FId = (select value from setting where name = 'HmisCode');

SELECT 
	@PN = p.Name, 
	@DN=d.Name, 
	@FN=f.FacilityName, 
	@NHC=M.NewHMISCode, 
	@OHC=M.OldHMISCode 
FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG

IF(OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId INTO #CurrentFacility FROM dbo.fn_GetFacilitySubSiteIds(@PId, @DId, @NHC);

IF(OBJECT_ID('tempdb..#CbsCohort') IS NOT NULL) DROP TABLE #CbsCohort;
CREATE TABLE #CbsCohort(
	Province VARCHAR(50),
	District VARCHAR(50),
	Facility VARCHAR(100),
	FacilityCode VARCHAR(10),
	PatientIdint INT, 
	NUPN VARCHAR(50),
	PatientGuid VARCHAR(50),
	SurName VARCHAR(50),
	Forenames VARCHAR(100),
	DateOfBirth DATETIME,
	AgeLastVisit INT,
	Sex VARCHAR(1),
	AgeCatFine VARCHAR(50),
	AgeCatCourse VARCHAR(50),
	ArtNumber VARCHAR(50),
	ArtStartDate DATETIME,
	DurationOnART INT,
	DurationOnARTCat VARCHAR(50),
	Pregnant INT,
	BreastFeeding INT,
	TX_CURR_BeginningOfReportingPeriod INT,
	TX_NEW_BeginningOfReportingPeriod INT,
	TX_NEW_InReportingPeriod INT,
	TransIn_InReportingPeriod INT,
	TX_RTT_InReportingPeriod INT,
	TX_ML_InReportingPeriod VARCHAR(50),
	TX_ML_Date DATETIME,
	DaysLateCat VARCHAR(50),
	TX_CURR_EndOfReportingPeriod INT
)

WHILE CONVERT(INT, @ReportingMonth) < CONVERT(INT, CONVERT(VARCHAR(6),GETDATE(), 112)) 
BEGIN

	IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
	IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
	IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
	IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
	IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
	IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
	IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
	IF(object_id('tempdb..#AllCurrPrEPClients') IS NOT NULL) DROP TABLE #AllCurrPrEPClients;
	if(object_id('tempdb..#TX_Curr_Prev') is not null) drop table #TX_Curr_Prev
	if(object_id('tempdb..#TX_New_Prev') is not null) drop table #TX_New_Prev
	if(object_id('tempdb..#TX_New') is not null) drop table #TX_New
	if(object_id('tempdb..#Cohort') is not null) drop table #Cohort
	if(object_id('tempdb..#CohortFin') is not null) drop table #CohortFin
	if(object_id('tempdb..#VIP') is not null) drop table #VIP
	if(object_id('tempdb..#Active') is not null) drop table #Active
	if(object_id('tempdb..#Phones') is not null) drop table #Phones
	if(object_id('tempdb..#DataSet') is not null) drop table #DataSet
	if(object_id('tempdb..#NewOnArt') is not null) drop table #NewOnArt;
	--if(object_id('tempdb..#X') is not null) drop table #X
	--if(object_id('tempdb..#X') is not null) drop table #X
	--if(object_id('tempdb..#X') is not null) drop table #X

    
	CREATE TABLE #Discontinued
	(
		PatientId             INT,
		VisitDate             DATE,
		DiscontinuationReason VARCHAR(32)
	);
	create index DiscontinuedPats on #Discontinued(PatientId)

	CREATE TABLE #DeadClients
	(
		PatientId             INT PRIMARY KEY,
		VisitDate             DATE,
		DiscontinuationReason VARCHAR(32)
	);
	create index DeadPats on #DeadClients(PatientId)


	CREATE TABLE #MostRecentHIVStatusAsOfEndDate
	(
		PatientId  INT PRIMARY KEY,
		TestResult INT,
		TestDate   DATE
	);


	CREATE TABLE #CurrentOnARTByClinicalVisit
	(
		PatientId             INT PRIMARY KEY,
		Sex                   VARCHAR(1),
		InteractionDate       DATE,
		DateOfBirth           DATE,
		AgeAsAtEnd            INT,
		AgeArtStart           INT,
		CurrentHivStatus      INT,
		CurrentHivStatusDate  DATE,
		OldestHivPosTestDate  DATE,
		ArtStartDate          DATE,
		ArtStartLocation      VARCHAR(9),
		ArtStartEditLocation  INT,
		CareStartDate         DATETIME,
		CareStartLocation     VARCHAR(9),
		CareStartEditLocation INT
	);

	CREATE TABLE #CurrentOnARTByDrugs
	(
		PatientId       INT PRIMARY KEY,
		InteractionDate DATE
	);

	CREATE TABLE #CurrentOnARTByVisitAndDrugs
	(
		PatientId                    INT PRIMARY KEY,
		InteractionDate              DATE,
		DeadBeforeEnd                BIT,
		NotActive                    BIT,
		DeadDate                     DATE,
		DiscontinuationDateBeforeEnd DATE,
		CurrentHivStatus             INT,
		DateOfBirth                  DATE,
		CareStartDate                DATE,
		ArtStartDate                 DATE,
		OldestHivPosTestDate         DATE,
		ArtStartEditLocation         INT,
		Sex                          VARCHAR(1),
		AgeAsAtEnd                   INT,
		AgeArtStart                  INT
	);
    
	CREATE TABLE #CurrentOnArt
	(
		PatientId    INT PRIMARY KEY,
		EditLocation INT,
		Age          INT,
		AgeLastVisit INT,
		Sex          VARCHAR(1),
		ArtStartDate DATE
	);
	create index CurrentPats on #CurrentOnArt(PatientId)

	CREATE TABLE #AllCurrPrEPClients
	(
		PatientId         INT PRIMARY KEY,
		Sex               VARCHAR(1),
		Age               INT,
		InitVisitDate     DATE,
		FollowUpVisitDate DATE
	);
	create index PrepPats on #AllCurrPrEPClients (PatientId)
	
	CREATE TABLE #NewOnArt(
		PatientId			INT,
		ArtStartDate		DATETIME,
		AgeAtArtStart		INT,
		Sex					VARCHAR(1),
		ArtStartEditLocation INT,
		IhapDate			DATETIME,
		IhapEditLocation	INT,
		OldestHivPosTestDate DATETIME,
		RoC_Status			VARCHAR(50),
		TX_ML_Date			DATETIME,
		NUPN				VARCHAR(50),
		ArtNumber			VARCHAR(50),
		HtsToArtStartDays	INT,
		QcFlag				INT
	);


	--exec DataTech.dbo.proc_GetTxCurr_Base
	exec DataTech.dbo.proc_GetTxCurr_Base
		@PrevStart,
		@PrevEnd

	EXEC DataTech.dbo.proc_GetTxNew_base
	@PrevStart,
	@PrevEnd;

	select * into #TX_Curr_Prev from #CurrentOnArt

	-- TX New Previous period
	SELECT 
		PatientId, 
		ArtStartDate, 
		AgeAtArtStart AgeArtStart, 
		Sex
	INTO #TX_New_Prev
	FROM #NewOnArt;

	CREATE INDEX Indx_TX_New_Prev_PatientId ON #TX_New_Prev (PatientId);


	IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) truncate TABLE #Discontinued;
	IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) truncate TABLE #DeadClients;
	IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) truncate TABLE #MostRecentHIVStatusAsOfEndDate;
	IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) truncate TABLE #CurrentOnARTByClinicalVisit;
	IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) truncate TABLE #CurrentOnARTByDrugs;
	IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) truncate TABLE #CurrentOnARTByVisitAndDrugs;
	IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) truncate TABLE #CurrentOnArt;
	IF(object_id('tempdb..#AllCurrPrEPClients') IS NOT NULL) truncate TABLE #AllCurrPrEPClients;
	IF(object_id('tempdb..#NewOnArt') IS NOT NULL) truncate TABLE #NewOnArt;

	--exec DataTech.dbo.proc_GetTxCurr_Base
	exec DataTech.dbo.proc_GetTxCurr_Base
		@StartDate,
		@EndDate

	EXEC DataTech.dbo.proc_GetTxNew_base
		@StartDate,
		@EndDate;

	-- TX New current period
	SELECT 
		c.PatientId, 
		c.ArtStartDate, 
		c.AgeAtArtStart AgeArtStart, 
		c.Sex
	INTO #TX_New
	FROM #NewOnArt c; 

	CREATE INDEX Indx_TX_New_PatientId ON #TX_New (PatientId);

	--Discontinued patients includes those made inactive and transfers out
	IF(OBJECT_ID('tempdb..#TX_ML') IS NOT NULL) DROP TABLE #TX_ML
	SELECT PatientId, DATEADD(dd, DATEDIFF(dd, 0, VisitDate), 0) VisitDate, DiscontinuationReason
	INTO #TX_ML FROM (
			SELECT 
				PatientId,
				VisitDate,
				ROW_NUMBER() OVER (PARTITION BY patientid ORDER BY VisitDate DESC) seq,
				CASE
					WHEN ((PatientMadeInactive = 1 and PatientMadeInactiveReason <>2) and ((PatientTransferOut is null or PatientTransferOut = 0) and PatientStoppedART <> 1))
						THEN 'InActive'
					when (PatientMadeInactive = 1 and PatientMadeInactiveReason =2) or PatientStoppedART =1 
						then 'StoppedART'
					WHEN (PatientTransferOut = 1) and (PatientMadeInactive = 0 or PatientMadeInactive is null or PatientStoppedART = 0 or PatientStoppedART is null)
						THEN 'TO'
					WHEN (PatientMadeInactive = 1 OR PatientStoppedART = 1) and PatientTransferOut = 1
						THEN 'InActive/TO'
				END DiscontinuationReason
			FROM crtPatientStatus
				WHERE (PatientMadeInactive = 1 OR PatientTransferOut = 1 or PatientStoppedART =1)
				and PatientDied <>1
				AND VisitDate <= @EndDate
				AND EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
			) g
	WHERE seq = 1;
	CREATE INDEX Indx_Discontinued_PatientId ON #TX_ML (PatientId);


	select distinct PatientId into #VIP from (
		select distinct PatientId from #TX_Curr_Prev
		union
		select distinct PatientId from #CurrentOnArt
		union
		select distinct PatientId from #TX_New_Prev
		union
		select distinct PatientId from #TX_New
	) Pats;

	with ServiceIds as (
	select ServiceCode from ServiceCodes 
		where Deprecated = 0	
		and (ServiceCategoryID = 4 --ART Service category
		and (ServiceName not like '%Case%Surveillance' and ServiceName not like 'Pre-Exposure Prophylaxis%' and ServiceName not like 'Cancer%' 
		and ServiceName not in ('Patient Locator', 'ART ARV Eligibility', 'ART ARV Initiation') and ServiceName not like '%Patient Status' 
		and ServiceName not like 'HIV-TB%' and ServiceName not like '%ARV Initiation' and ServiceName not like '%ARV Eligibility' and ServiceName not like '%Adherence'))
		--include MCH EMTCT visits
	), -- Service codes of ART IHP and Clinical Visit forms

	ClinicalVisits as (
	select distinct  PatientReportingId, CONVERT(date, InteractionDate) VisitDate, CONVERT(date, NextVisitDatm) NextVisitDate from Interaction
		where ServiceCode in (select * from ServiceIds)
	), --All interactions of type in 'ServiceIds'

	CVofInterest as (
	select rank() over(partition by PatientReportingId order by CONVERT(date, VisitDate) desc) Visit_Count, * from ClinicalVisits
	), --Adding a count to Interactions

	MyCVs as (
	select PatientReportingId PID, VisitDate, NextVisitDate, datediff(day, NextVisitDate, @EndDate) DaysLate from CVofInterest where Visit_Count = 1
	), --Latest visit per client

	AllARVPharms as (
	select rank() over(partition by PatientId order by CONVERT(date, VisitDate) desc) VisitCount, * from 
		(select PatientId, CONVERT(date, VisitDate) VisitDate, CONVERT(date, NextAppointmentDate) NextVisitDate from crtPharmacyVisit where ArvDrugDispensed = 1)A
	), --All ART pharmacy dispensations

	Pharms as (
	select *, datediff(day, NextVisitDate, @EndDate) DaysLate  from AllARVPharms where VisitCount = 1
	) --Latest ART Pharmacy Visits per client


	-- Getting latest ART Pharmacy or ART Clinical Visit per client
	select PatientId, max(VisitDate) VisitDate, max(NextVisitDate) AppointmentDate, min(DaysLate) DaysLate into #Active from (
		select PatientId, VisitDate, NextVisitDate, DaysLate from Pharms
		union
		select * from MyCVs
	) X
		group by PatientId;


	--Cohort of Clients being reviewed
	select distinct 
		C.PatientId, R.Sex, R.DateOfBirth, datediff(month, cs.ArtStartDate, @EndDate) DurationOnART, cs.ArtStartDate, 
		case when A.VisitDate is not null then datediff(day, R.DateOfBirth, A.VisitDate)/365.25 else datediff(day, R.DateOfBirth, @EndDate)/365.25 end AgeLastVisit, 
		A.DaysLate, X.DiscontinuationReason, X.VisitDate DiscontinuationDate,
		case when C.PatientId in (select distinct PatientId from #TX_Curr_Prev) then 1 else 0 end TX_Curr_m0, --TX_Curr previous month
		case when C.PatientId in (select distinct PatientId from #TX_New_Prev) then 1 else 0 end TX_New_m0, --TX_New previous month
		case when C.PatientId in (select distinct PatientId from #CurrentOnArt) then 1 else 0 end TX_Curr_m1, --TX_Curr current month
		case when C.PatientId in (select distinct PatientId from #TX_New) then 1 else 0 end TX_New_m1, --TX_New current month
		case when C.PatientId in (select distinct PatientId from crtPatientLocator where PatientTransfer = 2 and VisitDate between @StartDate and @EndDate) then 1 else 0 end TransIn
	into #Cohort 
	from #VIP C
	left join (
		select distinct * from #TX_ML where VisitDate between @PrevStart and @EndDate 
		union 
		select distinct * from #DeadClients where VisitDate between @PrevStart and @EndDate) X on C.PatientId = X.PatientId
	left join #Active A on C.PatientId = A.PatientId
	left join crtRegistrationInteraction R on C.PatientId = R.patientId_int
	left join ClientHivSummary cs on C.PatientId = cs.PatientId



	select *,
	case
		when DurationOnART >= 0 and DurationOnART < 3 then '<3 months'
		when DurationOnART >= 3 and DurationOnART < 6 then '3-5 months'
		when DurationOnART >= 6 and DurationOnART < 12 then '6-11 months'
		when DurationOnART >=12 then '12+ months'
	end DurationOnARTCat,
	case 
		when TX_Curr_m0 = 0 and TX_Curr_m1 = 1 and TX_New_m1 = 0 and TransIn = 0 then 1
		else 0
	end RTT,
	case 
		when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 
			and ((PatientId in (select PatientId from crtPatientStatus where PatientDied = 1 and PatientDiedDate between @StartDate and @EndDate)) 
					or DiscontinuationReason = 'Death') then 1
		else 0 
	end Died,
	case 
		when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason in ('InActive', 'InActive/TO') then 1
		when TX_Curr_m1 = 0 and TX_Curr_m0 = 0 and TX_New_m1 = 1 and DiscontinuationReason in ('InActive', 'InActive/TO') then 1
		--when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason is null then 1
		--when TX_Curr_m1 = 0 and TX_Curr_m0 = 0 and TX_New_m1 = 1 and DiscontinuationReason is null then 1
		else 0
	end MadeInactive,
	case
		when TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason = 'StoppedART' then 1 else 0
	end StoppedART,
	case when  TX_Curr_m1 = 0 and TX_Curr_m0 = 1 and DiscontinuationReason = 'TO' then 1 else 0 end TransOut,
	case when TX_Curr_m0 = 1 and TX_Curr_m1 = 1 
		and datepart(month, DateOfBirth) = datepart(month, @PrevStart) and ((AgeLastVisit >=15.0 and AgeLastVisit <16.0) or (AgeLastVisit>=20.0 and AgeLastVisit <21.0))
		then 1 else 0 end Graduated, 
	case
		when AgeLastVisit <1 then '<01'
		when AgeLastVisit >= 1 and AgeLastVisit < 5 then '''01-04'
		when AgeLastVisit >= 5 and AgeLastVisit < 10 then '''05-09'
		when AgeLastVisit >= 10 and AgeLastVisit < 15 then '''10-14'
		when AgeLastVisit >= 15 and AgeLastVisit < 20 then '15-19'
		when AgeLastVisit >= 20 and AgeLastVisit < 25 then '20-24'
		when AgeLastVisit >= 25 and AgeLastVisit < 30 then '25-29'
		when AgeLastVisit >= 30 and AgeLastVisit < 35 then '30-34'
		when AgeLastVisit >= 35 and AgeLastVisit < 40 then '35-39'
		when AgeLastVisit >= 40 and AgeLastVisit < 45 then '40-44'
		when AgeLastVisit >= 45 and AgeLastVisit < 50 then '45-49'
		when AgeLastVisit >= 50 and AgeLastVisit < 55 then '50-54'
		when AgeLastVisit >= 55 and AgeLastVisit < 60 then '55-59'
		when AgeLastVisit >= 60 and AgeLastVisit < 65 then '60-64'
		when AgeLastVisit >= 65 then '65+'
		else 'Uncategorised'
	end AgeCatFine,
	case
		when AgeLastVisit <15 then 'Pead'
		when AgeLastVisit >= 15 and AgeLastVisit < 20 then 'Adolescent'
		when AgeLastVisit >=20 then 'Adult'
	end AgeCatCourse
	into #CohortFin 
	from #Cohort

	--*************************** TX_RTT cliens that where LTFU for 12 months or more **************************************
	if(object_id('tempdb..#TXRTTCohort') is not null) drop table #TXRTTCohort
	select PatientId into #TXRTTCohort from #CohortFin where RTT = 1 --and DurationOnART >= 12

	if(object_id('tempdb..#TXRTTCohortLTFU12') is not null) drop table #TXRTTCohortLTFU12


	--latest ARV pickups in the period for all clients of interest
	IF (OBJECT_ID('tempdb..#Pharms') IS NOT NULL) DROP TABLE #Pharms
	IF (OBJECT_ID('tempdb..#Clinicals') IS NOT NULL) DROP TABLE #Clinicals

	;with AllClinicals as (
			SELECT c.PatientId, InteractionId, InteractionDate
				from (select *, row_number() over (partition by PatientId order by InteractionDate desc) Row_Count from 
					(select * from ClientClinicalCareDates 
						where ArtOrNonArtClinicalCare = 1
						and EditLocation  in (select distinct * from #CurrentFacility)
						and InteractionDate < @StartDate
						and NextVisitDate < @StartDate
						and VisitType not in (24, 25, 28, 29, 38, 40, 41, 42, 44, 59, 329, 333, 344, 352, 353, 354, 355, 356, 367, 359, 402, 590)) V) as c 
						inner join #TXRTTCohort i ON c.PatientId = i.PatientId
				where Row_Count = 1
	),

	AllPharms as (
			select PatIdPharm, InteractionID, VDate from (
					select PatientId as PatIdPharm, InteractionID, CONVERT(date, VisitDate) VDate, Visit_Count from (
							select *, row_number() over (partition by PatientId order by VisitDate desc) Visit_Count from crtPharmacyVisit
								where ArvDrugDispensed = 1
								and EditLocation in (select distinct  * from #CurrentFacility)
								and VisitDate < @StartDate
								and NextAppointmentDate < @StartDate
					) P
						where Visit_Count = 1
			)P1
			inner join #TXRTTCohort c on P1.PatIdPharm=c.PatientId
			
	),

	AllVisits as (
			select row_number() over(partition by PatientId order by InteractionDate desc) rc, * from (
					select top 0 * from AllClinicals
					union
					select * from AllPharms
			) X
	),

	RTT_LTFU12 as (
			select A.PatientId, A.InteractionDate, A.InteractionId, datediff(day, i.NextVisitDatm, c.VisitDate)*12/365.25 MonthsSinceLTFU 
			from AllVisits A 
			inner join #Active c on A.PatientId = c.PatientId and A.rc = 1
			inner join Interaction i on A.PatientId = i.PatientReportingId and A.InteractionId = i.InteractionID
			
	)

	select *,
	case
		when MonthsSinceLTFU >=0.0 and MonthsSinceLTFU <12.0 then '<12 Months'
		when MonthsSinceLTFU >=12.0 then '>=12 Months'
		else 'Uncategorised'
	end RTT_LTFU_Cat
	into #TXRTTCohortLTFU12 from RTT_LTFU12;

	CREATE INDEX Indx_TXRTT_LTFU_PatientId ON #TXRTTCohortLTFU12 (PatientId);
	
	if(object_id('tempdb..#DataSet') is not null) drop table #DataSet
	SELECT 
		a.PatientId, 
		r.PatientId NUPN,
		g.MappedGuid PatientGuid,
		LTRIM(r.SurName) SurName,
		CASE WHEN r.MiddleName IS NOT NULL OR r.MiddleName != '' THEN CONCAT(r.FirstName, ' ', r.MiddleName) ELSE r.FirstName END Forenames,
		a.DateOfBirth,
		a.AgeLastVisit,
		a.Sex,
		a.AgeCatFine,
		a.AgeCatCourse,
		r.ArtNumber,
		a.ArtStartDate,
		a.DurationOnART,
		a.DurationOnARTCat,
		CASE 
			WHEN (dbo.fnIsPregnantAsOf(a.PatientId, @EndDate)=1) or
					(a.PatientId in (
						select distinct PatientId from crtANCVisit
							where VisitDate<=@EndDate 
							and VisitDate>=DATEADD(day,-270, @EndDate)
						)
					)
					THEN 1 
			ELSE 0
		END Pregnant,
		CASE
			WHEN dbo.fnIsBreastfeedingAsOf(a.PatientId, @EndDate) = 1 then 1
			ELSE 0
		END BreastFeeding,
		a.TX_Curr_m0 TX_CURR_BeginningOfReportingPeriod,
		a.TX_New_m0 TX_NEW_BeginningOfReportingPeriod,
		a.TX_New_m1 TX_NEW_InReportingPeriod,
		a.TransIn TransIn_InReportingPeriod,
		a.RTT TX_RTT_InReportingPeriod,
		CASE
			WHEN (a.Died = 1 OR a.DiscontinuationReason = 'Death') THEN 'Dead'
			WHEN (a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.TransOut = 1 OR a.DiscontinuationReason = 'TO') THEN 'TransOut'
			WHEN (a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.StoppedART = 1 OR a.DiscontinuationReason = 'StoppedART') THEN 'StoppedART'
			WHEN (a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 1 OR a.DiscontinuationReason IN ('InActive', 'InActive/TO')) THEN 'MadeInactive'
			WHEN (a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0
				AND (a.MadeInactive = 0) 
				AND (a.TransOut = 0)
				AND (a.StoppedART = 0)
				AND a.DiscontinuationReason IS NULL
				THEN 'IIT'
		END TX_ML_InReportingPeriod,
		CASE 
			WHEN (a.Died = 1 OR (a.Died = 0 AND a.TX_Curr_m0 = 1 AND a.TX_Curr_m1 = 0 AND a.DiscontinuationReason IS NOT NULL)) THEN a.DiscontinuationDate 
			WHEN (a.Died = 0 AND a.TX_Curr_m0 = 1 AND a.TX_Curr_m1 = 0 AND a.DiscontinuationReason IS NULL) THEN @EndDate 
		END TX_ML_Date, 
		CASE
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 1 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >0 AND a.DaysLate <30) THEN '''01-29'
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >=30 AND a.DaysLate <60) THEN '30-59'
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >=60 AND a.DaysLate <90) THEN '60-89'
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >=90 AND a.DaysLate <120) THEN '90-119'
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >=120 AND a.DaysLate <150) THEN '120-149'
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >=150 AND a.DaysLate <180) THEN '150-179'
			WHEN ((a.Died != 1 OR a.DiscontinuationReason != 'Death') AND a.TX_Curr_m1 = 0 AND (a.MadeInactive = 0) AND (a.TransOut = 0) AND (a.StoppedART = 0) AND a.DiscontinuationReason IS NULL)
				AND (a.DaysLate >=180) THEN '''>=180'
		END DaysLateCat,
		a.TX_Curr_m1 TX_CURR_EndOfReportingPeriod
	INTO #DataSet
	FROM #CohortFin a
	JOIN crtRegistrationInteraction r ON a.PatientId = r.patientId_int
	JOIN GuidMap g ON a.PatientId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid
		ORDER BY SurName, Forenames;

	if(object_id('tempdb..#DataSet2') is not null) drop table #DataSet2
	SELECT DISTINCT
		@PN Province,
		@DN District,
		CASE @FId
			--Chilanga Aliases
			WHEN '50010005A' THEN 'Mundengwa'
			WHEN '50010005T' THEN 'Tubalange Mini Hospital'
			WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
			--Chongwe Aliases
			WHEN '500300GC0' THEN 'Chongwe District Hospital'
			--Kafue Aliases
			WHEN '500400021' THEN 'Mungu'
			WHEN '500400053' THEN 'Lukolongo'
			--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
			WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
			WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025M' THEN 'Mtendere Urban Health Centre' -- Duplicate HMIS Code used
			--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
			WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
			--WHEN '500400261' THEN 'New Kabweza Health Post'
			-- Lusaka Aliases
			WHEN '500600308' THEN 'Chainda Market Community Post'
			WHEN '500600329' THEN 'Chaisa Urban Health Centre'
			WHEN '50060035Y' THEN 'John Howard - Discover Health'
			WHEN '50060035X' THEN 'Saint Lukes - JSI'
			WHEN '50060036B' THEN 'Chazanga Health Post'
			WHEN '50060036C' THEN 'Chazanga Community Post'
			WHEN '50060037X' THEN 'Chelstone CBTO'
			WHEN '50060037O' THEN 'Obama Market Community Post'
			WHEN '50060046X' THEN 'Chimwemwe Health Post'
			WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
			WHEN '50060046K' THEN 'Katambalala Community Post'
			WHEN '500600JH3' THEN 'State House Clinic'
			WHEN '500600JH0' THEN 'State House Clinic'
			WHEN '50060106B' THEN 'Desai Health Post'
			WHEN '50060106C' THEN 'Desai Health Post'
			WHEN '50060106A' THEN 'Desai Health Post'
			WHEN '50060106X' THEN 'Jordan Health Post'
			WHEN '50060124A' THEN 'Lumumba Market Community Post'
			WHEN '500601291' THEN 'Kabwata Market Community Post'
			WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
			WHEN '50060135B' THEN 'Comesa Market Community Post'
			WHEN '50060135A' THEN 'Lumbama Market  Community Post'
			WHEN '50060136U' THEN 'Bethel Linda Health Post'
			WHEN '50060136Y' THEN 'John Laing Health Post'
			WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
			WHEN '500601361' THEN 'Pamodzi Community Post'
			WHEN '50060137P' THEN 'Mbasela Health Post'
			WHEN '50060138B' THEN 'Garden House Health Post'
			WHEN '500601381' THEN 'Masauko Community Post'
			WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
			WHEN '50060143A' THEN 'Chawama Central Market Community Post'
			WHEN '50060180N' THEN 'Mandevu Community Post'
			WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
			WHEN '50060198N' THEN 'Mtendere Market Community Post'
			WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
			WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
			WHEN '50060243A' THEN 'Chachacha Market Community Post'
			WHEN '50060243C' THEN 'PPAZ'
			WHEN '5006XX96A' THEN 'Chunga Community Post'
			WHEN '5006XX96B' THEN 'Twikatane Community Post'
			-- Chirundu Aliases
			-- Luangwa Aliases
			-- Rufunsa Aliases
			WHEN '500700241' THEN 'Lubalashi Health Post'
			ELSE @FN 
		END Facility,
		@FId FacilityCode,
		* 
	INTO #DataSet2
	FROM #DataSet
		WHERE (TX_CURR_BeginningOfReportingPeriod = 1 OR TX_NEW_BeginningOfReportingPeriod = 1 OR TX_NEW_InReportingPeriod = 1 OR TX_RTT_InReportingPeriod = 1 OR TransIn_InReportingPeriod = 1);

	INSERT INTO #CbsCohort
	SELECT DISTINCT *
	FROM #DataSet2 ;
	
	SET @ReportingStartDate = DATEADD(MONTH, 1, @ReportingStartDate)
	SET @ReportingMonth = CONVERT(VARCHAR(6), @ReportingStartDate, 112);
	SET @StartDate = DATEADD(MONTH, -11, @ReportingStartDate);
	SET @EndDate = EOMONTH(@ReportingStartDate, 0);
	SET @EndDate = DATEADD(SECOND, 86399, @EndDate); --adding 23:59:59
	SET @PrevStart = DATEADD(month, -1, @StartDate);
	SET @PrevEnd = EOMONTH(@PrevStart);
	SET @PrevEnd = DATEADD(SECOND, 86399, @PrevEnd); --adding 23:59:59

END

INSERT INTO DataTech.dbo.CbsCohort2
SELECT DISTINCT
	Province,
	District,
	Facility,
	FacilityCode,
	PatientIdInt,
	NUPN,
	PatientGuid,
	DateOfBirth,
	AgeLastVisit,
	Sex,
	AgeCatFine,
	AgeCatCourse,
	ArtNumber,
	ArtStartDate,
	DurationOnART,
	DurationOnARTCat,
	Pregnant,
	BreastFeeding,
	TX_CURR_BeginningOfReportingPeriod,
	TX_CURR_EndOfReportingPeriod
FROM #CbsCohort
	ORDER BY Sex, AgeCatFine;


IF (OBJECT_ID('tempdb..#AllViralLoads') IS NOT NULL) DROP TABLE #AllViralLoads;
IF (OBJECT_ID('tempdb..#AllViralLoads2') IS NOT NULL) DROP TABLE #AllViralLoads2;
SELECT DISTINCT
	g.MappedGuid PatientGuid,
	vl.*,
	chs.ArtStartDate,
	chs.Sex,
	DATEDIFF(MONTH, chs.ArtStartDate, vl.InteractionDate) DurationOnArt
INTO #AllViralLoads
FROM ClientViralLoadTestResult vl
JOIN ClientHivSummary chs ON vl.PatientId = chs.PatientId
JOIN GuidMap g ON vl.PatientId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid
	WHERE vl.InteractionDate BETWEEN @BeginDate AND @EndDate;;

INSERT INTO DataTech.dbo.VLs2020and2021
SELECT * FROM #AllViralLoads;