-- ART Appointment list with
	-- VL Due Date
	-- TPT status and Due date

USE cdc_fdb_db;

	DECLARE   
	    @StartDate DATETIME,
	    @EndDate DATETIME, 
        @ProvinceId VARCHAR(2), 
	    @DistrictId VARCHAR(2), 
	    @FacilityGuid VARCHAR(100), 
	    @hmiscode VARCHAR(10),  
	    @ProvinceName VARCHAR(100), 
	    @DistrictName VARCHAR(100), 
	    @FacilityName VARCHAR(100), 
	    @NewHmisCode VARCHAR(10), 
	    @OldHmisCode VARCHAR(10),
	    @LTFUThreshold INT, 
	    @RC INT,
	    @OnArtMode BIT,
	    @Now DATETIME;

    SET @Now = GETDATE();
    SET @StartDate = DATEADD(MONTH, -1, @Now);
    SET @EndDate = @Now;
    --IF(@EndDate > @Now) SET @EndDate = @Now -- Prevent future date 
    SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
    SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
    SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
    SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
    SET @LTFUThreshold = 30;
    SET @OnArtMode = 1; -- 1 = CURRENT, 0 = EVER 

    SELECT 
        @ProvinceName = p.[Name], 
        @DistrictName = d.[Name], 
        @FacilityName = f.FacilityName, 
        @NewHmisCode = f.HMISCode, 
        @OldHmisCode = m.OldHMISCode 
    FROM Facility f 
    LEFT JOIN District d ON f.DistrictId = d.Code 
    LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
    LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	    WHERE F.FacilityGuid = @FacilityGuid;



--************************* End Custom Declarations *****************************
--************************* Temp Table Declarations *****************************
    IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
        DROP TABLE #CurrentFacility
    SELECT DISTINCT SubSiteId
    INTO #CurrentFacility
    FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

   
    IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
    IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
    IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
    IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
    IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
    IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
    IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
    IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
    
    CREATE TABLE #Discontinued
    (
        PatientId             INT,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #DeadClients
    (
        PatientId             INT PRIMARY KEY,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #MostRecentHIVStatusAsOfEndDate
    (
        PatientId  INT PRIMARY KEY,
        TestResult INT,
        TestDate   DATE
    );
    CREATE TABLE #CurrentOnARTByClinicalVisit
    (
        PatientId             INT PRIMARY KEY,
        Sex                   VARCHAR(1),
        InteractionDate       DATE,
        DateOfBirth           DATE,
        AgeAsAtEnd            INT,
        AgeArtStart           INT,
        CurrentHivStatus      INT,
        CurrentHivStatusDate  DATE,
        OldestHivPosTestDate  DATE,
        ArtStartDate          DATE,
        ArtStartLocation      VARCHAR(9),
        ArtStartEditLocation  INT,
        CareStartDate         DATETIME,
        CareStartLocation     VARCHAR(9),
        CareStartEditLocation INT
    );
    CREATE TABLE #CurrentOnARTByDrugs
    (
        PatientId           INT PRIMARY KEY,
        InteractionDate     DATE
    );
    CREATE TABLE #CurrentOnARTByVisitAndDrugs
    (
        PatientId                    INT PRIMARY KEY,
        InteractionDate              DATE,
        DeadBeforeEnd                BIT,
        NotActive                    BIT,
        DeadDate                     DATE,
        DiscontinuationDateBeforeEnd DATE,
        CurrentHivStatus             INT,
        DateOfBirth                  DATE,
        CareStartDate                DATE,
        ArtStartDate                 DATE,
        OldestHivPosTestDate         DATE,
        ArtStartEditLocation         INT,
        Sex                          VARCHAR(1),
        AgeAsAtEnd                   INT,
        AgeArtStart                  INT
    );
    CREATE TABLE #CurrentOnArt
    (
        PatientId    INT PRIMARY KEY,
    	EditLocation INT,
        Age          INT,
        AgeLastVisit INT,
        Sex          VARCHAR(1),
        ArtStartDate DATE
    );
    CREATE TABLE #AllCurrPrEPClients
    (
        PatientId         INT PRIMARY KEY,
        Sex               VARCHAR(1),
        Age               INT,
        InitVisitDate     DATE,
        FollowUpVisitDate DATE
    );
--************************* End Temp Table Declarations *****************************************
	EXEC DataTech.dbo.proc_GetTxCurr_Base
		@StartDate,
		@EndDate,
		@LTFUThreshold,
		@OnArtMode;

	IF(OBJECT_ID('tempdb..#LastVL') IS NOT NULL) DROP TABLE #LastVL;
	IF(OBJECT_ID('tempdb..#LastArvPharm') IS NOT NULL) DROP TABLE #LastArvPharm;
	IF(OBJECT_ID('tempdb..#LastTbPharm') IS NOT NULL) DROP TABLE #LastTbPharm;
	--IF(OBJECT_ID('tempdb..#X') IS NOT NULL) DROP TABLE #x;
	

	-- Last Viral Load
	SELECT 
		a.PatientId,
		a.MonthsOnARTatLastVL,
		a.InteractionDate VL_OrderDate,
		a.LabTestValue,
		CASE
			WHEN a.MonthsOnARTatLastVL < 3 THEN DATEADD(MONTH, 6, a.ArtStartDate)
			WHEN a.MonthsOnARTatLastVL >= 6 AND a.MonthsOnARTatLastVL < 18 AND a.LabTestValue < 1000.00 THEN DATEADD(MONTH, 6, a.InteractionDate)
			WHEN a.MonthsOnARTatLastVL >= 18 AND a.LabTestValue < 1000.00 THEN DATEADD(MONTH, 12, a.InteractionDate)
			WHEN a.MonthsOnARTatLastVL >= 6 AND a.LabTestValue >= 1000.00 THEN DATEADD(MONTH, 3, a.InteractionDate)
		END VL_DueDate
	INTO #LastVL
	FROM (
		SELECT DISTINCT 
			vl.*,
			c.ArtStartDate,
			DATEDIFF(MONTH, c.ArtStartDate, vl.InteractionDate) MonthsOnARTatLastVL,
			ROW_NUMBER() OVER(PARTITION BY vl.PatientId ORDER BY vl.InteractionDate DESC) seq
		FROM ClientViralLoadTestResult vl
		JOIN #CurrentOnArt c ON vl.PatientId = c.PatientId
	) a
		WHERE a.Seq = 1;

	-- Last ARV Pickup
	SELECT 
		PatientId,
		InteractionId,
		InteractionDate,
		ArvRegimen,
		ArvDurationInDays,
		RefillDate,
		NextArvAppointmentDate
	INTO #LastArvPharm
	FROM (
			SELECT DISTINCT
				adt.PatientId,
				adt.InteractionId,
				adt.InteractionDate,
				REPLACE(adt.Components, ' ', '') ArvRegimen,
				MAX(x.DDS) ArvDurationInDays,
				DATEADD(DAY, MAX(x.DDS), adt.InteractionDate) RefillDate,
				CONVERT(DATE, cad.NextAppointmentDate) NextArvAppointmentDate,
				ROW_NUMBER() OVER(PARTITION BY adt.PatientId ORDER BY adt.InteractionDate DESC) seq
			FROM ClientArvDispensationTransposed adt
			JOIN ClientArvsDate cad ON adt.PatientId = cad.PatientId AND adt.InteractionDate = cad.InteractionDate
			JOIN (
					SELECT DISTINCT 
						DD.InteractionID,
						DD.EditLocation,
						sca.ServiceCategoryName ServiceArea, 
						i.PatientReportingId PatientId, 
						i.InteractionDate VisitDate, 
						i.NextVisitDatm NextAppointmentDate,
						CASE
							WHEN DD.UnitsDispensed IS NULL OR DD.UnitsDispensed <= 0 THEN 0
							WHEN (DD.UnitsDispensed IS NOT NULL AND DD.UnitsDispensed >= 1.00)  
								AND DD.UnitQuantityPerDose IS NOT NULL 
								AND (mf.DosesPerDay IS NOT NULL AND mf.DosesPerDay > 0)
								AND (DD.UnitQuantityPerDose*mf.DosesPerDay) > 0
								THEN FLOOR(DD.UnitsDispensed/(DD.UnitQuantityPerDose*mf.DosesPerDay))
							WHEN (DD.UnitsDispensed IS NOT NULL AND DD.UnitsDispensed >= 1.00)  
								AND (
									(DD.UnitQuantityPerDose IS NULL AND (mf.DosesPerDay IS NULL OR mf.DosesPerDay <= 0))
									OR (DD.UnitQuantityPerDose IS NOT NULL 
										AND (mf.DosesPerDay IS NOT NULL AND mf.DosesPerDay > 0)
										AND (DD.UnitQuantityPerDose*mf.DosesPerDay) <= 0)
								)
								THEN FLOOR(DD.UnitsDispensed)
							WHEN (DD.UnitsDispensed IS NOT NULL AND DD.UnitsDispensed >= 1.00)  
								AND DD.UnitQuantityPerDose IS NULL 
								AND (mf.DosesPerDay IS NOT NULL AND mf.DosesPerDay > 0)
								THEN FLOOR(DD.UnitsDispensed/mf.DosesPerDay)
							WHEN (DD.UnitsDispensed IS NOT NULL AND DD.UnitsDispensed >= 1.00)  
								AND DD.UnitQuantityPerDose IS NOT NULL 
								AND (mf.DosesPerDay IS NULL OR mf.DosesPerDay <= 0)
								THEN FLOOR(DD.UnitsDispensed/DD.UnitQuantityPerDose)	
						END DDS, 
						D.GenericName, 
						CASE D.GenericIngredients
							WHEN 'amprenavir' THEN 'APV'
							WHEN 'enfuvirtide' THEN 'T20'
							WHEN 'Etravirine' THEN 'ETR'
							WHEN 'tipranavir' THEN 'TPV'
							ELSE REPLACE(D.GenericIngredients, ' ', '')
						END Abbr
					FROM crctMedicationDispensingDetails DD
					JOIN Interaction i on DD.InteractionID = i.InteractionID
					JOIN DrugProductsCachedView D on DD.MedDrugId = D.MedDrugId
					JOIN MedFrequency mf ON DD.Frequency = mf.FrequencyID
					JOIN ServiceCodes sco ON i.ServiceCode = sco.ServiceCode
					JOIN ServiceCategory sca ON sco.ServiceCategoryID = sca.ServiceCategoryID
						WHERE i.InteractionDate IS NOT NULL 
						AND DD.UnitsDispensed IS NOT NULL AND DD.UnitsDispensed > 0
						AND D.DrugClass = 'Antiretrovirals'
						AND D.WHOSubClass LIKE 'Antiretroviral - %'	
			) x ON adt.PatientId = x.PatientId AND adt.InteractionId = x.InteractionID
				GROUP BY adt.PatientId, adt.InteractionId, adt.InteractionDate, adt.Components, CONVERT(DATE, cad.NextAppointmentDate)
	) a
		WHERE a.seq = 1;

	-- ************* TPT and ATT **************
	IF(OBJECT_ID('tempdb..#TB_FDCs') IS NOT NULL) DROP TABLE #TB_FDCs;
	IF(OBJECT_ID('tempdb..#Drug_INH') IS NOT NULL) DROP TABLE #Drug_INH;
	IF(OBJECT_ID('tempdb..#TBHistory') IS NOT NULL) DROP TABLE #TBHistory;
	IF(OBJECT_ID('tempdb..#TPTHistory') IS NOT NULL) DROP TABLE #TPTHistory;
	IF(OBJECT_ID('tempdb..#TBSummary') IS NOT NULL) DROP TABLE #TBSummary;
	IF(OBJECT_ID('tempdb..#TPTSummary') IS NOT NULL) DROP TABLE #TPTSummary;
	IF(OBJECT_ID('tempdb..#TB_Dx') IS NOT NULL) DROP TABLE #TB_Dx;

	--Anti-TB
	SELECT DISTINCT
		ep.PatientId,
		ep.InteractionID,
		sc.ServiceName,
		ep.EditLocation,
		ep.InteractionTime,
		pe.OnsetDate,
		pe.ResolvedDate,
		pe.Certainty,
		icpc2.Title [Full Diagnosis],
		icpc2.ShortTitle [Short Diagnosis],
		icd10.Title [Comments]
	INTO #TB_Dx
	FROM crctProblemEpisode pe
	JOIN crtProblemEpisode ep ON pe.InteractionID = ep.InteractionID
	JOIN #CurrentOnArt c ON ep.PatientId = c.PatientId
	JOIN ServiceCodes sc ON ep.ServiceCode = sc.ServiceCode
	LEFT JOIN ICPC2eCodes icpc2 ON pe.ICPCProblem = icpc2.ICPCConceptID
	LEFT JOIN ICD10Lookup icd10 ON pe.ICDProblem = icd10.ICD10ConceptID
	LEFT JOIN Hia1MappingData hia on icpc2.ICPC2eCode = hia.Icpc2Code OR icd10.ICD10ID = hia.Icd10Code
		WHERE COALESCE(pe.OnsetDate, ep.InteractionTime) IS NOT NULL
		AND (icpc2.Title LIKE '%Tuberculosis%' OR  icpc2.Title LIKE '%TB%' OR icd10.Title LIKE '%Tuberculosis%' OR  icd10.Title LIKE '%TB%')
		AND pe.OnsetDate BETWEEN DATEADD(YEAR, -4, @EndDate) AND @EndDate
		ORDER BY ep.PatientId;

	SELECT MedDrugId
	INTO #TB_FDCs
	FROM DrugProductsCachedView
		WHERE (Synonym LIKE '%INH%' OR DrugClass = 'Anti-tuberculosis')
		AND Synonym != 'INH';

	SELECT b.PatientId, b.PatientArtNumber, a.InteractionID, b.VisitDate, CAST(a.UnitsDispensed as INT) UnitsDispensed, 
		DATEADD(DAY, CAST(a.UnitsDispensed as INT), b.VisitDate) NextRefillDate 
	INTO #TBHistory 
	FROM crctMedicationDispensingDetails a
	LEFT JOIN crtPharmacyVisit b ON a.InteractionID = b.InteractionID
		WHERE a.MedDrugId IN (SELECT DISTINCT MedDrugId FROM #TB_FDCs)
		AND b.PatientId IN (SELECT DISTINCT PatientId FROM #TB_Dx)
		ORDER BY b.PatientId, b.VisitDate;

	SELECT a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate, 
		MIN(b.VisitDate) Earliest_TBDx_Date, 
		MAX(b.NextRefillDate) NextRefillDate,
		SUM(b.UnitsDispensed) ATT_Tabs_Collected
	INTO #TBSummary
	FROM ClientHivSummary a
	INNER JOIN #TBHistory b ON a.PatientId = b.PatientId
		WHERE b.VisitDate <= @EndDate
		AND b.NextRefillDate >= DATEADD(YEAR, -3, @StartDate)
		GROUP BY a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate;
	
	-- TPT
	SELECT MedDrugId 
	INTO #Drug_INH 
	FROM DrugProductsCachedView
		WHERE GenericName = 'Isoniazid' OR Synonym = 'INH' OR GenericName = 'Rifapentine' OR Synonym = 'RPT';


	SELECT b.PatientId, b.PatientArtNumber, a.InteractionID, b.VisitDate, CAST(a.UnitsDispensed as INT) UnitsDispensed, 
		DATEADD(DAY, CAST(a.UnitsDispensed as INT), b.VisitDate) NextRefillDate 
	INTO #TPTHistory 
	FROM crctMedicationDispensingDetails a
	LEFT JOIN crtPharmacyVisit b ON a.InteractionID = b.InteractionID
		WHERE a.MedDrugId IN (SELECT DISTINCT MedDrugId FROM #Drug_INH)
		AND b.PatientId IN (SELECT DISTINCT PatientId FROM #CurrentOnArt)
		ORDER BY b.PatientId, b.VisitDate;



	SELECT a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate, 
		MIN(b.VisitDate) Earliest_TPT_Date, 
		MAX(b.NextRefillDate) NextRefillDate,
		SUM(b.UnitsDispensed) INH_Tabs_Collected
	INTO #TPTSummary 
	FROM ClientHivSummary a
	INNER JOIN #TPTHistory b ON a.PatientId = b.PatientId
		WHERE b.VisitDate <= @EndDate
		AND b.NextRefillDate >= DATEADD(YEAR, -3, @StartDate)
		GROUP BY a.PatientId, a.ArtStartDate, a.AgeOnArtStart, a.DateOfBirth, a.PatientDiedDate;

	--**************************************** Current Address ********************************************
	IF(OBJECT_ID('tempdb..#Address') IS NOT NULL) DROP TABLE #Address;
	SELECT DISTINCT * INTO #Address FROM (
		SELECT ROW_NUMBER() OVER(PARTITION BY PatientGUID ORDER BY CreatedTime DESC) N, NaturalNumber PatientID, A.PatientGUID,
			CASE
				WHEN 
					(PhoneContact = '' OR PhoneContact IS NULL) 
					AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
					AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
					THEN NULL
				WHEN 
					(PhoneContact != '' AND PhoneContact IS NOT NULL) 
					AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
					AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
					THEN REPLACE(PhoneContact, ',', '')
				WHEN 
					(PhoneContact = '' OR PhoneContact IS NULL) 
					AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
					AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
					THEN REPLACE(PhoneNumber, ',', '')
				WHEN 
					(PhoneContact = '' OR PhoneContact IS NULL) 
					AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
					AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
					THEN REPLACE(MobilePhoneNumber, ',', '')
				WHEN 
					(PhoneContact != '' AND PhoneContact IS NOT NULL) 
					AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
					AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
					THEN (REPLACE(PhoneContact, ',', '') + ' | ' + REPLACE(PhoneNumber, ',', '') + ' | ' + REPLACE(MobilePhoneNumber, ',', ''))
				WHEN 
					(PhoneContact = '' OR PhoneContact IS NULL) 
					AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
					AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
					THEN (REPLACE(PhoneNumber, ',', '') + ' | ' + REPLACE(MobilePhoneNumber, ',', ''))
				WHEN 
					(PhoneContact != '' AND PhoneContact IS NOT NULL) 
					AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
					AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
					THEN (REPLACE(PhoneContact, ',', '') + ' | ' + REPLACE(MobilePhoneNumber, ',', ''))
				WHEN 
					(PhoneContact != '' AND PhoneContact IS NOT NULL) 
					AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
					AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
					THEN (REPLACE(PhoneContact, ',', '') + ' | ' + REPLACE(PhoneNumber, ',', ''))
			END PNumber 
		FROM [Address] A JOIN guidmap g ON a.PatientGUID = g.MappedGuid
			WHERE AddressType = 'Current'
	) Ads
		WHERE N = 1;


	SELECT DISTINCT 
		@ProvinceName Province,
		@DistrictName District,
		CASE @hmiscode
			--Chilanga Aliases
			--WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
			WHEN '50010005A' THEN 'Mundengwa'
			WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
			--Chongwe Aliases
			WHEN '500300GC0' THEN 'Chongwe District Hospital'
			--Kafue Aliases
			WHEN '500400021' THEN 'Mungu'
			WHEN '500400053' THEN 'Lukolongo'
			--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
			WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
			WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025M' THEN 'Mtendere' -- Duplicate HMIS Code used
			--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
			WHEN '50040026K' THEN 'Kabweza Health Post' -- Duplicate HMIS Code used
			--WHEN '500400261' THEN 'New Kabweza Health Post'
			-- Lusaka Aliases
			WHEN '500600308' THEN 'Chainda Market Community Post'
			WHEN '500600329' THEN 'Chaisa Urban Health Centre'
			WHEN '50060035Y' THEN 'John Howard - Discover Health'
			WHEN '50060035X' THEN 'Saint Lukes - JSI'
			WHEN '50060036B' THEN 'Chazanga Health Post'
			WHEN '50060036C' THEN 'Chazanga Community Post'
			WHEN '50060037X' THEN 'Chelstone CBTO'
			WHEN '50060037O' THEN 'Obama Market Community Post'
			WHEN '50060046X' THEN 'Chimwemwe Health Post'
			WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
			WHEN '50060046K' THEN 'Katambalala Community Post'
			WHEN '500600JH3' THEN 'State House Clinic'
			WHEN '500600JH0' THEN 'State House Clinic'
			WHEN '50060106B' THEN 'Desai Health Post'
			WHEN '50060106C' THEN 'Desai Market Community Post'
			WHEN '50060106A' THEN 'George Community Post'
			WHEN '50060106X' THEN 'Jordan Health Post'
			WHEN '50060124A' THEN 'Lumumba Market Community Post'
			WHEN '500601291' THEN 'Kabwata Market Community Post'
			WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
			WHEN '50060135B' THEN 'Comesa Market Community Post'
			WHEN '50060135A' THEN 'Lumbama Market  Community Post'
			WHEN '50060136U' THEN 'Bethel Linda Health Post'
			WHEN '50060136Y' THEN 'John Laing Health Post'
			WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
			WHEN '500601361' THEN 'Pamodzi Community Post'
			WHEN '50060137P' THEN 'Mbasela Health Post'
			WHEN '50060138B' THEN 'Garden House Health Post'
			WHEN '500601381' THEN 'Masauko Community Post'
			WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
			WHEN '50060143A' THEN 'Chawama Central Market Community Post'
			WHEN '50060180N' THEN 'Mandevu Community Post'
			WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
			WHEN '50060198N' THEN 'Mtendere Market Community Post'
			WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
			WHEN '50060211N' THEN 'Ng'+'''ombe Market Community Post'
			WHEN '50060243A' THEN 'Chachacha Market Community Post'
			WHEN '50060243C' THEN 'PPAZ'
			WHEN '5006XX96A' THEN 'Chunga Community Post'
			WHEN '5006XX96B' THEN 'Twikatane Community Post'
			ELSE @FacilityName
		END FacilityName,
		@hmiscode FacilityId,
		r.PatientId NUPN,
		r.SurName,
		r.FirstName,
		r.MiddleName,
		c.Sex,
		CONVERT(DATE, r.DateOfBirth) DateOfBirth,
		r.ArtNumber,
		c.ArtStartDate,
		arv.InteractionDate LastArvPickup,
		DataTech.dbo.fn_RemoveDrugDuplicates(arv.ArvRegimen) ArvRegimen,
		arv.ArvDurationInDays,
		arv.RefillDate,
		CASE 
			WHEN (arv.RefillDate <= CONVERT(DATE, @EndDate) OR arv.RefillDate IS NULL) THEN CONVERT(VARCHAR(6), @StartDate, 112)
			WHEN arv.RefillDate > @EndDate THEN CONVERT(VARCHAR(6), arv.RefillDate, 112)
		END MonthDueForPharmPickup,
		arv.NextArvAppointmentDate,
		vl.VL_OrderDate,
		vl.LabTestValue LastVLResult,
		CASE 
			WHEN vl.VL_DueDate IS NULL THEN DATEADD(MONTH, 6, c.ArtStartDate)
			ELSE vl.VL_DueDate
		END VL_DueDate,
		CASE
			WHEN b.Earliest_TPT_Date BETWEEN @StartDate AND @EndDate THEN 'New on TPT'
			WHEN tb.Earliest_TBDx_Date BETWEEN @StartDate AND @EndDate THEN 'New on ATT'
			WHEN b.Earliest_TPT_Date < @StartDate AND b.NextRefillDate >= @StartDate THEN 'Current on TPT'
			WHEN tb.Earliest_TBDx_Date < @StartDate AND tb.NextRefillDate >= @StartDate THEN 'Current on ATT'
			WHEN (b.Earliest_TPT_Date < @StartDate AND b.NextRefillDate < @StartDate AND DATEDIFF(MONTH, b.NextRefillDate, @StartDate) >= 36) 
				AND (tb.Earliest_TBDx_Date < @StartDate AND tb.NextRefillDate < @StartDate AND DATEDIFF(MONTH, tb.NextRefillDate, @StartDate) >= 36)
				THEN 'Due for TPT'
			WHEN (b.Earliest_TPT_Date < @StartDate AND b.NextRefillDate < @StartDate AND DATEDIFF(MONTH, b.NextRefillDate, @StartDate) < 36)  
				THEN 'Previously on TPT (Covered)'
			WHEN (tb.Earliest_TBDx_Date < @StartDate AND tb.NextRefillDate < @StartDate AND DATEDIFF(MONTH, tb.NextRefillDate, @StartDate) < 36)
				THEN 'Previously on ATT (Covered)'
			WHEN (b.Earliest_TPT_Date IS NULL AND tb.Earliest_TBDx_Date IS NULL) THEN 'Never on TPT or ATT'
		END TPT_Status, 
		COALESCE(b.Earliest_TPT_Date, tb.Earliest_TBDx_Date) [TPT or ATT Start Date],
		b.INH_Tabs_Collected, 
		COALESCE(b.NextRefillDate, tb.NextRefillDate) NextTPTRefillDate,
		ad.PNumber	PhoneContacts
	FROM #CurrentOnArt c
	JOIN crtRegistrationInteraction r ON c.PatientId = r.patientId_int
	LEFT JOIN #LastVL vl ON c.PatientId = vl.PatientId
	LEFT JOIN #LastArvPharm arv ON c.PatientId = arv.PatientId
	LEFT JOIN #TPTSummary b ON c.PatientId = b.PatientId
	LEFT JOIN #TBSummary tb ON c.PatientId = tb.PatientId
	LEFT JOIN #Address ad ON c.PatientId = ad.PatientID;