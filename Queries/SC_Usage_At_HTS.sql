USE cdc_fdb_db;

DECLARE 
	@StartDate DATETIME,
	@EndDate DATETIME,
	@ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME;

SET @StartDate = '20191001';
SET @EndDate = GETDATE();

SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
SET @LTFUThreshold = 30;
SET @OnArtMode = 1; -- 1 = CURRENT, 0 = EVER 
--SELECT @StartDate StartDate, @EndDate EndDate;
--SELECT @StartDate2 StartDate, @EndDate2 EndDate;

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;

IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility; -- Renamed FROM facility filter
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

IF (OBJECT_ID('tempdb..#HtsRaw') IS NOT NULL) DROP TABLE #HtsRaw; -- Renamed FROM facility filter
WITH AllHivTests AS (
		SELECT DISTINCT 
			*,
			ROW_NUMBER() OVER(PARTITION BY PatientIdInt, CONVERT(DATE, VisitDate) ORDER BY EntryPoint DESC) Seq
		FROM DataTech.dbo.[GetEmpiricalHivTestResults](@EndDate)
			WHERE VisitDate >= @StartDate
			AND EditLocation IN (SELECT DISTINCT SubSiteId FROM #CurrentFacility)
	)
	
SELECT 
	@ProvinceName Province,
	@DistrictName District,
	@FacilityName Facility,
	@hmiscode HMISCode,
	PatientIdInt,
	CONVERT(DATE, VisitDate) TestDate,
	CONVERT(VARCHAR(6), VisitDate, 112) TestMonth,
	CASE TestResults
		WHEN -1 THEN 'Indeterminate / No Result'
		WHEN 0 THEN 'Known Positive'
		WHEN 1 THEN 'Positive'
		WHEN 2 THEN 'Negative'
	END HivResult,
	EntryPoint,
	EditLocation
INTO #HtsRaw
FROM AllHivTests a
	WHERE Seq = 1;

SELECT 
	Province,
	District,
	Facility,
	HMISCode,
	TestMonth,
	EntryPoint,
	HivResult,
	COUNT(PatientIdInt)	PatientsTested
FROM #HtsRaw
	GROUP BY Province, District, Facility, HMISCode, TestMonth, EntryPoint, HivResult
	ORDER BY TestMonth, EntryPoint;