use [cdc_fdb_db];
set ansi_nulls off;

 declare   
	@StartDate  DATETIME,
    @EndDate    DATETIME,
	@FId varchar(10),
    @QP nvarchar(10),
	@TH nvarchar(10),
	@Threshold int,
	@PN varchar(100),
	@DN varchar(100),
	@FN varchar(100),
	@FG nvarchar(100),
	@NHC varchar(10),
	@OHC nvarchar(10)
--set start and end dates
set @StartDate = '20210401'
set @EndDate = eomonth(@StartDate, 0)
SET @EndDate = DATEADD(SECOND, 86399, @EndDate) --adding 23:59:59
set @QP = convert(nvarchar(6), @StartDate,112)
set @Threshold = 30
set @TH = convert(varchar, @Threshold)+' Days'
set @FG = (select [Value] from Setting where [Name] = 'FacilityGuid')
set @FId = (select value from setting where name = 'HmisCode')

SELECT TOP 1 @PN = p.Name, @DN=d.Name, @FN=f.FacilityName, @NHC=M.NewHMISCode, @OHC=M.OldHMISCode FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG

--/* 
--Addresses 
if(object_id('tempdb..#CurrentAddress') is not null) drop table #CurrentAddress
select distinct * into #CurrentAddress from (
		select rank() over(partition by PatientGUID order by CreatedTime desc) N, NaturalNumber PatientID, A.PatientGUID,
			case
				when PhoneContact = '' and PhoneNumber = '' and MobilePhoneNumber = '' then null
				when PhoneContact != '' and PhoneNumber = '' and MobilePhoneNumber = '' then PhoneContact
				when PhoneContact = '' and PhoneNumber != '' and MobilePhoneNumber is null then PhoneNumber
				when PhoneContact = '' and PhoneNumber = '' and MobilePhoneNumber != '' then MobilePhoneNumber
				when PhoneContact != '' and PhoneNumber != '' and MobilePhoneNumber != '' then concat(PhoneContact, ' | ', PhoneNumber, ' | ', MobilePhoneNumber)
				when PhoneContact = '' and PhoneNumber != '' and MobilePhoneNumber != '' then concat(PhoneNumber, ' | ', MobilePhoneNumber)
				when PhoneContact != '' and PhoneNumber = '' and MobilePhoneNumber != '' then concat(PhoneContact, ' | ', MobilePhoneNumber)
				when PhoneContact != '' and PhoneNumber != '' and MobilePhoneNumber = '' then concat(PhoneContact, ' | ', PhoneNumber)
			end PNumber from [Address] A join guidmap g on a.PatientGUID = g.MappedGuid
			where AddressType = 'Current'
	) Ads
		where N = 1
--*/

/* All client registrations */
if(object_id('tempdb..#AllPts') is not null) drop table #AllPts
select distinct R.PatientGUID, RI.patientId_int PID, concat(RI.SurName, ', ', RI.FirstName) ClientName, A.PNumber PhoneNumber, ArtNumber, RI.PatientId NUPN into #AllPts from Registration R 
left join crtRegistrationInteraction RI on RI.PatientId = R.PatientID
left join #CurrentAddress A on R.PatientGUID = A.PatientGUID and A.PatientID = RI.patientId_int
	
create index AllPts_Index on #AllPts(PID)

/* Last pharmacy pickup */
if(object_id('tempdb..#LastPharm') is not null) drop table #LastPharm
select distinct PatientId, PatientArtNumber, InteractionId, VisitDate, NextAppointmentDate into #LastPharm from (
	select rank() over(partition by PatientId order by VisitDate desc) VCount, * from crtPharmacyVisit
		where ArvDrugDispensed = 1
		and VisitDate between @StartDate and @EndDate
) Z
	where VCount = 1

/* Last ART clinical visit */
if(object_id('tempdb..#LastClinical') is not null) drop table #LastClinical
select convert(int, PatientGuid) PatientId, InteractionID, ServiceCode, InteractionDate, NextVisitDatm into #LastClinical from (
	select rank() over(partition by PatientGuid order by InteractionDate desc) VC, * from Interaction
		where ServiceCode in (select distinct ServiceCode from ServiceCodes where ServiceCategoryID = 4 and Status = 'Active')
		and ServiceCode not in (329, 333, 352, 353, 356, 357, 359)
		and InteractionDate between @StartDate and @EndDate
) Z
	where VC = 1

if(object_id('tempdb..#Dataset') is not null) drop table #Dataset
select 
	@PN Province, 
	@DN District, 
	case @FId 
		--Chilanga Aliases
		WHEN '500100051' THEN 'Kazimva RHC Unknown (1)'
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Market Community Post'
		WHEN '50060106A' THEN 'George Community Post Unknown (1)'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		else @FN 
	end Facility, 
	@FId FacilityCode, 
	A.ClientName, 
	A.PhoneNumber, 
	A.ArtNumber, 
	A.NUPN,
	P.VisitDate LastDateofPharmacyPickup, 
	C.InteractionDate LastDateofAnySCInteraction, 
	datediff(day, P.VisitDate, C.InteractionDate) DurationBetweenLastPharmacyAndLastInteraction,
	case
		when datediff(day, P.VisitDate, C.InteractionDate) > 0 then 'Yes'
		when datediff(day, P.VisitDate, P.NextAppointmentDate) = 0 then 'Yes'
		else 'No'
	end Investigate
into #Dataset 
from #AllPts A
join #LastPharm P on A.PID = P.PatientId
left join #LastClinical C on C.PatientId = A.PID

select distinct * from #Dataset
	where LastDateofPharmacyPickup is not null
	and Investigate in ('Yes', 'No')
	and LastDateofPharmacyPickup between @StartDate and @EndDate