USE cdc_fdb_db;
SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;

	DECLARE   
	    @AggregationLevel VARCHAR(32),
		@FyStartDate DATETIME,
		@Period VARCHAR(20),
		@StartDate DATETIME,
	    @EndDate DATETIME, 
        @ProvinceId VARCHAR(2), 
	    @DistrictId VARCHAR(2), 
	    @FacilityGuid VARCHAR(100), 
	    @hmiscode VARCHAR(10),  
	    @ProvinceName VARCHAR(100), 
	    @DistrictName VARCHAR(100), 
	    @FacilityName VARCHAR(100), 
	    @NewHmisCode VARCHAR(10), 
	    @OldHmisCode VARCHAR(10),
	    @LTFUThreshold INT, 
	    @RC INT,
	    @OnArtMode BIT,
	    @Now DATETIME;

    SET @Now = GETDATE();
    SET @StartDate = '20211201';
    --SET @EndDate = EOMONTH(@StartDate, 2);
	SET @EndDate = DataTech.dbo.fn_GetLastDayOfMonth(@StartDate);
    IF(@EndDate > @Now) SET @EndDate = @Now; -- Prevent future date 
    SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
    SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
    SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
    SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
    SET @LTFUThreshold = 30;
    SET @OnArtMode = 1; -- 1 = CURRENT, 0 = EVER 
    SET @AggregationLevel = (SELECT [Value] FROM cdc_fdb_db.dbo.Setting WHERE [Name] = 'AggregationLevel');
	SET @FyStartDate = (
			CASE 
				WHEN DATEPART(m,@StartDate) in (10,11,12) THEN CONVERT(DATETIME, (DATENAME(YEAR, @StartDate) + '1001'))
				ELSE CONVERT(DATETIME, (DATENAME(YEAR, DATEADD(YEAR, -1, @StartDate)) + '1001')) 
			END); 

    SET @Period = (
		CASE DATEDIFF(MONTH, @StartDate, @EndDate) 
			WHEN 0 THEN convert(varchar(6), @StartDate,112) 
			WHEN 2 THEN 
				CASE
					WHEN  DATEPART(MONTH, @StartDate) IN (10, 11, 12) THEN CONCAT('FY', RIGHT((DATENAME(YEAR, @StartDate) +1), 2), 'Q1')
					WHEN  DATEPART(MONTH, @StartDate) NOT IN (10, 11, 12) THEN CONCAT('FY', RIGHT(DATENAME(YEAR, @StartDate), 2), 'Q', (DATEPART(QUARTER, @StartDate)+1))
				END
			WHEN 11 THEN CONCAT('12M ', CONVERT(VARCHAR(6), @EndDate, 112))
			WHEN 12 THEN CONCAT('12M ', CONVERT(VARCHAR(6), @EndDate, 112))
			ELSE 'UNDEFINED'
		END
	);

	SELECT 
        @ProvinceName = p.[Name], 
        @DistrictName = d.[Name], 
        @FacilityName = f.FacilityName, 
        @NewHmisCode = f.HMISCode, 
        @OldHmisCode = m.OldHMISCode 
    FROM Facility f 
    LEFT JOIN District d ON f.DistrictId = d.Code 
    LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
    LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	    WHERE F.FacilityGuid = @FacilityGuid;


    IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility;
    SELECT DISTINCT SubSiteId
    INTO #CurrentFacility
    FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

   
    IF(OBJECT_ID('tempdb..#Regimen') IS NOT NULL) DROP TABLE #Regimen;
	IF(OBJECT_ID('tempdb..#LastArvPharmacy') IS NOT NULL) DROP TABLE #LastArvPharmacy;
    IF(OBJECT_ID('tempdb..#AllEverPrEPClients') IS NOT NULL)DROP TABLE #AllEverPrEPClients;
	IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
    IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
    IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
    IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
    IF(OBJECT_ID('tempdb..#AllHivTestResults') IS NOT NULL) DROP TABLE #AllHivTestResults;
    IF(OBJECT_ID('tempdb..#EarliestHivPosResult') IS NOT NULL) DROP TABLE #EarliestHivPosResult;
    IF(OBJECT_ID('tempdb..#LatestHivNegResult') IS NOT NULL) DROP TABLE #LatestHivNegResult;
    IF(OBJECT_ID('tempdb..#ArtClinical') IS NOT NULL) DROP TABLE #ArtClinical;
    IF(OBJECT_ID('tempdb..#InitArtClinical') IS NOT NULL) DROP TABLE #InitArtClinical;
    
    CREATE TABLE #Discontinued
    (
        PatientId             INT,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #DeadClients
    (
        PatientId             INT PRIMARY KEY,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #MostRecentHIVStatusAsOfEndDate
    (
        PatientId  INT PRIMARY KEY,
        TestResult INT,
        TestDate   DATE
    );
    CREATE TABLE #AllCurrPrEPClients
    (
        PatientId         INT PRIMARY KEY,
        Sex               VARCHAR(1),
        Age               INT,
        InitVisitDate     DATE,
        FollowUpVisitDate DATE,
		LastHivTest		  VARCHAR(5),
		LastHivTestDate   DATE,
		ArvRegimen		  VARCHAR(100),
		Duration		  INT,
        NextVisitDate	  DATE,
        RefillDate		  DATE
    );
	CREATE TABLE #AllEverPrEPClients
    (
        PatientId         INT PRIMARY KEY,
        Sex               VARCHAR(1),
        Age               INT,
        InitVisitDate     DATE,
        FollowUpVisitDate DATE,
		LastHivTest		  VARCHAR(5),
		LastHivTestDate   DATE,
		ArvRegimen		  VARCHAR(100),
		Duration		  INT,
        NextVisitDate	  DATE,
        RefillDate		  DATE
    );
	CREATE TABLE #Regimen(
		Interaction_Id	INT NOT NULL,-- PRIMARY KEY,
		PatId			INT NOT NULL,
		VisitDate		DATETIME NOT NULL,
		NextVisitDate	DATETIME,
		DailyDrugStock	INT,
		ArvRegimen		VARCHAR(100),
		EditLocation	INT,
		ServiceArea		VARCHAR(100),
		ArtPasserby		INT,
		PepDispensation	INT
        );

    EXEC DataTech.dbo.[proc_ArvRegimenDatesAndQuanitity];

	
    SELECT DISTINCT 
		a.PatId PatientId,
		a.Interaction_Id InteractionId,
        a.VisitDate InteractionDate,
		a.ArvRegimen,
		a.DailyDrugStock Duration,
        a.NextVisitDate,
        a.RefillDate
	INTO #LastArvPharmacy
	FROM (
			SELECT DISTINCT 
				ROW_NUMBER() OVER(PARTITION BY PatId ORDER BY VisitDate DESC) Seq,
				*,
                CASE 
					WHEN DailyDrugStock < 1000 THEN DATEADD(DAY, DailyDrugStock, VisitDate) 
					ELSE DATEADD(DAY, 1000, VisitDate)
				END RefillDate -- Giving a maximum allowance of 1000 days of drugs to avoid overflow errors in case of ridiculous dispensations
			FROM #Regimen
				WHERE (
					(@OnArtMode = 1 
                    AND ((VisitDate >= DATEADD(DAY, -@LTFUThreshold, @EndDate) AND VisitDate <= @EndDate) -- Visit within 30 days of reporting end
							OR (VisitDate < DATEADD(DAY, -@LTFUThreshold, @EndDate) 
								AND (CASE WHEN DailyDrugStock < 1000 THEN DATEADD(DAY, DailyDrugStock, VisitDate) ELSE DATEADD(DAY, 1000, VisitDate) END)>= DATEADD(DAY, -@LTFUThreshold, @EndDate)) 
							)-- or drug refill date within 30 days of report end or after
                    )
					OR (@OnArtMode = 0 AND VisitDate <= @EndDate)
				)
				AND (
					(@AggregationLevel = 'Facility' AND ((ArtPasserby = 0 OR ArtPasserby IS NULL) AND (PepDispensation = 0 OR PepDispensation IS NULL))) -- Dispensations marked ArtPasserBy and PepDispensation removed at Facility Level
					OR (@AggregationLevel <> 'Facility' AND PepDispensation <> 1) -- Dispensations marked PepDispensation removed at National, Provincial or District Level
				)
				AND (ArvRegimen LIKE 'T[AD]F' OR ArvRegimen LIKE '[3F]TC+T[AD]F' OR ArvRegimen LIKE 'T[AD]F+[3F]TC') --Check actual PrEP dispensation
	) a
		WHERE a.Seq = 1;

--@PC: HIV Lab data in period
	IF(OBJECT_ID('tempdb..#HIVLabs') IS NOT NULL) DROP TABLE #HIVLabs
	SELECT 
		i.PatientReportingId PatId,
		a.InteractionID, 
		a.EditLocation, 
		CASE
            WHEN a.LabOrderDate IS NULL THEN i.InteractionDate 
            ELSE a.LabOrderDate END LabOrderDate, 
		a.LabTestID, 
		c.AbbrvSyn, 
		b.LabTestValue, 
		c.Units 
	INTO #HIVLabs
	FROM cdc_fdb_db.dbo.crctLaboratoryOrderDetails a
	JOIN cdc_fdb_db.dbo.crctLaboratoryResultDetails b ON a.InteractionID = b.InteractionID AND a.LaboratoryOrderID = b.LaboratoryOrderID
	LEFT JOIN cdc_fdb_db.dbo.Interaction i ON a.InteractionID = i.InteractionID AND ServiceCode = 402 -- 402: Lab orders and results
	LEFT JOIN cdc_fdb_db.dbo.LabTestsDictionary c ON a.LabTestID = c.LabTestID
		WHERE a.LabTestID IN (102, 103, 104, 105, 106, 107, 108) --CD4, CD4-TLC%, CD8, CD3, HIV Genotype and VL
		AND a.LabOrderDate BETWEEN @StartDate AND @EndDate
		AND a.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
		ORDER BY PatId, LabOrderDate DESC;

	
	SELECT
	DENSE_RANK() OVER(PARTITION BY a.PatientId ORDER BY a.InteractionDate ASC) ASC_RANK,
	DENSE_RANK() OVER(PARTITION BY a.PatientId ORDER BY a.InteractionDate DESC) DESC_RANK,
	*
	INTO #ArtClinical
	FROM (
			SELECT DISTINCT
				cccd.*,
				scod.ServiceName
			FROM ClientClinicalCareDates cccd
			JOIN ServiceCodes scod ON cccd.VisitType = scod.ServiceCode
				WHERE (((scod.ServiceCategoryID = 4) AND scod.ServiceCode NOT IN (29, 33, 333, 329, 359, 352, 353, 354, 355, 356, 357))--ART visits only, No PrEP
					OR (cccd.InteractionId IN (SELECT DISTINCT InteractionID FROM #HIVLabs))
					OR (cccd.InteractionId IN (SELECT DISTINCT anc.InteractionID FROM cdc_fdb_db.dbo.crtANCVisit anc WHERE anc.PMTCTKnownStatus = 1 OR anc.HIVTestResult = 3 OR anc.HIVTestResultV5 = 0)) --ANC Positive
					) -- @PC: Only ART Visits, ANC EMTCT and HIV Labs
				AND scod.Deprecated = 0
				AND cccd.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
				--AND cccd.InteractionDate <= @EndDate
	) a;
	
	SELECT DISTINCT *
	INTO #InitArtClinical
	FROM #ArtClinical
		WHERE ASC_RANK = 1;
    

    -- Step 1: All recent HIV tests (latest entry per client, sorted by result (1 pos, 2 neg))
    -- This function already returns one unique result per client :)
    
    SELECT PatientId, TestDate, TestResult 
    INTO #AllHivTestResults
    FROM cdc_fdb_db.dbo.[fn_GetAllHivTestResults_RevisedTxCurr](DEFAULT)
        WHERE TestDate <= @EndDate;

    SELECT PatientId, TestDate, TestResult 
    INTO #EarliestHivPosResult
    FROM (
            SELECT PatientId, TestDate, TestResult, ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY TestDate ASC) seq 
            FROM #AllHivTestResults
                WHERE TestResult = 1
    ) ehpr
        WHERE ehpr.seq = 1;

    SELECT PatientId, TestDate, TestResult 
    INTO #LatestHivNegResult
    FROM (
            SELECT PatientId, TestDate, TestResult, ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY TestDate DESC) seq 
            FROM #AllHivTestResults
                WHERE TestResult = 2
                AND PatientId NOT IN (SELECT DISTINCT PatientId FROM #EarliestHivPosResult)
    ) lhnr
        WHERE lhnr.seq = 1

    INSERT INTO #MostRecentHIVStatusAsOfEndDate (PatientId, TestResult, TestDate)
	SELECT PatientId, TestResult, TestDate
    FROM #EarliestHivPosResult
    UNION
    SELECT PatientId, TestResult, TestDate
    FROM #LatestHivNegResult
    
    
    DELETE
    FROM #MostRecentHIVStatusAsOfEndDate
    WHERE PatientId NOT IN (
                SELECT DISTINCT PatientId
                FROM cdc_fdb_db.dbo.ClientClinicalCareDates
                    WHERE EditLocation IN (SELECT SubSiteId FROM #CurrentFacility));
    -- This client returns ALL clients in a dataset! Thus we need to filter to clients who were seen at least ONCE at this location (think of national level run)

	-- Identify the dead
    INSERT INTO #DeadClients(PatientId, VisitDate, DiscontinuationReason)
    SELECT dd.PatientId, dd.VisitDate, 'Death' AS DiscontinuationReason
    FROM cdc_fdb_db.dbo.fn_GetPatientDeadDateAsOf(@EndDate, default) dd;
    -- This function already returns one unique result per client :)

	/*
	INSERT INTO #Discontinued(PatientId, VisitDate, DiscontinuationReason)
    SELECT 
        PatientId, 
        DATEADD(dd, DATEDIFF(dd, 0, VisitDate), 0) VisitDate, 
        DiscontinuationReason
    FROM (
            SELECT 
                PatientId,
                VisitDate,
                ROW_NUMBER() OVER (PARTITION BY patientid ORDER BY VisitDate DESC) seq,
                CASE
                    WHEN PatientMadeInactive = 1 
                        AND (PatientTransferOut IS NULL OR PatientTransferOut = 0) 
                        AND (PatientStoppedART IS NULL OR PatientStoppedART = 0)
                        THEN 'InActive'
                    WHEN PatientTransferOut = 1
                        AND (PatientStoppedART IS NULL OR PatientStoppedART = 0)
                        AND (PatientMadeInactive IS NULL or PatientMadeInactive = 0)
                        THEN 'TO'
                    WHEN PatientMadeInactive = 1 AND PatientTransferOut = 1
                        AND (PatientStoppedART IS NULL OR PatientStoppedART = 0)
                        THEN 'InActive/TO'
                    WHEN PatientStoppedART = 1
                        AND (PatientTransferOut IS NULL OR PatientTransferOut = 0) 
                        AND (PatientMadeInactive IS NULL OR PatientMadeInactive = 0) --Note: Add logic for other reason made inactive is refused ART
                        THEN 'Stopped ART' --@PC: Include other discontinuation reasons
                END DiscontinuationReason 
            FROM cdc_fdb_db.dbo.crtPatientStatus
                WHERE ((PatientMadeInactive = 1 OR PatientTransferOut = 1 or PatientStoppedART = 1) and PatientDied != 1)
                AND PatientId NOT IN (SELECT PatientId FROM #DeadClients)
                AND DATEADD(dd, DATEDIFF(dd, 0, VisitDate), 0) <= @EndDate
                AND (EditLocation IN (SELECT SubSiteId FROM #CurrentFacility))
             -- implied reactivation block rectifies this at national level (subsequent visits at other locations)
         ) g
        WHERE seq = 1;
	
    DELETE
    FROM d
    FROM #Discontinued d
    -- JOIN ClientClinicalCareDates c ON d.PatientId = c.PatientId --@PC: Streamline to relevant visits -- to add HIV labs
    LEFT JOIN #LastArvPharmacy c ON d.PatientId = c.PatientId --@PC: Streamlined to ART visits only 
    WHERE d.DiscontinuationReason != 'Death'
        -- and c.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility) --ALREADY FILTERED ABOVE
        AND c.InteractionDate <= @EndDate
        AND d.VisitDate < c.InteractionDate;
    -- < not <= (yes factor time, e.g Final Followup before being trans-out same day)
	*/

	-- Insert those who started PrEP in the period
    INSERT INTO #AllEverPrEPClients(PatientId)
    SELECT DISTINCT initial.patient_guid
    FROM cdc_fdb_db.dbo.Art45_PrEPInitialInteraction1 initial
        WHERE initial.overview_interaction_time BETWEEN @StartDate AND @EndDate
        AND edit_location IN (SELECT SubSiteId FROM #CurrentFacility);
        --AND patient_guid NOT IN (SELECT PatientId FROM #MostRecentHIVStatusAsOfEndDate WHERE TestResult = 1); --@PC: Clients that were initially on ARVs for PrEP that sero-convert still consider PrEP.

    -- Insert those still on PrEP in the period (follow-ups)
    INSERT INTO #AllEverPrEPClients(PatientId)
    SELECT DISTINCT followUp.patient_guid
    FROM cdc_fdb_db.dbo.Art45_PrEPFollowUpInteraction1 followUp
        WHERE followUp.patient_guid NOT IN (SELECT PatientId FROM #AllEverPrEPClients)
        AND followUp.overview_interaction_time <= @EndDate
        AND edit_location IN (SELECT SubSiteId FROM #CurrentFacility)
        --AND patient_guid NOT IN (SELECT PatientId FROM #MostRecentHIVStatusAsOfEndDate WHERE TestResult = 1)
        --AND patient_guid NOT IN (SELECT PatientId FROM cdc_fdb_db.dbo.crtIHAP WHERE VisitDate <= @EndDate)
    UNION
    SELECT DISTINCT pii.patient_guid 
    FROM cdc_fdb_db.dbo.Art45_PrEPInitialInteraction1 pii
        WHERE pii.overview_interaction_time < @StartDate
        AND pii.patient_guid NOT IN (
            SELECT DISTINCT patient_guid FROM cdc_fdb_db.dbo.Art45_PrEPFollowUpInteraction1 
                WHERE overview_interaction_time <= @EndDate
                AND edit_location IN (SELECT SubSiteId FROM #CurrentFacility)
            ) --@PC: Including PrEP clients with init before period and no follow-up visits
        --AND patient_guid not in (SELECT PatientId FROM #MostRecentHIVStatusAsOfEndDate WHERE TestResult = 1)
        --AND patient_guid not in (SELECT PatientId FROM cdc_fdb_db.dbo.crtIHAP WHERE VisitDate <= @EndDate);
    -- then demographic data
    UPDATE c
    SET c.Sex = chs.Sex,
        c.Age = convert(varchar, DATEDIFF(YEAR, chs.DateOfBirth, @EndDate))
    FROM #AllEverPrEPClients c
    JOIN cdc_fdb_db.dbo.ClientHivSummary chs ON c.PatientId = chs.PatientId;

    -- Initial visit date
    UPDATE c
    SET InitVisitDate = z.overview_interaction_time
    FROM #AllEverPrEPClients c
    JOIN (
            SELECT patient_guid, MIN(overview_interaction_time) overview_interaction_time
            FROM cdc_fdb_db.dbo.Art45_PrEPInitialInteraction1
                WHERE patient_guid IN (SELECT PatientId FROM #AllEverPrEPClients) 
                    AND (eligibility_plan_prescription_start_pr_ep = 1
							OR investigations_and_referrals_pr_epd_ispensed_one_month = 1
							OR (drug_orders_1_med_order_text_map IS NOT NULL AND drug_orders_1_is_regimen = 1)
							OR (patient_guid IN (
										SELECT DISTINCT PatientId 
										FROM #LastArvPharmacy 
											WHERE InteractionDate >= overview_interaction_time
											
									)
								) --Check actual dispensation
                    )
                GROUP BY patient_guid
    ) z ON c.PatientId = z.patient_guid;

    -- FollowUp properties
    UPDATE c
    SET FollowUpVisitDate = overview_interaction_time
    FROM #AllEverPrEPClients c
             JOIN (SELECT patient_guid, MAX(overview_interaction_time) overview_interaction_time
                   FROM  (
						SELECT patient_guid, overview_interaction_time
						FROM cdc_fdb_db.dbo.Art45_PrEPFollowUpInteraction1
							WHERE patient_guid IN (SELECT PatientId FROM #AllEverPrEPClients)
							AND overview_interaction_time <= @EndDate
						UNION
						SELECT PatientId, InteractionDate 
						FROM #LastArvPharmacy 
							WHERE PatientId IN (SELECT PatientId FROM #AllEverPrEPClients)
				) x
					GROUP BY X.patient_guid
			) z ON c.PatientId = z.patient_guid;

	UPDATE c
	SET c.ArvRegimen = rx.ArvRegimen,
		c.Duration = rx.Duration,
		c.NextVisitDate = COALESCE(rx.NextVisitDate, pv.NextAppointmentDate),
		c.RefillDate = rx.RefillDate,
		c.LastHivTest = (CASE WHEN hts.TestResult = 1 THEN 'POS' WHEN hts.TestResult = 2 THEN 'NEG' ELSE NULL END),
		c.LastHivTestDate = hts.TestDate
	FROM #AllEverPrEPClients c
	JOIN #LastArvPharmacy rx ON c.PatientId = rx.PatientId
	LEFT JOIN (
			SELECT patient_guid PatientID, MAX(overview_next_visit_date) NextAppointmentDate
			FROM (
					SELECT patient_guid, overview_next_visit_date FROM cdc_fdb_db.dbo.Art45_PrEPInitialInteraction1
					UNION
					SELECT patient_guid, overview_next_visit_date FROM cdc_fdb_db.dbo.Art45_PrEPFollowUpInteraction1
			) x
				GROUP BY patient_guid
		) pv ON c.PatientId = pv.PatientID
	LEFT JOIN #MostRecentHIVStatusAsOfEndDate hts ON c.PatientId = hts.PatientId;


    INSERT INTO #AllCurrPrEPClients
	SELECT * FROM #AllEverPrEPClients
		WHERE (
			InitVisitDate BETWEEN @StartDate AND @EndDate OR 
			FollowUpVisitDate BETWEEN @StartDate AND @EndDate OR 
			RefillDate >= @EndDate
			);  
	
	WITH PrEP AS (
			SELECT 
				'PrEP_NEW' Indicator,
				* 
			FROM #AllCurrPrEPClients
				WHERE InitVisitDate BETWEEN @StartDate AND @EndDate
			UNION	
			SELECT 
				'PrEP_CT' Indicator,
				* 
			FROM #AllCurrPrEPClients
				WHERE InitVisitDate < @StartDate
				AND (FollowUpVisitDate BETWEEN @StartDate AND @EndDate) 
			UNION	
			SELECT 
				'PrEP_CT Previous Period' Indicator,
				* 
			FROM #AllCurrPrEPClients
				WHERE InitVisitDate < @StartDate 
				AND (FollowUpVisitDate < @StartDate OR FollowUpVisitDate IS NULL)
				AND (NextVisitDate >= @StartDate AND RefillDate >= @StartDate) 
			UNION	
			SELECT 
				'Late' Indicator,
				* 
			FROM #AllEverPrEPClients
				WHERE PatientId NOT IN (SELECT DISTINCT PatientId FROM #AllCurrPrEPClients)
				AND PatientId NOT IN (SELECT DISTINCT PatientId FROM #DeadClients)
				AND InitVisitDate < @EndDate 
				AND (FollowUpVisitDate < @StartDate OR FollowUpVisitDate IS NULL)
				AND (NextVisitDate < @StartDate OR RefillDate < @StartDate) 
				AND (NextVisitDate >= DATEADD(YEAR, -1, @StartDate) OR RefillDate >= DATEADD(YEAR, -1, @StartDate))
			UNION	
			SELECT 
				'Dead' Indicator,
				* 
			FROM #AllEverPrEPClients
				WHERE PatientId NOT IN (SELECT DISTINCT PatientId FROM #AllCurrPrEPClients)
				AND PatientId IN (SELECT DISTINCT PatientId FROM #DeadClients)	
	)

    SELECT DISTINCT
		@Period [Period],
		@ProvinceName Province, 
		@DistrictName District, 
		CASE @hmiscode
			--Chilanga Aliases
			WHEN '50010005A' THEN 'Mundengwa'
			WHEN '50010005T' THEN 'Tubalange Mini Hospital'
			WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
			--Chongwe Aliases
			WHEN '500300GC0' THEN 'Chongwe District Hospital'
			--Kafue Aliases
			WHEN '500400021' THEN 'Mungu'
			WHEN '500400053' THEN 'Lukolongo'
			--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
			WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
			WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
			WHEN '50040025M' THEN 'Mtendere (Kafue) Urban Health Centre' -- Duplicate HMIS Code used
			--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
			WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
			--WHEN '500400261' THEN 'New Kabweza Health Post'
			-- Lusaka Aliases
			WHEN '500600308' THEN 'Chainda Market Community Post'
			WHEN '500600329' THEN 'Chaisa Urban Health Centre'
			WHEN '50060035Y' THEN 'John Howard - Discover Health'
			WHEN '50060035X' THEN 'Saint Lukes - JSI'
			WHEN '50060036B' THEN 'Chazanga Health Post'
			WHEN '50060036C' THEN 'Chazanga Community Post'
			WHEN '50060037X' THEN 'Chelstone CBTO'
			WHEN '50060037O' THEN 'Obama Market Community Post'
			WHEN '50060046X' THEN 'Chimwemwe Health Post'
			WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
			WHEN '50060046K' THEN 'Katambalala Community Post'
			WHEN '500600JH3' THEN 'State House Clinic'
			WHEN '500600JH0' THEN 'State House Clinic'
			WHEN '50060106B' THEN 'Desai Health Post'
			WHEN '50060106C' THEN 'Desai Health Post'
			WHEN '50060106A' THEN 'Desai Health Post'
			WHEN '50060106X' THEN 'Jordan Health Post'
			WHEN '50060124A' THEN 'Lumumba Market Community Post'
			WHEN '500601291' THEN 'Kabwata Market Community Post'
			WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
			WHEN '50060135B' THEN 'Comesa Market Community Post'
			WHEN '50060135A' THEN 'Lumbama Market  Community Post'
			WHEN '50060136U' THEN 'Bethel Linda Health Post'
			WHEN '50060136Y' THEN 'John Laing Health Post'
			WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
			WHEN '500601361' THEN 'Pamodzi Community Post'
			WHEN '50060137P' THEN 'Mbasela Health Post'
			WHEN '50060138B' THEN 'Garden House Health Post'
			WHEN '500601381' THEN 'Masauko Community Post'
			WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
			WHEN '50060143A' THEN 'Chawama Central Market Community Post'
			WHEN '50060180N' THEN 'Mandevu Community Post'
			WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
			WHEN '50060198N' THEN 'Mtendere Market Community Post'
			WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
			WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
			WHEN '50060243A' THEN 'Chachacha Market Community Post'
			WHEN '50060243C' THEN 'PPAZ'
			WHEN '5006XX96A' THEN 'Chunga Community Post'
			WHEN '5006XX96B' THEN 'Twikatane Community Post'
			-- Chirundu Aliases
			-- Luangwa Aliases
			-- Rufunsa Aliases
			WHEN '500700241' THEN 'Lubalashi Health Post'
			ELSE @FacilityName
		END Facility, 
		@hmiscode FacilityCode,
		CASE
		WHEN Age >= 0 AND Age < 1 THEN '<01'
		WHEN Age >= 1 AND Age < 5 THEN '''01-04'
		WHEN Age >= 5 AND Age < 10 THEN '''05-09'
		WHEN Age >= 10 AND Age < 15 THEN '''10-14'
		WHEN Age >= 15 AND Age < 20 THEN '15-19'
		WHEN Age >= 20 AND Age < 25 THEN '20-24'
		WHEN Age >= 25 AND Age < 30 THEN '25-29'
		WHEN Age >= 30 AND Age < 35 THEN '30-34'
		WHEN Age >= 35 AND Age < 40 THEN '35-39'
		WHEN Age >= 40 AND Age < 45 THEN '40-44'
		WHEN Age >= 45 AND Age < 50 THEN '45-49'
		WHEN Age >= 50 AND Age < 55 THEN '50-54'
		WHEN Age >= 55 AND Age < 60 THEN '55-59'
		WHEN Age >= 60 AND Age < 65 THEN '60-64'
		WHEN Age >= 65 THEN '65+'
		ELSE 'Uncategorised'
	END AgeGroup,
		*
	FROM PrEP
		ORDER BY Indicator, FollowUpVisitDate DESC;