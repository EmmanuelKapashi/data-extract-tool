USE cdc_fdb_db;

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
SET NOCOUNT ON;

DECLARE   
	@BeginDate DATETIME,
	@BeginDate2 DATETIME,
	@StartDate DATETIME,
	@EndDate DATETIME, 
    @StartDate2 DATETIME,
	@EndDate2 DATETIME,
	@ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME;

SET @Now = GETDATE();

SET @StartDate2 = '20211201';
SET @EndDate2 = EOMONTH(@StartDate2, 0);
SET @EndDate2 = DATEADD(SECOND, 86399, @EndDate2); --adding 23:59:59
IF(@EndDate2 > @Now) SET @EndDate2 = @Now; -- Prevent future date

SET @StartDate = DATEADD(YEAR, -1, @StartDate2);
SET @EndDate = EOMONTH(@StartDate, 0);
SET @EndDate = DATEADD(SECOND, 86399, @EndDate); --adding 23:59:59

SET @BeginDate = DATEADD(MONTH, -11, @StartDate);
SET @BeginDate2 = DATEADD(MONTH, -11, @StartDate2);
 
SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
SET @LTFUThreshold = 30;
SET @OnArtMode = 1; -- 1 = CURRENT, 0 = EVER 
--SELECT @StartDate StartDate, @EndDate EndDate;
--SELECT @StartDate2 StartDate, @EndDate2 EndDate;

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;

--From facilities, get AND filter only the current facility in the current province AND district
IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility -- Renamed FROM facility filter
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

DECLARE
	@SubSiteIds SubSiteTableType;
INSERT INTO @SubSiteIds
SELECT DISTINCT SubSiteId
FROM #CurrentFacility;

IF (OBJECT_ID('tempdb..#Raw') IS NOT NULL) DROP TABLE #Raw;
IF (OBJECT_ID('tempdb..#Refined') IS NOT NULL) DROP TABLE #Refined;

WITH MyServiceCodes AS (
		SELECT DISTINCT 
			ServiceCode,
			ServiceName
		FROM ServiceCodes sc
			WHERE ServiceCategoryID = 3
			AND LocaleName = 'ZAMBIA'
			AND Status = 'Active'
	)
	
SELECT DISTINCT 
	sc.ServiceName,
	i.PatientReportingId PatientIdInt,
	g.MappedGuid PatientGuid,
	i.InteractionID,
	i.InteractionDate,
	CONVERT(VARCHAR(6), i.InteractionDate, 112) InteractionMonth
INTO #Raw
FROM Interaction i
JOIN MyServiceCodes sc ON i.ServiceCode = sc.ServiceCode
JOIN GuidMap g ON i.PatientReportingId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid
	WHERE i.EditLocation IN (SELECT DISTINCT SubSiteId FROM #CurrentFacility)
	AND i.InteractionDate BETWEEN @BeginDate2 AND @Now;

SELECT 
	ServiceName,
	InteractionMonth,
	COUNT(DISTINCT PatientIdInt) DistinctPatients,
	COUNT(DISTINCT InteractionID) DistinctVisits
INTO #Refined
FROM #Raw
	GROUP BY ServiceName, InteractionMonth
	ORDER BY ServiceName;

SELECT 
	@ProvinceName Province,
	@DistrictName District,
	@FacilityName Facility,
	@hmiscode HMISCode,
	* 
FROM #Refined;

