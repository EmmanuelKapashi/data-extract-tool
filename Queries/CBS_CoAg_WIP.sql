USE cdc_fdb_db;

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
SET NOCOUNT ON;

DECLARE   
	@StartDate DATETIME,
	@EndDate DATETIME, 
    @ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME;

SET @Now = GETDATE();
SET @StartDate = '20201201';
SET @EndDate = EOMONTH(@StartDate, 12);
SET @EndDate = DATEADD(SECOND, 86399, @EndDate); --adding 23:59:59
IF(@EndDate > @Now) SET @EndDate = @Now -- Prevent future date 
SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
SET @LTFUThreshold = 30;
SET @OnArtMode = 0; -- 1 = CURRENT, 0 = EVER 
--SELECT @StartDate StartDate, @EndDate EndDate

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;

--From facilities, get AND filter only the current facility in the current province AND district
IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility -- Renamed FROM facility filter
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

DECLARE
	@SubSiteIds SubSiteTableType;
INSERT INTO @SubSiteIds
SELECT DISTINCT SubSiteId
FROM #CurrentFacility;

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
    
CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);
CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);
CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId           INT PRIMARY KEY,
    InteractionDate     DATE
);
CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);

EXEC DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@LTFUThreshold,
	@OnArtMode;

IF (OBJECT_ID('tempdb..#EverOnArt') IS NOT NULL) DROP TABLE #EverOnArt;
SELECT * 
INTO #EverOnArt
FROM #CurrentOnArt;

SET @OnArtMode = 1;
IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) TRUNCATE TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) TRUNCATE TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) TRUNCATE TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) TRUNCATE TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)TRUNCATE TABLE #AllCurrPrEPClients;

EXEC DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@LTFUThreshold,
	@OnArtMode;



IF (OBJECT_ID('tempdb..#TX_ClientsOfInterest') IS NOT NULL) DROP TABLE #TX_ClientsOfInterest;
SELECT DISTINCT
	'' Demographic,
	g.MappedGuid PatientGuid,
	ch.PatientId, 
	ch.Sex,
	ch.ArtStartDate,
	cri.ArtNumber, 
	dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) Age,
	CASE
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 0 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 15 THEN '<15'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 15 THEN '15+'
		ELSE 'Uncategorised'
	END AgeGroupChildOrAdult,
	CASE
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 0 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 10 THEN '0-9'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 10 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 15 THEN '''10-14'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 15 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 25 THEN '15-24'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 25 THEN '25+'
		ELSE 'Uncategorised'
	END AgeGroupFinerChildOrAdult, 
	CASE
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 0 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 1 THEN '<01'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 1 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 5 THEN '''01-04'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 5 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 10 THEN '''05-09'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 10 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 15 THEN '''10-14'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 15 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 20 THEN '15-19'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 20 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 25 THEN '20-24'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 25 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 30 THEN '25-29'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 30 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 35 THEN '30-34'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 35 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 40 THEN '35-39'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 40 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 45 THEN '40-44'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 45 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 50 THEN '45-49'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 50 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 55 THEN '50-54'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 55 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 60 THEN '55-59'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 60 AND dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) < 65 THEN '60-64'
		WHEN dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) >= 65 THEN '65+'
		ELSE 'Uncategorised'
	END AgeGroup,
	cri.DateOfDeath,
	cri.EducationLevel,
	cri.Occupation,
	cri.Married,

	'' Testing,
	hts.VisitDate,
	hts.TestResults,
	hts.EntryPoint,
	
	'' Treatment,
	CASE
		WHEN ch.ArtStartEditLocation IN (SELECT SubSiteId FROM #CurrentFacility) 
			AND ch.ArtStartDate >= @StartDate 
			AND (loc.PatientTransfer = 1 OR loc.PatientTransfer IS NULL) 
			THEN 1
		ELSE 0
	END IsTxNew,
	CASE
		WHEN chs.PatientId IS NOT NULL THEN 1
		ELSE 0
	END IsTxCurr,
	ch.OldestHivPosTestDate,
	ch.IhapDate,
	ch.IhapEditLocation,
	
	'' [Viral Suppression],
	CASE
		WHEN (
			(cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](ch.PatientId, DATEADD(DAY, 90, ch.ArtStartDate)) = 1)
			OR (cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](ch.PatientId, DATEADD(DAY, 180, ch.ArtStartDate)) = 1)
			OR (cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](ch.PatientId, DATEADD(DAY, 210, ch.ArtStartDate)) = 1)
			OR (bf.PatientId IS NOT NULL AND bf.VisitDate BETWEEN DATEADD(DAY, 90, ch.ArtStartDate) AND DATEADD(DAY, 180, ch.ArtStartDate))
			--Add check from PostNatal visit
			OR (pnbf.IsBF = 1 AND pnbf.VisitDate BETWEEN DATEADD(DAY, 90, ch.ArtStartDate) AND DATEADD(DAY, 180, ch.ArtStartDate))
			) THEN 1
		ELSE 0
	END IsBreastFeedingAtSixMonthOnArt,
	CASE
		WHEN (
			(cdc_fdb_db.dbo.fnIsPregnantAsOf(ch.PatientId, ch.ArtStartDate) = 1)
			OR (cdc_fdb_db.dbo.fnIsPregnantAsOf(ch.PatientId, DATEADD(DAY, 90, ch.ArtStartDate)) = 1)
			OR (cdc_fdb_db.dbo.fnIsPregnantAsOf(ch.PatientId, DATEADD(DAY, 180, ch.ArtStartDate)) = 1)
			OR (cdc_fdb_db.dbo.fnIsPregnantAsOf(ch.PatientId, DATEADD(DAY, 210, ch.ArtStartDate)) = 1)
			OR (anc.patient_guid IS NOT NULL)
			) THEN 1
		ELSE 0
	END IsPregnantAtSixMonthOnArt,
	NULL IsEligibleForVlMonitoring
INTO #TX_ClientsOfInterest
FROM ClientHivSummary ch 
JOIN GuidMap g ON ch.PatientId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid
JOIN crtRegistrationInteraction cri ON cri.patientid_int = ch.PatientId
JOIN #EverOnArt art ON ch.PatientId = art.PatientId
LEFT JOIN #CurrentOnArt chs ON ch.PatientId = chs.PatientId
LEFT JOIN crtPatientLocator loc ON chs.PatientId = loc.PatientId
		AND loc.EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
		AND loc.VisitDate >= @StartDate AND loc.VisitDate <= @EndDate
LEFT JOIN (
		SELECT 
			PatientIdInt, EditLocation, VisitDate, MAX(EntryPoint) EntryPoint, TestResults
		FROM DataTech.dbo.GetEmpiricalHivTestResults(@EndDate)
			WHERE TestResults = 1
			AND EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
			AND VisitDate >= @StartDate
			GROUP BY PatientIdInt, EditLocation, VisitDate, TestResults
	) hts ON ch.PatientId = hts.PatientIdInt
LEFT JOIN (
		SELECT DISTINCT patient_guid
		FROM cdc_fdb_db.dbo.ANCV5InitialVisitInteraction1
			WHERE overview_interaction_time <= @EndDate
			AND (first_visit_data_edd >= @EndDate OR DATEADD(DAY, 270, first_visit_data_lmp) >= @EndDate)
	
	) anc ON ch.PatientId = anc.patient_guid
LEFT JOIN (
		SELECT * FROM cdc_fdb_db.dbo.fn_MotherBreastfeedingStatus_new(@StartDate, @EndDate, @SubSiteIds)
	) bf ON ch.PatientId = bf.PatientId AND bf.BreastFeeding = 'Yes'
LEFT JOIN (
	SELECT 
		PatientIdInt,
		InteractionId,
		EditLocation,
		VisitDate,
		IsBF,
		ROW_NUMBER() OVER(PARTITION BY PatientIdInt ORDER BY VisitDate DESC) Seq
	FROM (
			SELECT 
				patient_guid PatientIdInt,
				interaction_id InteractionId,
				edit_location EditLocation,
				overview_interaction_time VisitDate,
				1 IsBF
			FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
				WHERE (
					mother_details_feeding_option IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					OR mother_details_feeding_practice IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					)
			UNION
			SELECT 
				patient_guid PatientIdInt,
				interaction_id InteractionId,
				edit_location EditLocation,
				overview_interaction_time VisitDate,
				0 IsBF
			FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
				WHERE (
					mother_details_feeding_option NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					AND mother_details_feeding_practice NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					)
					OR mother_details_feeding_option IS NULL
					OR mother_details_feeding_practice IS NULL
		) bf
	) pnbf ON ch.PatientId = pnbf.PatientIdInt AND pnbf.VisitDate BETWEEN @StartDate AND @EndDate
	WHERE ch.ArtStartEditLocation >= 1 
	AND ch.ArtStartDate IS NOT NULL
	AND ch.ArtStartDate <= @EndDate;

CREATE INDEX Indx_TX_ClientsOfInterest_PatientId
    ON #TX_ClientsOfInterest (PatientId);

CREATE INDEX Indx_TX_ClientsOfInterest_Sex
    ON #TX_ClientsOfInterest (Sex);

CREATE INDEX Indx_TX_ClientsOfInterest_AgeArtStart
    ON #TX_ClientsOfInterest (Age);

UPDATE x
SET x.IsEligibleForVlMonitoring = (CASE
		WHEN 
			((Sex = 'M' OR (Sex = 'F' AND IsBreastFeedingAtSixMonthOnArt = 0 AND IsPregnantAtSixMonthOnArt = 0)) AND DATEADD(DAY, 180, ArtStartDate) <= @EndDate) -- General ART rule: On ARVs >= 6 months 
			OR (Sex = 'F' AND (IsBreastFeedingAtSixMonthOnArt = 1 OR IsPregnantAtSixMonthOnArt = 1) AND DATEADD(DAY, 90, ArtStartDate) <= @EndDate) -- EMTCT rule: PBFW on ARVs for >= 3 Months
			THEN 1
		ELSE 0
	END)
FROM #TX_ClientsOfInterest x

IF (OBJECT_ID('tempdb..#VL_Eligibility') IS NOT NULL) DROP TABLE #VL_Eligibility;
IF (OBJECT_ID('tempdb..#AllViralLoads') IS NOT NULL) DROP TABLE #AllViralLoads;
IF (OBJECT_ID('tempdb..#FirstViralLoad') IS NOT NULL) DROP TABLE #FirstViralLoad;
IF (OBJECT_ID('tempdb..#LastViralLoad') IS NOT NULL) DROP TABLE #LastViralLoad;
IF (OBJECT_ID('tempdb..#ViralLoadAt3M') IS NOT NULL) DROP TABLE #ViralLoadAt3M;
IF (OBJECT_ID('tempdb..#ViralLoadAt6M') IS NOT NULL) DROP TABLE #ViralLoadAt6M;
IF (OBJECT_ID('tempdb..#ViralLoadAt12M') IS NOT NULL) DROP TABLE #ViralLoadAt12M;
IF (OBJECT_ID('tempdb..#ViralLoadAt24M') IS NOT NULL) DROP TABLE #ViralLoadAt24M;
IF (OBJECT_ID('tempdb..#ViralLoadAt36M') IS NOT NULL) DROP TABLE #ViralLoadAt36M;



SELECT DISTINCT
	vl.*,
	c.ArtStartDate,
	c.Sex,
	DATEDIFF(MONTH, c.ArtStartDate, vl.InteractionDate) DurationOnArt,
	ROW_NUMBER() OVER(PARTITION BY vl.PatientId ORDER BY vl.InteractionDate ASC) AscSeq,
	ROW_NUMBER() OVER(PARTITION BY vl.PatientId ORDER BY vl.InteractionDate DESC) DescSeq,
	CASE
		WHEN (
			(cdc_fdb_db.dbo.fnIsPregnantAsOf(vl.PatientId, vl.InteractionDate) = 1)
			OR (anc.patient_guid IS NOT NULL)
			) THEN 1
		ELSE 0
	END IsPregnant,
	CASE
		WHEN (
			(cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](vl.PatientId, DATEADD(DAY, 90, c.ArtStartDate)) = 1)
			OR (cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](vl.PatientId, DATEADD(DAY, 180, c.ArtStartDate)) = 1)
			OR (cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](vl.PatientId, DATEADD(DAY, 210, c.ArtStartDate)) = 1)
			OR (bf.PatientId IS NOT NULL)
			--Add check from PostNatal visit
			OR (pnbf.IsBF = 1 AND pnbf.VisitDate BETWEEN c.ArtStartDate AND DATEADD(DAY, 210, c.ArtStartDate))
			) THEN 1
		ELSE 0
	END IsBreastFeeding
INTO #AllViralLoads
FROM ClientViralLoadTestResult vl
JOIN #TX_ClientsOfInterest c ON vl.PatientId = c.PatientId
LEFT JOIN (
		SELECT DISTINCT patient_guid
		FROM cdc_fdb_db.dbo.ANCV5InitialVisitInteraction1
			WHERE overview_interaction_time <= @EndDate
			AND (first_visit_data_edd >= @EndDate OR DATEADD(DAY, 270,first_visit_data_lmp) >= @EndDate)
	
	) anc ON vl.PatientId = anc.patient_guid
LEFT JOIN (
		SELECT * FROM cdc_fdb_db.dbo.fn_MotherBreastfeedingStatus_new(@StartDate, @EndDate, @SubSiteIds)
	) bf ON vl.PatientId = bf.PatientId AND bf.BreastFeeding = 'Yes'
LEFT JOIN (
	SELECT 
		PatientIdInt,
		InteractionId,
		EditLocation,
		VisitDate,
		IsBF,
		ROW_NUMBER() OVER(PARTITION BY PatientIdInt ORDER BY VisitDate DESC) Seq
	FROM (
			SELECT 
				patient_guid PatientIdInt,
				interaction_id InteractionId,
				edit_location EditLocation,
				overview_interaction_time VisitDate,
				1 IsBF
			FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
				WHERE (
					mother_details_feeding_option IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					OR mother_details_feeding_practice IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					)
			UNION
			SELECT 
				patient_guid PatientIdInt,
				interaction_id InteractionId,
				edit_location EditLocation,
				overview_interaction_time VisitDate,
				0 IsBF
			FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
				WHERE (
					mother_details_feeding_option NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					AND mother_details_feeding_practice NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
					)
					OR mother_details_feeding_option IS NULL
					OR mother_details_feeding_practice IS NULL
		) bf
	) pnbf ON vl.PatientId = pnbf.PatientIdInt AND pnbf.VisitDate BETWEEN @StartDate AND @EndDate;

SELECT DISTINCT 
	*,
	CASE
		WHEN 
			(DATEDIFF(DAY, ArtStartDate, InteractionDate) >= 180) -- General ART rule: On ARVs >= 6 months 
			OR (DATEDIFF(DAY, ArtStartDate, InteractionDate) >= 90 AND Sex = 'F' AND (IsPregnant = 1 OR IsBreastFeeding = 1)) -- EMTCT rule: PBFW on ARVs for >= 3 Months
			THEN 1
		ELSE 0
	END EligibleForVL
INTO #VL_Eligibility
FROM #AllViralLoads

SELECT DISTINCT *
INTO #FirstViralLoad
FROM #AllViralLoads
	WHERE AscSeq = 1;

SELECT DISTINCT *
INTO #LastViralLoad
FROM #AllViralLoads
	WHERE DescSeq = 1;

--SELECT DISTINCT *
--INTO #ViralLoadAt3M
--FROM #AllViralLoads
--	WHERE DurationOnArt = 3;

SELECT DISTINCT 
	vl.*,
	e.EligibleForVL
INTO #ViralLoadAt6M
FROM (
		SELECT DISTINCT *, ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY InteractionDate DESC) seq 
		FROM #AllViralLoads
			WHERE DurationOnArt >= 3 AND DurationOnArt <= 8 
	) vl 
LEFT JOIN #VL_Eligibility e ON vl.PatientId = e.PatientId AND vl.InteractionDate = e.InteractionDate
	WHERE vl.seq = 1;

SELECT DISTINCT *
INTO #ViralLoadAt12M
FROM (
		SELECT DISTINCT *, ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY InteractionDate DESC) seq 
		FROM #AllViralLoads
			WHERE DurationOnArt >= 9 AND DurationOnArt <= 15
	) vl
	WHERE vl.seq = 1;

SELECT DISTINCT *
INTO #ViralLoadAt24M
FROM #AllViralLoads
	WHERE DurationOnArt >= 21 AND DurationOnArt <= 27;

SELECT DISTINCT *
INTO #ViralLoadAt36M
FROM (
		SELECT DISTINCT *, ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY InteractionDate DESC) seq 
		FROM #AllViralLoads
			WHERE DurationOnArt >= 33 AND DurationOnArt <= 39
	) vl
	WHERE vl.seq = 1;


SELECT DISTINCT
	@ProvinceName Province,
	@DistrictName District,
	@FacilityName Facility,
	@FacilityGuid FacilityGuid,
	@hmiscode HMISCode,
	tx.*,
	vl0.DurationOnArt DurationOnArtAtFirst_VL,
	vl0.InteractionDate First_VL_Date,
	vl0.LabTestValue First_VL,
	--vl3.InteractionDate VL_at_3months,
	--vl3.LabTestValue Date_of_VL_at_3months,
	vl6.EligibleForVL EligibleForVL,
	vl6.InteractionDate Date_of_VL_at_6months,
	vl6.LabTestValue VL_at_6months,
	vl12.InteractionDate VL_at_12months,
	vl12.LabTestValue VL_at_12months,
	vl24.InteractionDate Date_of_VL_at_24months,
	vl24.LabTestValue VL_at_24months,
	vl36.InteractionDate Date_of_VL_at_36months,
	vl36.LabTestValue VL_at_36months,
	vlFin.InteractionDate Date_of_Last_VL,
	vlFin.DurationOnArt DurationOnArtAtLast_VL,
	vlFin.LabTestValue Last_VL
FROM #TX_ClientsOfInterest tx
LEFT JOIN #FirstViralLoad vl0 ON tx.PatientId = vl0.PatientId
--LEFT JOIN #ViralLoadAt3M vl3 ON tx.PatientId = vl3.PatientId
LEFT JOIN #ViralLoadAt6M vl6 ON tx.PatientId = vl6.PatientId
LEFT JOIN #ViralLoadAt12M vl12 ON tx.PatientId = vl12.PatientId
LEFT JOIN #ViralLoadAt24M vl24 ON tx.PatientId = vl24.PatientId
LEFT JOIN #ViralLoadAt36M vl36 ON tx.PatientId = vl36.PatientId
LEFT JOIN #LastViralLoad vlFin ON tx.PatientId = vlFin.PatientId;