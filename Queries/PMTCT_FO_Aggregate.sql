USE cdc_fdb_db;


DECLARE   
	@StartDate DATETIME,
	@EndDate DATETIME, 
    @ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@FacilityId VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME,
	@LoginName VARCHAR(50);

SET @Now = GETDATE()
SET @StartDate = '20220101'
SET @EndDate = EOMONTH(@StartDate, 0)
SET @EndDate = DATEADD(SECOND, 86399, @EndDate) --adding 23:59:59
IF(@EndDate > @Now) SET @EndDate = @Now -- Prevent future date 
SET @FacilityId = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode')
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid')
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId')
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId')
SET @LTFUThreshold = 30
SET @OnArtMode = 0 -- 1 = CURRENT, 0 = EVER 

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;


--************************* End Custom Declarations *****************************
--************************* Temp Table Declarations *****************************
IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility;
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @FacilityId);

IF (OBJECT_ID('tempdb..#AllFacility') IS NOT NULL) DROP TABLE #AllFacility;
SELECT DISTINCT 
	fss.SubSiteId,
	p.Name ProvinceName,
	d.Name DistrictName,
	f.FacilityName
INTO #AllFacility
FROM FacilitySubSite fss
JOIN Facility f ON fss.FacilityGuid = f.FacilityGuid
JOIN District d ON f.DistrictId = d.DistrictSeq
JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq;

IF(OBJECT_ID('tempdb..#Outcomes') IS NOT NULL) DROP TABLE #Outcomes;
CREATE TABLE #Outcomes(
	CohortMonth VARCHAR(6),
	Outcomes VARCHAR(50),
	Sex VARCHAR(1),
	Infants INT,
	Died INT,
	StartedARVProphylaxis INT,
	StartedART INT
);

INSERT INTO #Outcomes
EXEC [DataTech].dbo.[proc_GetHivExposedInfantCohortOutcomeReport_Aggregate]
	@ProvinceId,
	@DistrictId,
	@FacilityId,
	@StartDate,
	@EndDate,
	@LoginName;

SELECT 
	@ProvinceName Province,
	@DistrictName District,
	CASE @FacilityId
		--Chilanga Aliases
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010005T' THEN 'Tubalange Mini Hospital'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'Desai Health Post'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		-- Chirundu Aliases
		-- Luangwa Aliases
		-- Rufunsa Aliases
		WHEN '500700241' THEN 'Lubalashi Health Post'
		ELSE @FacilityName
	END Facility,
	@FacilityId FacilityHMIS,
	*
FROM #Outcomes;