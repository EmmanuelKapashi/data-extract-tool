USE cdc_fdb_db;
SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;

DECLARE
    @PROVINCE_ID  VARCHAR(2),
	@DISTRICT_ID VARCHAR(2),
	@FACILITY_ID VARCHAR(10),
	@FG varchar(100),
	@province varchar(32),
	@district varchar(32),
	@facilityname varchar(100),
	@StartDate DATETIME,
	@EndDate DATETIME,
	@LTFUThreshold INT,
	@NHC varchar(8),
	@OHC varchar(8),
	@RepPeriod varchar(20),
	@RepYear varchar(4);

SET @PROVINCE_ID = (select value from setting where name = 'ProvinceId')
SET @DISTRICT_ID = (select value from setting where name = 'DistrictId')
SET @FACILITY_ID = (select value from setting where name = 'Hmiscode')
SET @FG = (select value from setting where name = 'FacilityGuid');
SELECT @province = p.Name, @district=d.Name, @facilityname=f.FacilityName, @NHC=M.NewHMISCode, @OHC=M.OldHMISCode FROM Facility f 
join district d on f.DistrictId = d.Code 
join province p on d.ProvinceSeq = p.ProvinceSeq
join FacilityOldHMISMap M on M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG;
SET @StartDate = '20211001'; 
SET @EndDate = eomonth(@StartDate, 2); 
set @EndDate = dateadd(second, 86399, @EndDate); --adding 23:59:59
SET @LTFUThreshold = 30;
set @RepPeriod = (
		CASE DATEDIFF(MONTH, @StartDate, @EndDate) 
			WHEN 0 THEN convert(nvarchar(6), @StartDate,112) 
			WHEN 2 THEN 
				CASE
					WHEN  DATEPART(MONTH, @StartDate) IN (10, 11, 12) THEN CONCAT('FY', RIGHT((DATENAME(YEAR, @StartDate) +1), 2), 'Q1')
					WHEN  DATEPART(MONTH, @StartDate) NOT IN (10, 11, 12) THEN CONCAT('FY', RIGHT(DATENAME(YEAR, @StartDate), 2), 'Q', (DATEPART(QUARTER, @StartDate)+1))
				END
			ELSE 'UNDEFINED'
		END
);
set @RepYear = concat('FY', right(datename(year, @StartDate), 2));

DECLARE
		@PID VARCHAR(2) = '50',
		@DID VARCHAR(2) = '33',
		--@CFID VARCHAR(50),
		@FID VARCHAR(9);

	--SET @CFID = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGUID')
	SET @FID = (
				CASE @FG
					WHEN 'tvhd4akqn785db9xjaxk8oqrlb1lka08' THEN '500400251' --Chikoka
					WHEN 'g0gdw9d53dm32raz0h46vrsqgbr9417l' THEN '500400253' --Kafue East
					WHEN 'tuzn5ssrponmtkuw06wsr1plserg46h3' THEN '500400251' --Mtendere
					WHEN '7f2pvfn4p4t60gs4uez20t6hxvjm2ftv' THEN '500400021' --Mungu
					WHEN 'uk1oj1tva6bjmiknoj664bin2bkkdjn8' THEN '500400260' --Kabweza
					WHEN 'n48r3u184v9ty34953tr4fduagd12wfv' THEN '500400080' --Old Kabweza
				END
	)

IF (OBJECT_ID('tempdb..#CurrentFacility') IS NOT NULL) DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@PROVINCE_ID, @DISTRICT_ID, @NHC)
UNION
SELECT SubSiteId FROM dbo.fn_GetFacilitySubSiteIds(@PID, @DID, @FID);

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
IF(OBJECT_ID('tempdb..#CurrentOnArtMostRecentViralLoad') IS NOT NULL)DROP TABLE #CurrentOnArtMostRecentViralLoad;


    CREATE TABLE #Discontinued
    (
        PatientId             INT,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #DeadClients
    (
        PatientId             INT PRIMARY KEY,
        VisitDate             DATE,
        DiscontinuationReason VARCHAR(32)
    );
    CREATE TABLE #MostRecentHIVStatusAsOfEndDate
    (
        PatientId  INT PRIMARY KEY,
        TestResult INT,
        TestDate   DATE
    );
    CREATE TABLE #CurrentOnARTByClinicalVisit
    (
        PatientId             INT PRIMARY KEY,
        Sex                   VARCHAR(1),
        InteractionDate       DATE,
        DateOfBirth           DATE,
        AgeAsAtEnd            INT,
        AgeArtStart           INT,
        CurrentHivStatus      INT,
        CurrentHivStatusDate  DATE,
        OldestHivPosTestDate  DATE,
        ArtStartDate          DATE,
        ArtStartLocation      VARCHAR(9),
        ArtStartEditLocation  INT,
        CareStartDate         DATETIME,
        CareStartLocation     VARCHAR(9),
        CareStartEditLocation INT
    );
    CREATE TABLE #CurrentOnARTByDrugs
    (
        PatientId       INT PRIMARY KEY,
        InteractionDate DATE
    );
    CREATE TABLE #CurrentOnARTByVisitAndDrugs
    (
        PatientId                    INT PRIMARY KEY,
        InteractionDate              DATE,
        DeadBeforeEnd                BIT,
        NotActive                    BIT,
        DeadDate                     DATE,
        DiscontinuationDateBeforeEnd DATE,
        CurrentHivStatus             INT,
        DateOfBirth                  DATE,
        CareStartDate                DATE,
        ArtStartDate                 DATE,
        OldestHivPosTestDate         DATE,
        ArtStartEditLocation         INT,
        Sex                          VARCHAR(1),
        AgeAsAtEnd                   INT,
        AgeArtStart                  INT
    );
    CREATE TABLE #CurrentOnArt
    (
        PatientId    INT PRIMARY KEY,
    	  EditLocation INT,
        Age          INT,
        AgeLastVisit INT,
        Sex          VARCHAR(1),
        ArtStartDate DATE
    );
    CREATE TABLE #AllCurrPrEPClients
    (
        PatientId         INT PRIMARY KEY,
        Sex               VARCHAR(1),
        Age               INT,
        InitVisitDate     DATE,
        FollowUpVisitDate DATE
    );

	CREATE TABLE #CurrentOnArtMostRecentViralLoad
	(
		PatientId				INT PRIMARY KEY,
		Sex						VARCHAR(1),
		AgeLastVisit			INT,
		ArtStartDate			DATE,
		IsPregnant				INT,
		IsBreastFeeding			INT,
		ViralLoadOrderDate		DATETIME,
		ViralLoad				DECIMAL(12,2),
		IsPregnantAtLastVL		INT,
		IsBreastFeedingAtLastVL	INT,
		Indication				INT,
		EligibleForVL			INT,
		IsValidViralLoad		INT
	);

EXEC cdc_fdb_db.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@LTFUThreshold,
	1;

IF (object_id('tempdb..#TX_New_ClientsOfInterest')) IS NOT NULL
DROP TABLE #TX_New_ClientsOfInterest;
SELECT chs.PatientId, ArtStartDate, datediff(day, DateOfBirth, ArtStartDate)/365.25 AgeArtStart, Sex
    INTO #TX_New_ClientsOfInterest
FROM ClientHivSummary chs --left join Art45_PatientLocatorInteraction1 lctr1 on chs.PatientId = lctr1.patient_guid
        --left join Art3_PatientLocatorInteraction3 c on chs.PatientId = c.patient_guid
        left join crtPatientLocator loc on chs.PatientId = loc.PatientId and loc.VisitDate between @StartDate and @EndDate
	WHERE ArtStartDate IS NOT NULL
	AND ArtStartDate >= @StartDate
	AND ArtStartDate <= @EndDate
		--filter out transfer in and remove records where clients started ART in diferent facility @mchilando
	AND ((loc.PatientTransfer <> 2 or loc.PatientTransfer is null) and loc.VisitDate between @StartDate and @EndDate)
	AND chs.ArtStartEditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
	AND chs.PatientId NOT IN (select PatientId from #AllCurrPrEPClients); --exclude new PrEP clients

CREATE INDEX Indx_TX_New_ClientsOfInterest_PatientId
ON #TX_New_ClientsOfInterest (PatientId);

			
DECLARE
		@SubSiteIds SubSiteTableType;
	INSERT INTO @SubSiteIds
	SELECT DISTINCT SubSiteId
    FROM #CurrentFacility;
	
	IF(OBJECT_ID('tempdb..#MostRecentViralLoadResult') IS NOT NULL) DROP TABLE #MostRecentViralLoadResult;
	SELECT 
		avl.PatientId,
		avl.VisitDate ViralLoadOrderDate,
		avl.ViralLoad,
		avl.Indication,
		CASE
			WHEN (
				(cdc_fdb_db.dbo.fnIsPregnantAsOf(avl.PatientId, avl.VisitDate) = 1)
				OR (anc.patient_guid IS NOT NULL)
				) THEN 1
			ELSE 0
		END IsPregnant,
		CASE
			WHEN (
				(cdc_fdb_db.[dbo].[fnARTIsBreastfeedingAsOf](avl.PatientId, @EndDate) = 1)
				OR (bf.PatientId IS NOT NULL)
				--Add check from PostNatal visit
				OR (pnbf.IsBF = 1)
				) THEN 1
			ELSE 0
		END IsBreastFeeding
	INTO #MostRecentViralLoadResult
	FROM (
		
				SELECT 
					PatientId,
					VisitDate,
					ViralLoad,
					Indication,
					ROW_NUMBER() OVER(PARTITION BY PatientId ORDER BY VisitDate DESC) Seq
				FROM cdc_fdb_db.[dbo].[fn_GetAllViralLoadTestResults](@EndDate, DEFAULT) 
		) avl
	LEFT JOIN (
			SELECT DISTINCT patient_guid
			FROM cdc_fdb_db.dbo.ANCV5InitialVisitInteraction1
				WHERE overview_interaction_time <= @EndDate
				AND (first_visit_data_edd >= @EndDate OR DATEADD(DAY, 270,first_visit_data_lmp) >= @EndDate)
	
		) anc ON avl.PatientId = anc.patient_guid
	LEFT JOIN (
			SELECT * FROM cdc_fdb_db.dbo.fn_MotherBreastfeedingStatus_new(@StartDate, @EndDate, @SubSiteIds)
		) bf ON avl.PatientId = bf.PatientId AND bf.BreastFeeding = 'Yes'
	LEFT JOIN (
		SELECT 
			PatientIdInt,
			InteractionId,
			EditLocation,
			VisitDate,
			IsBF,
			ROW_NUMBER() OVER(PARTITION BY PatientIdInt ORDER BY VisitDate DESC) Seq
		FROM (
				SELECT 
					patient_guid PatientIdInt,
					interaction_id InteractionId,
					edit_location EditLocation,
					overview_interaction_time VisitDate,
					1 IsBF
				FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
					WHERE (
						mother_details_feeding_option IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						OR mother_details_feeding_practice IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						)
				UNION
				SELECT 
					patient_guid PatientIdInt,
					interaction_id InteractionId,
					edit_location EditLocation,
					overview_interaction_time VisitDate,
					0 IsBF
				FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
					WHERE (
						mother_details_feeding_option NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						AND mother_details_feeding_practice NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						)
						OR mother_details_feeding_option IS NULL
						OR mother_details_feeding_practice IS NULL
		) bf
	) pnbf ON avl.PatientId = pnbf.PatientIdInt AND pnbf.Seq = 1 AND pnbf.VisitDate BETWEEN DATEADD(YEAR, -1, @EndDate) AND @EndDate
		WHERE avl.Seq = 1
		AND avl.VisitDate BETWEEN DATEADD(YEAR, -1, @EndDate) AND @EndDate;
	
    IF(OBJECT_ID('tempdb..#TxCurrViralLoad') IS NOT NULL) DROP TABLE #TxCurrViralLoad;
	SELECT
		c.PatientId,
		c.Sex,
		c.AgeLastVisit,
		c.ArtStartDate,
		CASE
			WHEN (
				(cdc_fdb_db.dbo.fnIsPregnantAsOf(c.PatientId, @EndDate) = 1)
				OR (anc.patient_guid IS NOT NULL)
				) THEN 1
			ELSE 0
		END IsPregnant,
		CASE
			WHEN (
				(cdc_fdb_db.dbo.[fnARTIsBreastfeedingAsOf](c.PatientId, @EndDate) = 1)
				OR (bf.PatientId IS NOT NULL)
				--Add check from PostNatal visit
				OR (pnbf.IsBF = 1)
				) THEN 1
			ELSE 0
		END IsBreastFeeding,
		mrvl.ViralLoadOrderDate,
		mrvl.ViralLoad,
		mrvl.IsBreastFeeding IsBreastFeedingAtLastVL,
		mrvl.IsPregnant IsPregnantAtLastVL,
		mrvl.Indication
	INTO #TxCurrViralLoad
	FROM #CurrentOnArt c 
	LEFT JOIN #MostRecentViralLoadResult mrvl ON mrvl.PatientId = c.PatientId
	LEFT JOIN (
			SELECT DISTINCT patient_guid
			FROM cdc_fdb_db.dbo.ANCV5InitialVisitInteraction1
				WHERE overview_interaction_time <= @EndDate
				AND (first_visit_data_edd >= @EndDate OR DATEADD(DAY, 270,first_visit_data_lmp) >= @EndDate)
	
		) anc ON c.PatientId = anc.patient_guid
	LEFT JOIN (
			SELECT * FROM cdc_fdb_db.dbo.fn_MotherBreastfeedingStatus_new(@StartDate, @EndDate, @SubSiteIds)
		) bf ON c.PatientId = bf.PatientId AND bf.BreastFeeding = 'Yes'
	LEFT JOIN (
		SELECT 
			PatientIdInt,
			InteractionId,
			EditLocation,
			VisitDate,
			IsBF,
			ROW_NUMBER() OVER(PARTITION BY PatientIdInt ORDER BY VisitDate DESC) Seq
		FROM (
				SELECT 
					patient_guid PatientIdInt,
					interaction_id InteractionId,
					edit_location EditLocation,
					overview_interaction_time VisitDate,
					1 IsBF
				FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
					WHERE (
						mother_details_feeding_option IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						OR mother_details_feeding_practice IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						)
				UNION
				SELECT 
					patient_guid PatientIdInt,
					interaction_id InteractionId,
					edit_location EditLocation,
					overview_interaction_time VisitDate,
					0 IsBF
				FROM cdc_fdb_db.dbo.PostnatalV5Interaction1
					WHERE (
						mother_details_feeding_option NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						AND mother_details_feeding_practice NOT IN (0, 2) --0 = EBF, 1 = ERF, 2 = Mixed
						)
						OR mother_details_feeding_option IS NULL
						OR mother_details_feeding_practice IS NULL
		) bf
	) pnbf ON c.PatientId = pnbf.PatientIdInt AND pnbf.Seq = 1 AND pnbf.VisitDate BETWEEN DATEADD(YEAR, -1, @EndDate) AND @EndDate;

    IF(OBJECT_ID('tempdb..#TxCurrViralLoadPats') IS NOT NULL) DROP TABLE #TxCurrViralLoadPats;
	SELECT DISTINCT 
		*,
		CASE
			WHEN 
				(DATEDIFF(DAY, ArtStartDate, @EndDate) >= 180) -- General ART rule: On ARVs >= 6 months 
				OR (DATEDIFF(DAY, ArtStartDate, @EndDate) >= 90 AND Sex = 'F' AND (IsPregnant = 1 OR IsBreastFeeding = 1)) -- EMTCT rule: PBFW on ARVs for >= 3 Months
				THEN 1
			ELSE 0
		END EligibleForVL,
		CASE
			WHEN ViralLoad IS NULL OR (ViralLoad IS NOT NULL AND DATEDIFF(DAY,ArtStartDate, ViralLoadOrderDate) < 90) THEN 0 -- No VL or BVL
			WHEN 
				ViralLoad <1000
				AND ((DATEDIFF(DAY, ArtStartDate, @EndDate) >= 180) OR (DATEDIFF(DAY, ArtStartDate, @EndDate) >= 90 AND Sex = 'F' AND (IsPregnant = 1 OR IsBreastFeeding = 1)))
				AND DATEDIFF(DAY,ArtStartDate, ViralLoadOrderDate) >= 90
				THEN 1 
			WHEN 
				ViralLoad >=1000
				AND ((DATEDIFF(DAY, ArtStartDate, @EndDate) >= 180) OR (DATEDIFF(DAY, ArtStartDate, @EndDate) >= 90 AND Sex = 'F' AND (IsPregnant = 1 OR IsBreastFeeding = 1)))
				AND DATEDIFF(DAY,ArtStartDate, ViralLoadOrderDate) >= 90
				THEN 1 
		END IsValidViralLoad
	INTO #TxCurrViralLoadPats
	FROM #TxCurrViralLoad;

	
	INSERT INTO #CurrentOnArtMostRecentViralLoad
	SELECT DISTINCT * 
	FROM #TxCurrViralLoadPats;

--select * from #viralload2
--if(object_id('tempdb..#dbname') is not null) drop table #dbname
if(object_id('tempdb..#DS') is not null) drop table #DS
if(object_id('tempdb..#DSAggr') is not null) drop table #DSAggr
if(object_id('tempdb..#TX_Curr') is not null) drop table #TX_Curr
if(object_id('tempdb..#TXCurr_Eligible_for_VL') is not null) drop table #TXCurr_Eligible_for_VL
if(object_id('tempdb..#TX_PVLS_Denominator') is not null) drop table #TX_PVLS_Denominator
if(object_id('tempdb..#TX_PVLS_Numerator') is not null) drop table #TX_PVLS_Numerator

select 
	case
		when AgeLastVisit = 0 then '<01'
		when AgeLastVisit >= 1 and AgeLastVisit < 5 then '''01-04'
		when AgeLastVisit >= 5 and AgeLastVisit < 10 then '''05-09'
		when AgeLastVisit >= 10 and AgeLastVisit < 15 then '''10-14'
		when AgeLastVisit >= 15 and AgeLastVisit < 20 then '15-19'
		when AgeLastVisit >= 20 and AgeLastVisit < 25 then '20-24'
		when AgeLastVisit >= 25 and AgeLastVisit < 30 then '25-29'
		when AgeLastVisit >= 30 and AgeLastVisit < 35 then '30-34'
		when AgeLastVisit >= 35 and AgeLastVisit < 40 then '35-39'
		when AgeLastVisit >= 40 and AgeLastVisit < 45 then '40-44'
		when AgeLastVisit >= 45 and AgeLastVisit < 50 then '45-49'
		when AgeLastVisit >= 50 and AgeLastVisit < 55 then '50-54'
		when AgeLastVisit >= 55 and AgeLastVisit < 60 then '55-59'
		when AgeLastVisit >= 60 and AgeLastVisit < 65 then '60-64'
		when AgeLastVisit >= 65 then '65+'
	end AgeCatFine,
	*,
	CASE WHEN IsValidViralLoad = 1 AND ViralLoad < 1000 THEN 1 ELSE 0 END Suppressed
into #DS
from #CurrentOnArtMostRecentViralLoad



select 
	Sex, AgeCatFine,
	count(PatientId) TXCurr, 
	SUM(EligibleForVL) EligibleForVL,
	SUM(IsValidViralLoad) TX_PVLS_Denominator,
	SUM(Suppressed) TX_PVLS_Numerator, 
	'' Sep_0,
	SUM(CASE WHEN IsBreastFeeding = 1 AND EligibleForVL = 1 THEN 1 ELSE 0 END) BF_EligibleForVL,
	sum(CASE WHEN IsBreastFeeding = 1 AND IsValidViralLoad = 1 THEN 1 ELSE 0 END) TX_PVLS_DenominatorIsBF,
	sum(CASE WHEN IsBreastFeeding = 1 AND Suppressed = 1 THEN 1 ELSE 0 END) TX_PVLS_NumeratorisBF, 
	'' Sep_1,
	SUM(CASE WHEN IsPregnant = 1 AND EligibleForVL = 1 THEN 1 ELSE 0 END) Preg_EligibleForVL,
	sum(CASE WHEN IsPregnant = 1 AND IsValidViralLoad = 1 THEN 1 ELSE 0 END) TX_PVLS_DenominatorIsPregnant,
	sum(CASE WHEN IsPregnant = 1 AND Suppressed = 1 THEN 1 ELSE 0 END) TX_PVLS_NumeratorisPregnant
into #DSAggr 
from #DS
	group by Sex, AgeCatFine
	order by Sex, AgeCatFine

select 
	@RepPeriod [Period],
	@province Province,
	@district District,
	CASE @FACILITY_ID
		--Chilanga Aliases
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010005T' THEN 'Tubalange Mini Hospital'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'Desai Health Post'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		-- Chirundu Aliases
		-- Luangwa Aliases
		-- Rufunsa Aliases
		WHEN '500700241' THEN 'Lubalashi Health Post'
		ELSE @facilityname 
	END Facility,
	* FROM #DSAggr