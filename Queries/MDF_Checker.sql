USE cdc_fdb_db;
SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;

DECLARE
    @PROVINCE_ID  VARCHAR(2),
	@DISTRICT_ID VARCHAR(2),
	@FACILITY_ID VARCHAR(10),
	@FG VARCHAR(50),
	@province VARCHAR(32),
	@district VARCHAR(32),
	@facilityname VARCHAR(100),
	@NHC VARCHAR(8),
	@OHC VARCHAR(8),
	@Existingdate DATETIME,
	@FilePath VARCHAR(MAX);


SET @PROVINCE_ID = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
SET @DISTRICT_ID = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
SET @FACILITY_ID = (SELECT [Value] FROM Setting WHERE [Name] = 'Hmiscode');
SET @FG = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
SET @Existingdate = CONVERT(VARCHAR,GETDATE(),112);
SET @FilePath = (SELECT [filename] FROM dbo.sysfiles WHERE [name] = 'database');
SET @FilePath = REPLACE(@FilePath, '\smartcare_db.mdf', '');

SELECT 
	@province = p.Name, 
	@district = d.Name, 
	@facilityname = f.FacilityName, 
	@NHC = M.NewHMISCode, 
	@OHC = M.OldHMISCode 
FROM Facility f
JOIN district d ON f.DistrictId = d.DistrictSeq
JOIN province p ON d.ProvinceSeq = p.ProvinceSeq
JOIN FacilityOldHMISMap M ON M.FacilityGUID = F.FacilityGuid
	WHERE f.FacilityGuid = @FG;



SELECT DISTINCT
	--@FG as FacilityGuid,
	@FACILITY_ID AS FacilityHMIS,
	@province Province,
	@district District,
	@facilityname as FacilityName,
	@FilePath as MDF_FOLDER,
	(select count(*) from Registration) as Registration,
	(select count(*) from ClientHivSummary) as ClientHivSummary
	--	, (select count(*) from ClientHivSummary where IhapDate is not null) as ClientHivSummaryWithIHap
	--	, (select count(*) from crtIHAP ) as crtIHAP
	--	, (select count(*) from crtIHAP where ArtNumber is not null ) as crtIHAPWithArtNumbers