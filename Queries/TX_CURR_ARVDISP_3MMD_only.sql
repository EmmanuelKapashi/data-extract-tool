USE cdc_fdb_db;

DECLARE   
	@StartDate DATETIME,
	@EndDate DATETIME, 
    @ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME

SET @Now = GETDATE()
SET @StartDate = '20211201'
SET @EndDate = EOMONTH(@StartDate, 0)
SET @EndDate = DATEADD(SECOND, 86399, @EndDate) --adding 23:59:59
IF(@EndDate > @Now) SET @EndDate = @Now -- Prevent future date 
SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode')
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid')
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId')
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId')
SET @LTFUThreshold = 30
SET @OnArtMode = 1 -- 1 = CURRENT, 0 = EVER 

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;

DECLARE
		@PID2 VARCHAR(2) = '50',
		@DID2 VARCHAR(2) = '33',
		@FID2 VARCHAR(9);

	SET @FID2 = (
				CASE @FacilityGuid
					WHEN 'tvhd4akqn785db9xjaxk8oqrlb1lka08' THEN '500400251' --Chikoka
					WHEN 'g0gdw9d53dm32raz0h46vrsqgbr9417l' THEN '500400253' --Kafue East
					WHEN 'tuzn5ssrponmtkuw06wsr1plserg46h3' THEN '500400251' --Mtendere
					WHEN '7f2pvfn4p4t60gs4uez20t6hxvjm2ftv' THEN '500400021' --Mungu
					WHEN 'uk1oj1tva6bjmiknoj664bin2bkkdjn8' THEN '500400260' --Kabweza
					WHEN 'n48r3u184v9ty34953tr4fduagd12wfv' THEN '500400080' --Old Kabweza
				END
	)

--************************* End Custom Declarations *****************************
--************************* Temp Table Declarations *****************************
IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode)
UNION 
SELECT DISTINCT SubSiteId
FROM dbo.fn_GetFacilitySubSiteIds(@PID2, @DID2, @FID2);

   
IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
    
CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);
CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);
CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId           INT PRIMARY KEY,
    InteractionDate     DATE
);
CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);
--************************* End Temp Table Declarations *****************************************
	--=================================== END TEST AREA ======================================= --PASS2

EXEC DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate;

IF(OBJECT_ID('tempdb..#Regimen') IS NOT NULL) DROP TABLE #Regimen;
CREATE TABLE #Regimen(
	Interaction_Id	INT NOT NULL,-- PRIMARY KEY,
	PatId			INT NOT NULL,
	VisitDate		DATETIME NOT NULL,
	NextVisitDate	DATETIME,
	DailyDrugStock	INT,
	ArvRegimen		VARCHAR(100),
	EditLocation	INT,
	ServiceArea		VARCHAR(100),
	ArtPasserby		INT,
	PepDispensation	INT
);
EXEC DataTech.dbo.[proc_ArvRegimenDatesAndQuanitity];

IF(OBJECT_ID('tempdb..#CurrentRegimen') IS NOT NULL) DROP TABLE #CurrentRegimen
SELECT DISTINCT 
	a.PatId,
	a.Interaction_Id,
	a.VisitDate,
	a.NextVisitDate,
	a.DailyDrugStock,
	a.ArvRegimen
INTO #CurrentRegimen
FROM (
		SELECT DISTINCT 
			ROW_NUMBER() OVER(PARTITION BY PatId ORDER BY VisitDate DESC) Seq,
			*
		FROM #Regimen
			WHERE (VisitDate >= @StartDate AND VisitDate <= @EndDate)
				OR (VisitDate < @StartDate AND NextVisitDate >= DATEADD(DAY, -@LTFUThreshold, @EndDate))
) a
	WHERE a.Seq = 1;

--**************************************** Current Address ********************************************
IF(OBJECT_ID('tempdb..#Address') IS NOT NULL) DROP TABLE #Address;
SELECT DISTINCT * INTO #Address FROM (
	SELECT ROW_NUMBER() OVER(PARTITION BY PatientGUID ORDER BY CreatedTime DESC) N, NaturalNumber PatientID, A.PatientGUID,
		CASE
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN NULL
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN REPLACE(PhoneContact, ',', '')
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN REPLACE(PhoneNumber, ',', '')
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN REPLACE(MobilePhoneNumber, ',', '')
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN CONCAT(REPLACE(PhoneContact, ',', ''), ' | ', REPLACE(PhoneNumber, ',', ''), ' | ', REPLACE(MobilePhoneNumber, ',', ''))
			WHEN 
				(PhoneContact = '' OR PhoneContact IS NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN CONCAT(REPLACE(PhoneNumber, ',', ''), ' | ', REPLACE(MobilePhoneNumber, ',', ''))
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber = '' OR PhoneNumber IS NULL) 
				AND (MobilePhoneNumber != '' AND MobilePhoneNumber IS NOT NULL)
				THEN CONCAT(REPLACE(PhoneContact, ',', ''), ' | ', REPLACE(MobilePhoneNumber, ',', ''))
			WHEN 
				(PhoneContact != '' AND PhoneContact IS NOT NULL) 
				AND (PhoneNumber != '' AND PhoneNumber IS NOT NULL) 
				AND (MobilePhoneNumber = '' OR MobilePhoneNumber IS NULL) 
				THEN CONCAT(REPLACE(PhoneContact, ',', ''), ' | ', REPLACE(PhoneNumber, ',', ''))
		END PNumber 
	FROM [Address] A JOIN guidmap g ON a.PatientGUID = g.MappedGuid
		WHERE AddressType = 'Current'
) Ads
	WHERE N = 1;
--use both PatientGUID AND PatientId to link ON JOINs


--************************************** END Current Address *******************************************

IF(OBJECT_ID('tempdb..#LastVL') IS NOT NULL) DROP TABLE #LastVL;
IF(object_id('tempdb..#FDS') is not null) drop table #FDS;

WITH ViralLoadResults AS (
		
		SELECT DISTINCT 
			vl.*,
			c.ArtStartDate,
			DATEDIFF(MONTH, c.ArtStartDate, vl.InteractionDate) DurationOnArtAtVl,
			ROW_NUMBER() OVER(PARTITION BY vl.PatientId ORDER BY vl.InteractionDate DESC) seq
		FROM ClientViralLoadTestResult vl
		JOIN #CurrentOnArt c ON vl.PatientId = c.PatientId
			WHERE vl.LabTestValue IS NOT NULL
			AND ISNUMERIC(vl.LabTestValue) = 1
),

LastViralLoadResult AS (

		SELECT *
		FROM ViralLoadResults vl
			WHERE seq = 1
			AND DurationOnArtAtVl >= 3
)


SELECT DISTINCT
	g.MappedGuid PatientGUID,
	r.PatientId NUPN,
	LTRIM(r.SurName) Surname,
	CASE WHEN r.MiddleName IS NOT NULL AND LTRIM(r.MiddleName) != '' THEN CONCAT(LTRIM(r.FirstName), ' ', LTRIM(r.MiddleName)) ELSE LTRIM(r.FirstName) END Forenames,
	r.ArtNumber,
	r.DateOfBirth,
	a.*,
	DATEDIFF(DAY, a.ArtStartDate, @EndDate)/365.25*12.0 DurationOnArt,
	CASE
		WHEN a.AgeLastVisit >= 0 AND a.AgeLastVisit < 1 THEN '<01'
		WHEN a.AgeLastVisit >= 1 AND a.AgeLastVisit < 5 THEN '''01-04'
		WHEN a.AgeLastVisit >= 5 AND a.AgeLastVisit < 10 THEN '''05-09'
		WHEN a.AgeLastVisit >= 10 AND a.AgeLastVisit < 15 THEN '''10-14'
		WHEN a.AgeLastVisit >= 15 AND a.AgeLastVisit < 20 THEN '15-19'
		WHEN a.AgeLastVisit >= 20 AND a.AgeLastVisit < 25 THEN '20-24'
		WHEN a.AgeLastVisit >= 25 AND a.AgeLastVisit < 30 THEN '25-29'
		WHEN a.AgeLastVisit >= 30 AND a.AgeLastVisit < 35 THEN '30-34'
		WHEN a.AgeLastVisit >= 35 AND a.AgeLastVisit < 40 THEN '35-39'
		WHEN a.AgeLastVisit >= 40 AND a.AgeLastVisit < 45 THEN '40-44'
		WHEN a.AgeLastVisit >= 45 AND a.AgeLastVisit < 50 THEN '45-49'
		WHEN a.AgeLastVisit >= 50 AND a.AgeLastVisit < 55 THEN '45-49'
		WHEN a.AgeLastVisit >= 55 AND a.AgeLastVisit < 60 THEN '45-49'
		WHEN a.AgeLastVisit >= 60 AND a.AgeLastVisit < 65 THEN '45-49'
		WHEN a.AgeLastVisit >= 65 THEN '>=65'
		ELSE 'Unclassified'
	END AgeCatFine,
	CASE
		WHEN a.AgeLastVisit >= 0 AND a.AgeLastVisit < 15 THEN '<15'
		WHEN a.AgeLastVisit >= 15 AND a.AgeLastVisit < 20 THEN '15-19'
		WHEN a.AgeLastVisit >= 20 THEN '>=20'
		ELSE 'Unclassified'
	END AgeCatCourse,
	cr.ArvRegimen,
	cr.DailyDrugStock,
	CASE 
		WHEN cr.DailyDrugStock IS NULL OR cr.DailyDrugStock = 0 THEN 'No Dispensation'
		WHEN cr.DailyDrugStock > 0 AND cr.DailyDrugStock < 90 THEN 'Not MMD'
		WHEN cr.DailyDrugStock >= 90 AND cr.DailyDrugStock < 180 THEN '3 MMD'
		WHEN cr.DailyDrugStock >= 180 THEN '6 MMD'
	END MMD_Catergory,
	CASE
		WHEN a.AgeLastVisit >= 15 and cr.ArvRegimen IN (
			'TDF+3TC+DTG', 'EFV+3TC+TDF',
			'TAF+3TC+DTG', 'DTG+FTC+TAF+TDF+3TC+DTG',
			'TAF+FTC+DTG', 'DTG+FTC+TAF',
			'TDF+3TC+EFV',
			'TDF+FTC+EFV',
			'TAF+FTC+EFV',
			'3TC+AZT+LPV+RTV',
			'DTG+FTC+TAF'
			) THEN '1'
		WHEN a.AgeLastVisit >= 15 and cr.ArvRegimen IN (
			'DTG+3TC+AZT', 'AZT+3TC+DTG' 
			) THEN '2'
		--WHEN a.AgeLastVisit >= 15 and cr.ArvRegimen IN (
			
		--	) THEN 3
		--WHEN a.AgeLastVisit < 15 and cr.ArvRegimen IN (
			
		--	) THEN 1
		--WHEN a.AgeLastVisit < 15 and cr.ArvRegimen IN (
			
		--	) THEN 2
		--WHEN a.AgeLastVisit < 15 and cr.ArvRegimen IN (
			
		--	) THEN 3
		WHEN (cr.ArvRegimen IN ('FTC+TDF', '3TC+TDF', 'FTC+TAF', '3TC+TAF') AND a.AgeLastVisit >= 2) 
			OR (cr.ArvRegimen LIKE '%NVP%' AND a.AgeLastVisit < 2)
			THEN 'Possibly PrEP/HEI'
		WHEN cr.ArvRegimen IS NULL THEN 'No Dispensation'
		ELSE 'Undefined or Non-regimen'
	END RegimenLine,
	vl.LabTestValue LastVLResult,
	vl.InteractionDate LastVLDate,
	CASE
		WHEN (vl.LabTestValue IS NULL AND DATEDIFF(DAY, a.ArtStartDate, @EndDate)/365.25*12.0 >= 5.00 )
			OR (vl.LabTestValue > 1000 AND DATEDIFF(DAY, vl.InteractionDate, @EndDate)/365.25*12.0 > 3.00 AND DATEDIFF(DAY, a.ArtStartDate, @EndDate)/365.25*12.0 >= 5.00)
			OR (vl.LabTestValue IS NOT NULL AND DATEDIFF(DAY, a.ArtStartDate, @EndDate)/365.25*12.0 >= 5 AND DATEDIFF(DAY, vl.InteractionDate, @EndDate)/365.25*12.0 > 12.0)
			THEN 'Due'
	END DueForVL,
	ad.PNumber CurrentPhoneNumbers
INTO #FDS
FROM #CurrentOnArt a
JOIN GuidMap g ON a.PatientId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid
JOIN crtRegistrationInteraction r ON a.PatientId = r.patientId_int
LEFT JOIN #CurrentRegimen cr ON a.PatientId = cr.PatId
LEFT JOIN LastViralLoadResult vl ON a.PatientId = vl.PatientId
LEFT JOIN #Address ad ON a.PatientId = ad.PatientID;

SELECT 
	@ProvinceName Province,
	@DistrictName District,
	CASE @hmiscode
		--Chilanga Aliases
		WHEN '50010005A' THEN 'Mundengwa'
		WHEN '50010005T' THEN 'Tubalange Mini Hospital'
		WHEN '50010006A' THEN 'Nakachenje Mini Hospital'
		--Chongwe Aliases
		WHEN '500300GC0' THEN 'Chongwe District Hospital'
		--Kafue Aliases
		WHEN '500400021' THEN 'Mungu'
		WHEN '500400053' THEN 'Lukolongo'
		--WHEN '500400080' THEN 'Chisankane Rural Health Center' -- Duplicate HMIS Code used
		WHEN '50040008O' THEN 'Old Kabweza Health Post' -- Duplicate HMIS Code used
		WHEN '50040025C' THEN 'Chikoka Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025E' THEN 'Kafue East Urban Health Centre' -- Duplicate HMIS Code used
		WHEN '50040025M' THEN 'Mtendere Urban Health Centre' -- Duplicate HMIS Code used
		--WHEN '500400260' THEN 'Shimabala Health Post' -- Duplicate HMIS Code used
		WHEN '50040026K' THEN 'New Kabweza Health Post' -- Duplicate HMIS Code used
		--WHEN '500400261' THEN 'New Kabweza Health Post'
		-- Lusaka Aliases
		WHEN '500600308' THEN 'Chainda Market Community Post'
		WHEN '500600329' THEN 'Chaisa Urban Health Centre'
		WHEN '50060035Y' THEN 'John Howard - Discover Health'
		WHEN '50060035X' THEN 'Saint Lukes - JSI'
		WHEN '50060036B' THEN 'Chazanga Health Post'
		WHEN '50060036C' THEN 'Chazanga Community Post'
		WHEN '50060037X' THEN 'Chelstone CBTO'
		WHEN '50060037O' THEN 'Obama Market Community Post'
		WHEN '50060046X' THEN 'Chimwemwe Health Post'
		WHEN '50060046Y' THEN 'Garden Site 3 Health Post'
		WHEN '50060046K' THEN 'Katambalala Community Post'
		WHEN '500600JH3' THEN 'State House Clinic'
		WHEN '500600JH0' THEN 'State House Clinic'
		WHEN '50060106B' THEN 'Desai Health Post'
		WHEN '50060106C' THEN 'Desai Health Post'
		WHEN '50060106A' THEN 'Desai Health Post'
		WHEN '50060106X' THEN 'Jordan Health Post'
		WHEN '50060124A' THEN 'Lumumba Market Community Post'
		WHEN '500601291' THEN 'Kabwata Market Community Post'
		WHEN '50060131K' THEN 'Kalingalinga Market Community Post'
		WHEN '50060135B' THEN 'Comesa Market Community Post'
		WHEN '50060135A' THEN 'Lumbama Market  Community Post'
		WHEN '50060136U' THEN 'Bethel Linda Health Post'
		WHEN '50060136Y' THEN 'John Laing Health Post'
		WHEN '50060136V' THEN 'Kanyama Salvation Army Health Post'
		WHEN '500601361' THEN 'Pamodzi Community Post'
		WHEN '50060137P' THEN 'Mbasela Health Post'
		WHEN '50060138B' THEN 'Garden House Health Post'
		WHEN '500601381' THEN 'Masauko Community Post'
		WHEN '50060140K' THEN 'Kaunda Square Market Community Post'
		WHEN '50060143A' THEN 'Chawama Central Market Community Post'
		WHEN '50060180N' THEN 'Mandevu Community Post'
		WHEN '50060183X' THEN 'Chunga Health Post - Discover Health'
		WHEN '50060198N' THEN 'Mtendere Market Community Post'
		WHEN '50060198X' THEN 'Kalikiliki Health Post - Discover Health'
		WHEN '50060211N' THEN 'Ng'+N'''ombe Market Community Post'
		WHEN '50060243A' THEN 'Chachacha Market Community Post'
		WHEN '50060243C' THEN 'PPAZ'
		WHEN '5006XX96A' THEN 'Chunga Community Post'
		WHEN '5006XX96B' THEN 'Twikatane Community Post'
		-- Chirundu Aliases
		-- Luangwa Aliases
		-- Rufunsa Aliases
		WHEN '500700241' THEN 'Lubalashi Health Post'
		ELSE @FacilityName
	END Facility,
	@hmiscode FacilityHMIS,
	NUPN,
	Surname,
	Forenames,
	Sex,
	DateOfBirth,
	Age,
	AgeCatCourse,
	AgeCatFine,
	CurrentPhoneNumbers,
	ArtStartDate,
	FLOOR(DurationOnArt) DurationOnArtInMonths,
	ArtNumber,
	ArvRegimen,
	DailyDrugStock,
	MMD_Catergory,
	LastVLDate,
	LastVLResult,
	DueForVL
FROM #FDS
	--GROUP BY ArvRegimen, AgeCatCourse
	ORDER BY MMD_Catergory, DurationOnArt;