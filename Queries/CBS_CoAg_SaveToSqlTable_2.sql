USE cdc_fdb_db;

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
SET NOCOUNT ON;


DECLARE   
	@BeginDate DATETIME,
	@BeginDate2 DATETIME,
	@StartDate DATETIME,
	@EndDate DATETIME, 
    @StartDate2 DATETIME,
	@EndDate2 DATETIME,
	@ProvinceId VARCHAR(2), 
	@DistrictId VARCHAR(2), 
	@FacilityGuid VARCHAR(100), 
	@hmiscode VARCHAR(10),  
	@ProvinceName VARCHAR(100), 
	@DistrictName VARCHAR(100), 
	@FacilityName VARCHAR(100), 
	@NewHmisCode VARCHAR(10), 
	@OldHmisCode VARCHAR(10),
	@LTFUThreshold INT, 
	@RC INT,
	@OnArtMode BIT,
	@Now DATETIME;

SET @Now = GETDATE();
SET @BeginDate = '20200101';
SET @BeginDate2 = '20210101';

SET @StartDate2 = '20211201';
SET @EndDate2 = EOMONTH(@StartDate2, 0);
SET @EndDate2 = DATEADD(SECOND, 86399, @EndDate2); --adding 23:59:59
IF(@EndDate2 > @Now) SET @EndDate2 = @Now; -- Prevent future date

SET @StartDate = DATEADD(YEAR, -1, @StartDate2);
SET @EndDate = EOMONTH(@StartDate, 0);
SET @EndDate = DATEADD(SECOND, 86399, @EndDate); --adding 23:59:59
 
SET @hmiscode = (SELECT [Value] FROM Setting WHERE [Name] = 'hmiscode');
SET @FacilityGuid = (SELECT [Value] FROM Setting WHERE [Name] = 'FacilityGuid');
SET @ProvinceId = (SELECT [Value] FROM Setting WHERE [Name] = 'ProvinceId');
SET @DistrictId = (SELECT [Value] FROM Setting WHERE [Name] = 'DistrictId');
SET @LTFUThreshold = 30;
SET @OnArtMode = 1; -- 1 = CURRENT, 0 = EVER 
--SELECT @StartDate StartDate, @EndDate EndDate;
--SELECT @StartDate2 StartDate, @EndDate2 EndDate;

SELECT 
    @ProvinceName = p.[Name], 
    @DistrictName = d.[Name], 
    @FacilityName = f.FacilityName, 
    @NewHmisCode = f.HMISCode, 
    @OldHmisCode = m.OldHMISCode 
FROM Facility f 
LEFT JOIN District d ON f.DistrictId = d.Code 
LEFT JOIN Province p ON d.ProvinceSeq = p.ProvinceSeq
LEFT JOIN FacilityOldHMISMap m ON m.FacilityGUID = f.FacilityGuid
	WHERE F.FacilityGuid = @FacilityGuid;

--From facilities, get AND filter only the current facility in the current province AND district
IF (OBJECT_ID('tempdb..#CurrentFacility')) IS NOT NULL
    DROP TABLE #CurrentFacility -- Renamed FROM facility filter
SELECT DISTINCT SubSiteId
INTO #CurrentFacility
FROM dbo.fn_GetFacilitySubSiteIds(@ProvinceId, @DistrictId, @hmiscode);

DECLARE
	@SubSiteIds SubSiteTableType;
INSERT INTO @SubSiteIds
SELECT DISTINCT SubSiteId
FROM #CurrentFacility;

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) DROP TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) DROP TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) DROP TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) DROP TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) DROP TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) DROP TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)DROP TABLE #AllCurrPrEPClients;
IF(OBJECT_ID('tempdb..#CbsCohort') IS NOT NULL)DROP TABLE #CbsCohort;

    
CREATE TABLE #Discontinued
(
    PatientId             INT,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #DeadClients
(
    PatientId             INT PRIMARY KEY,
    VisitDate             DATE,
    DiscontinuationReason VARCHAR(32)
);
CREATE TABLE #MostRecentHIVStatusAsOfEndDate
(
    PatientId  INT PRIMARY KEY,
    TestResult INT,
    TestDate   DATE
);
CREATE TABLE #CurrentOnARTByClinicalVisit
(
    PatientId             INT PRIMARY KEY,
    Sex                   VARCHAR(1),
    InteractionDate       DATE,
    DateOfBirth           DATE,
    AgeAsAtEnd            INT,
    AgeArtStart           INT,
    CurrentHivStatus      INT,
    CurrentHivStatusDate  DATE,
    OldestHivPosTestDate  DATE,
    ArtStartDate          DATE,
    ArtStartLocation      VARCHAR(9),
    ArtStartEditLocation  INT,
    CareStartDate         DATETIME,
    CareStartLocation     VARCHAR(9),
    CareStartEditLocation INT
);
CREATE TABLE #CurrentOnARTByDrugs
(
    PatientId           INT PRIMARY KEY,
    InteractionDate     DATE
);
CREATE TABLE #CurrentOnARTByVisitAndDrugs
(
    PatientId                    INT PRIMARY KEY,
    InteractionDate              DATE,
    DeadBeforeEnd                BIT,
    NotActive                    BIT,
    DeadDate                     DATE,
    DiscontinuationDateBeforeEnd DATE,
    CurrentHivStatus             INT,
    DateOfBirth                  DATE,
    CareStartDate                DATE,
    ArtStartDate                 DATE,
    OldestHivPosTestDate         DATE,
    ArtStartEditLocation         INT,
    Sex                          VARCHAR(1),
    AgeAsAtEnd                   INT,
    AgeArtStart                  INT
);
CREATE TABLE #CurrentOnArt
(
    PatientId    INT PRIMARY KEY,
    EditLocation INT,
    Age          INT,
    AgeLastVisit INT,
    Sex          VARCHAR(1),
    ArtStartDate DATE
);
CREATE TABLE #AllCurrPrEPClients
(
    PatientId         INT PRIMARY KEY,
    Sex               VARCHAR(1),
    Age               INT,
    InitVisitDate     DATE,
    FollowUpVisitDate DATE
);

EXEC DataTech.dbo.proc_GetTxCurr_Base
	@StartDate,
	@EndDate,
	@LTFUThreshold,
	@OnArtMode;

SELECT DISTINCT 
	*,
	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX' PatientStatus,
	CONVERT(DATETIME, '1900-JAN-01') StausDate
INTO #CbsCohort
FROM #CurrentOnArt;

IF(OBJECT_ID('tempdb..#Discontinued') IS NOT NULL) TRUNCATE TABLE #Discontinued;
IF(OBJECT_ID('tempdb..#DeadClients') IS NOT NULL) TRUNCATE TABLE #DeadClients;
IF(OBJECT_ID('tempdb..#MostRecentHIVStatusAsOfEndDate') IS NOT NULL) TRUNCATE TABLE #MostRecentHIVStatusAsOfEndDate;
IF(OBJECT_ID('tempdb..#CurrentOnARTByClinicalVisit') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByClinicalVisit;
IF(OBJECT_ID('tempdb..#CurrentOnARTByDrugs') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnARTByVisitAndDrugs') IS NOT NULL) TRUNCATE TABLE #CurrentOnARTByVisitAndDrugs;
IF(OBJECT_ID('tempdb..#CurrentOnArt') IS NOT NULL) TRUNCATE TABLE #CurrentOnArt;
IF(OBJECT_ID('tempdb..#AllCurrPrEPClients') IS NOT NULL)TRUNCATE TABLE #AllCurrPrEPClients;


EXEC DataTech.dbo.proc_GetTxCurr_Base
	@StartDate2,
	@EndDate2,
	@LTFUThreshold,
	@OnArtMode;

UPDATE x
SET x.PatientStatus = 'Active',
	x.StausDate = @EndDate2
FROM #CbsCohort x
JOIN #CurrentOnArt c ON x.PatientId = c.PatientId;

UPDATE x
SET x.PatientStatus = 'Dead',
	x.StausDate = ded.VisitDate
FROM #CbsCohort x
JOIN #DeadClients ded ON x.PatientId = ded.PatientId
	WHERE x.PatientStatus = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

UPDATE x
SET x.PatientStatus = dis.DiscontinuationReason,
	x.StausDate = dis.VisitDate
FROM #CbsCohort x
JOIN #Discontinued dis ON x.PatientId = dis.PatientId
	WHERE x.PatientStatus = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

UPDATE x
SET x.PatientStatus = 'IIT/LTFU',
	x.StausDate = @EndDate2
FROM #CbsCohort x
	WHERE x.PatientStatus = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

IF (OBJECT_ID('tempdb..#TX_ClientsOfInterest') IS NOT NULL) DROP TABLE #TX_ClientsOfInterest;
SELECT DISTINCT
	@ProvinceName Province,
	@DistrictName District,
	@FacilityName Facility,
	@hmiscode HMISCode,
	@FacilityGuid FacilityGUID,
	'' Demographic,
	g.MappedGuid PatientGuid,
	chs.PatientId PatientIdInt, 
	chs.Sex,
	chs.ArtStartDate,
	cri.ArtNumber, 
	dbo.fn_age(cri.DateOfbirth, ch.ArtStartDate) Age,
	CASE
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 0 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 15 THEN '<15'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 15 THEN '15+'
		ELSE 'Uncategorised'
	END AgeGroupChildOrAdult,
	CASE
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 0 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 10 THEN '0-9'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 10 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 15 THEN '''10-14'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 15 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 25 THEN '15-24'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 25 THEN '25+'
		ELSE 'Uncategorised'
	END AgeGroupFinerChildOrAdult, 
	CASE
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 0 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 1 THEN '<01'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 1 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 5 THEN '''01-04'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 5 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 10 THEN '''05-09'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 10 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 15 THEN '''10-14'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 15 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 20 THEN '15-19'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 20 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 25 THEN '20-24'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 25 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 30 THEN '25-29'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 30 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 35 THEN '30-34'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 35 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 40 THEN '35-39'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 40 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 45 THEN '40-44'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 45 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 50 THEN '45-49'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 50 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 55 THEN '50-54'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 55 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 60 THEN '55-59'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 60 AND dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) < 65 THEN '60-64'
		WHEN dbo.fn_age(cri.DateOfbirth, chs.ArtStartDate) >= 65 THEN '65+'
		ELSE 'Uncategorised'
	END AgeGroup,
	cri.DateOfDeath,
	cri.EducationLevel,
	cri.Occupation,
	cri.Married,

	'' Testing,
	hts.VisitDate OldestEmpiricalHivPosTestDate,
	hts.TestResults,
	hts.EntryPoint,
	
	'' Treatment,
	chs.PatientStatus,
	chs.StausDate,
	ch.OldestHivPosTestDate,
	ch.IhapDate,
	ch.IhapEditLocation
	
INTO #TX_ClientsOfInterest
FROM ClientHivSummary ch 
JOIN GuidMap g ON ch.PatientId = g.NaturalNumber AND g.MappedGuid = g.OwningGuid
JOIN crtRegistrationInteraction cri ON cri.patientid_int = ch.PatientId
JOIN #CbsCohort chs ON ch.PatientId = chs.PatientId
LEFT JOIN (
		SELECT 
			PatientIdInt, EditLocation, VisitDate, MAX(EntryPoint) EntryPoint, TestResults
		FROM DataTech.dbo.GetEmpiricalHivTestResults(@EndDate2)
			WHERE TestResults = 1
			AND EditLocation IN (SELECT SubSiteId FROM #CurrentFacility)
			GROUP BY PatientIdInt, EditLocation, VisitDate, TestResults
	) hts ON ch.PatientId = hts.PatientIdInt;

CREATE INDEX Indx_TX_ClientsOfInterest_PatientId
    ON #TX_ClientsOfInterest (PatientIdInt);

CREATE INDEX Indx_TX_ClientsOfInterest_Sex
    ON #TX_ClientsOfInterest (Sex);

CREATE INDEX Indx_TX_ClientsOfInterest_AgeArtStart
    ON #TX_ClientsOfInterest (Age);


INSERT INTO DataTech.dbo.CbsCohort
SELECT * FROM #TX_ClientsOfInterest;

IF (OBJECT_ID('tempdb..#VL_Eligibility') IS NOT NULL) DROP TABLE #VL_Eligibility;
IF (OBJECT_ID('tempdb..#AllViralLoads') IS NOT NULL) DROP TABLE #AllViralLoads;
IF (OBJECT_ID('tempdb..#AllViralLoads2') IS NOT NULL) DROP TABLE #AllViralLoads2;

SELECT DISTINCT
	c.PatientGuid,
	vl.*,
	c.ArtStartDate,
	c.Sex,
	DATEDIFF(MONTH, c.ArtStartDate, vl.InteractionDate) DurationOnArt
INTO #AllViralLoads
FROM ClientViralLoadTestResult vl
JOIN #TX_ClientsOfInterest c ON vl.PatientId = c.PatientIdInt
	WHERE vl.InteractionDate BETWEEN @BeginDate AND @EndDate;

INSERT INTO DataTech.dbo.VLs2020
SELECT DISTINCT * FROM #AllViralLoads;

SELECT DISTINCT
	c.PatientGuid,
	vl.*,
	c.ArtStartDate,
	c.Sex,
	DATEDIFF(MONTH, c.ArtStartDate, vl.InteractionDate) DurationOnArt
INTO #AllViralLoads2
FROM ClientViralLoadTestResult vl
JOIN #TX_ClientsOfInterest c ON vl.PatientId = c.PatientIdInt
	WHERE vl.InteractionDate BETWEEN @BeginDate2 AND @EndDate2;

INSERT INTO DataTech.dbo.VLs2021
SELECT DISTINCT * FROM #AllViralLoads2;
