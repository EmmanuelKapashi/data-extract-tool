
from flask_login import UserMixin, login_user, LoginManager, current_user, logout_user, login_required

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import psycopg2.extras

# Database Connection

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:313242241@localhost:5432/SmartcareDExT'
app.config["SQLALCHEMY_DATABASE_TRUCK_MODIFICATIONS"] = False
app.config['SECRET_KEY'] = 'appKey'
DB_HOST = "localhost"
DB_NAME = "SmartcareDExT"
DB_USER = "postgres"
DB_PASS = "313242241"

# Smartcare Server Connection

user = 'sa'
pwd = 'm7r@n$4mAz'
database = 'cdc_fdb_db'
db = SQLAlchemy(app)
conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)


db.init_app(app)

login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.init_app(app)

